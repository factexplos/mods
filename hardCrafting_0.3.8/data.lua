require "basic-lua-extensions"
require "functions"

-- Features:
require "prototypes.fast-long-inserter"
require "prototypes.incinerator"
require "prototypes.electric-incinerator"
require "prototypes.harder-iron-processing"
require "prototypes.harder-copper-processing"
require "prototypes.harder-coal"
require "prototypes.steel-dust"
require "prototypes.harder-buildings"
require "prototypes.belt-sorter"

-- Optional features:
--require "prototypes.engines-and-robots"
