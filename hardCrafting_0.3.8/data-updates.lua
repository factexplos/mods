require "functions"

-- Required:
require "prototypes.bigger-mining-drills"

-- Integration with recipes of other mods
require "prototypes.fluid-barrels"
require "prototypes.landfill"
