data:extend(
{
  {
    type = "item",
    name = "electric-stone-furnace",
    icon = "__Electric Furnaces__/graphics/icons/electric-stone-furnace.png",
    flags = {"goes-to-quickbar"},
    subgroup = "smelting-machine",
    order = "a[stone-furnace]b",
    place_result = "electric-stone-furnace",
    stack_size = 50
  },
  {
    type = "item",
    name = "electric-steel-furnace",
    icon = "__Electric Furnaces__/graphics/icons/electric-steel-furnace.png",
    flags = {"goes-to-quickbar"},
    subgroup = "smelting-machine",
    order = "b[steel-furnace]b",
    place_result = "electric-steel-furnace",
    stack_size = 50
  },
  {
    type = "item",
    name = "electric-furnace-2",
    icon = "__Electric Furnaces__/graphics/icons/electric-furnace-2.png",
    flags = {"goes-to-quickbar"},
    subgroup = "smelting-machine",
    order = "c[electric-furnace]b",
    place_result = "electric-furnace-2",
    stack_size = 50
  },
  {
    type = "item",
    name = "electric-furnace-3",
    icon = "__Electric Furnaces__/graphics/icons/electric-furnace-3.png",
    flags = {"goes-to-quickbar"},
    subgroup = "smelting-machine",
    order = "c[electric-furnace]c",
    place_result = "electric-furnace-3",
    stack_size = 50
  }
})