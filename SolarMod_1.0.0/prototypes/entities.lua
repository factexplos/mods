data:extend({
	{
		type = "solar-panel",
		name = "solar-generator-1",
		icon = "__SolarMod__/graphics/icons/solar-panel-1.png",
		flags = {"placeable-neutral", "player-creation"},
		minable = {hardness = 0.2, mining_time = 0.5, result = "solar-generator-1"},
		max_health = 100,
		corpse = "big-remnants",
		collision_box = {{-1.9, -1.9}, {1.9, 1.9}},
		selection_box = {{-2.0, -2.0}, {2.0, 2.0}},
		energy_source =
			{
			  type = "electric",
			  usage_priority = "solar"
			},
		picture =
			{
			  filename = "__SolarMod__/graphics/entity/solar-panel/solar-panel-1.png",
			  priority = "high",
			  width = 128,
			  height = 123
			},
    production = "300KW",
    fast_replaceable_group = "solar-panel",
	},
	{
		type = "solar-panel",
		name = "solar-generator-2",
		icon = "__SolarMod__/graphics/icons/solar-panel-2.png",
		flags = {"placeable-neutral", "player-creation"},
		minable = {hardness = 0.2, mining_time = 0.5, result = "solar-generator-2"},
		max_health = 100,
		corpse = "big-remnants",
		collision_box = {{-1.9, -1.9}, {1.9, 1.9}},
		selection_box = {{-2.0, -2.0}, {2.0, 2.0}},
		energy_source =
			{
			  type = "electric",
			  usage_priority = "solar"
			},
		picture =
			{
			  filename = "__SolarMod__/graphics/entity/solar-panel/solar-panel-2.png",
			  priority = "high",
			  width = 128,
			  height = 123
			},
    production = "800KW",
    fast_replaceable_group = "solar-panel",
	},
	{
		type = "solar-panel",
		name = "solar-generator-3",
		icon = "__SolarMod__/graphics/icons/solar-panel-3.png",
		flags = {"placeable-neutral", "player-creation"},
		minable = {hardness = 0.2, mining_time = 0.5, result = "solar-generator-3"},
		max_health = 100,
		corpse = "big-remnants",
		collision_box = {{-1.9, -1.9}, {1.9, 1.9}},
		selection_box = {{-2.0, -2.0}, {2.0, 2.0}},
		energy_source =
			{
			  type = "electric",
			  usage_priority = "solar"
			},
		picture =
			{
			  filename = "__SolarMod__/graphics/entity/solar-panel/solar-panel-3.png",
			  priority = "high",
			  width = 128,
			  height = 123
			},
    production = "2GW",
    fast_replaceable_group = "solar-panel",
	}
})