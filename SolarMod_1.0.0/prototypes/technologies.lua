data:extend({
	{
		type = "technology",
		name = "SOLAR-POWER-1",
		icon = "__base__/graphics/technology/solar-energy.png",
		effects =
		{
			{
			type = "unlock-recipe",
			recipe = "solar-generator-1"
			}
		},
	prerequisites = {"solar-energy"},
	unit =
			{
			count = 150,
			ingredients =
				{
					{"science-pack-1", 1},
					{"science-pack-2", 1},
					{"science-pack-3", 1}
				},
			time = 30
			},
	upgrade = true,
	order = "a-h-d",
	},
	{
		type = "technology",
		name = "SOLAR-POWER-2",
		icon = "__base__/graphics/technology/solar-energy.png",
		effects =
		{
			{
			type = "unlock-recipe",
			recipe = "solar-generator-2"
			}
		},
	prerequisites = {"SOLAR-POWER-1"},
	unit =
			{
			count = 200,
			ingredients =
				{
					{"science-pack-1", 2},
					{"science-pack-2", 2},
					{"science-pack-3", 2}
				},
			time = 60
			},
	upgrade = true,
	order = "a-h-e",
	},
	{
		type = "technology",
		name = "SOLAR-POWER-3",
		icon = "__base__/graphics/technology/solar-energy.png",
		effects =
		{
			{
			type = "unlock-recipe",
			recipe = "solar-generator-3"
			}
		},
	prerequisites = {"SOLAR-POWER-2"},
	unit =
			{
			count = 300,
			ingredients =
				{
					{"science-pack-1", 2},
					{"science-pack-2", 2},
					{"science-pack-3", 2}
				},
			time = 120
			},
	upgrade = true,
	order = "a-h-f",
	}
})