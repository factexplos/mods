data:extend({
	{
		type = "item",
		name = "solar-generator-1",
		icon = "__SolarMod__/graphics/icons/solar-panel-1.png",
		flags = { "goes-to-quickbar" },
		subgroup = "energy",
		order = "z[solar-generator-1]",
		place_result = "solar-generator-1",
		stack_size = 50
	},
	{
		type = "item",
		name = "solar-generator-2",
		icon = "__SolarMod__/graphics/icons/solar-panel-2.png",
		flags = { "goes-to-quickbar" },
		subgroup = "energy",
		order = "z-z[solar-generator-2]",
		place_result = "solar-generator-2",
		stack_size = 25
	},
	{
		type = "item",
		name = "solar-generator-3",
		icon = "__SolarMod__/graphics/icons/solar-panel-3.png",
		flags = { "goes-to-quickbar" },
		subgroup = "energy",
		order = "z-z-z[solar-generator-3]",
		place_result = "solar-generator-3",
		stack_size = 5
	}
})