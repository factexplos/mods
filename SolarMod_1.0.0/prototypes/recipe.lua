data:extend({
	{
		type = "recipe",
		name = "solar-generator-1",
		energy_required = 18,
		enabled = "false",
		ingredients =
			{
				{"steel-plate", 30},
				{"copper-plate", 200},
				{"solar-panel", 10}
			},
		result = "solar-generator-1"
	},
	{
		type = "recipe",
		name = "solar-generator-2",
		energy_required = 18,
		enabled = "false",
		ingredients =
			{
				{"steel-plate", 30},
				{"electronic-circuit", 100},
				{"copper-plate", 200},
				{"solar-generator-1", 2},
			},
		result = "solar-generator-2"
	},
	{
		type = "recipe",
		name = "solar-generator-3",
		energy_required = 18,
		enabled = "false",
		ingredients =
			{
				{"steel-plate", 30},
				{"advanced-circuit", 50},
				{"copper-plate", 200},
				{"solar-generator-2", 3},
			},
		result = "solar-generator-3"
	}
})