data:extend(
{
  {
    type = "recipe",
    name = "transformator",
    enabled = "false",
    ingredients =
    {
      {"iron-plate", 20},
      {"steel-plate", 20},
      {"copper-cable", 50},
      {"electronic-circuit", 10},
    },
    result = "transformator"
  },
 })