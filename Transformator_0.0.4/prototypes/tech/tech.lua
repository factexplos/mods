data:extend(
{
  {
    type = "technology",
    name = "transformator",
    icon = "__Transformator__/graphics/icons/trafoicon_big.png",
    effects =
    {
      {
        type = "unlock-recipe",
        recipe = "transformator"
      },
    },
    prerequisites = { "electric-energy-accumulators-1", "electric-energy-distribution-1"},
    unit = {
      count = 70,
      ingredients = {
        {"science-pack-1", 1},
        {"science-pack-2", 1}
      },
      time = 30
    },
    order = "c-e-c",
  },
})