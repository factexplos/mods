data:extend(
{
  	{
		type = "item",
		name = "transformator",
		icon = "__Transformator__/graphics/icons/trafoicon.png",
		flags = {"goes-to-quickbar"},
		subgroup = "energy",
		order = "e[accumulator]-a[basic-accumulator]-z",
		place_result = "transformator",
		stack_size = 5
	},
})