transparent_pic =
{
    filename = "__Transformator__/graphics/transparent.png",
    priority = "extra-high",
    width = 1,
    height = 1,
    frame_count = 1
}
con_pictures =
{
  filename = "__Transformator__/graphics/transparent.png",
  priority = "high",
  width = 1,
  height = 1,
  axially_symmetrical = false,
  direction_count = 1,
}
con_copper_wire_picture =
{
  filename = "__base__/graphics/entity/small-electric-pole/copper-wire.png",
  priority = "extra-high-no-scale",
  width = 224,
  height = 46
}
con_green_wire_picture =
{
  filename = "__base__/graphics/entity/small-electric-pole/green-wire.png",
  priority = "extra-high-no-scale",
  width = 224,
  height = 46
}
con_radius_visualisation_picture =
{
  filename = "__base__/graphics/entity/small-electric-pole/electric-pole-radius-visualization.png",
  width = 12,
  height = 12
}
con_red_wire_picture =
{
  filename = "__base__/graphics/entity/small-electric-pole/red-wire.png",
  priority = "extra-high-no-scale",
  width = 224,
  height = 46
}
con_wire_shadow_picture =
{
  filename = "__base__/graphics/entity/small-electric-pole/wire-shadow.png",
  priority = "extra-high-no-scale",
  width = 224,
  height = 46
}
con_resistances =
{
  {
	type = "fire",
	percent = 90
  }
}

data:extend(
{ 
  {
    type = "splitter",
    name = "transformator",
    icon = "__Transformator__/graphics/icons/trafoicon.png",
    flags = {"placeable-neutral","player-creation"},
    minable = {mining_time = 1, result = "transformator"},
    max_health = 300,
    corpse = "big-remnants",
    dying_explosion = "medium-explosion",
    resistances =
    {
      {
        type = "fire",
        percent = 70
      }
    },
	rotable = 0,
	collision_mask = { "item-layer", "object-layer", "player-layer", "water-tile"},
    collision_box = {{-0.9, -1.9}, {0.9, 1.9}},
    selection_box = {{-1.0, -2.0}, {1.0, 2.0}},
	--selection_box = {{-0.7, -1.7}, {0.7, 1.7}},
    drawing_box = {{-1.0, -3.0}, {1.0, 2.0}},
    animation_speed_coefficient = 32,
    structure_animation_speed_coefficient = 1.2,
    structure_animation_movement_cooldown = 10,
    belt_horizontal = transparent_pic, -- specified in transport-belt-pictures.lua
    belt_vertical = transparent_pic,
    ending_top = transparent_pic,
    ending_bottom = transparent_pic,
    ending_side = transparent_pic,
    starting_top = transparent_pic,
    starting_bottom = transparent_pic,
    starting_side = transparent_pic,
    speed = 0.0625,
    structure =
    {
      north =
      {
        filename = "__Transformator__/graphics/transformator/trafosprites.png",
        x = 233,
        width = 233,
        height = 155,
        frame_count = 1,
		shift = {2.6, -0.45}
      },
      east =
      {
        filename = "__Transformator__/graphics/transformator/trafosprites.png",
        width = 233,
        height = 155,
        frame_count = 1,
		shift = {1.5, -1.15}
      },
      south =
      {
        filename = "__Transformator__/graphics/transformator/trafosprites.png",
        x = 699,
        width = 233,
        height = 155,
        frame_count = 1,
		shift = {2.6, -0.45}
      },
      west =
      {
        filename = "__Transformator__/graphics/transformator/trafosprites.png",
        x = 466,
        width = 233,
        height = 155,
        frame_count = 1,
		shift = {1.5, -1.15}
      },
    },
    ending_patch = ending_patch_prototype
  },
 
  {
    type = "generator",
    name = "powerproducer",
    icon = "__Transformator__/graphics/icons/trafoicon.png",
    flags = {"placeable-neutral","player-creation"},
    max_health = 300,
    corpse = "big-remnants",
    dying_explosion = "medium-explosion",
    effectivity = 1,
    fluid_usage_per_tick = 16.666,
    collision_mask = {"ghost-layer"},
    collision_box = {{-0.9, -0.9}, {0.9, 0.9}},
    selection_box = {{-1.0, -1.0}, {1.0, 1.0}},
    fluid_box =
    {
      base_area = 2,
      pipe_covers = pipecoverspictures(),
      pipe_connections =
      {
      },
    },
    energy_source =
    {
      type = "electric",
      usage_priority = "primary-output"
    },
    horizontal_animation = transparent_pic,
    vertical_animation = transparent_pic,
    working_sound =
    {
      sound =
      {
        filename = "__Transformator__/sound/MainsBrum50Hz.ogg",
        volume = 0.6
      },
      max_sounds_per_type = 3
    },
    min_perceived_performance = 0.25,
    order = "z",
    selectable_in_game = false
  },
  {
    type = "assembling-machine",
    name = "powerconsumer",
    icon = "__Transformator__/graphics/icons/trafoicon.png",
    flags = {"placeable-neutral", "player-creation"},
    max_health = 150,
    corpse = "medium-remnants",
    collision_mask = {"ghost-layer"},
    collision_box = {{-0.9, -0.9}, {0.9, 0.9}},
    selection_box = {{-1.0, -1.0}, {1.0, 1.0}},
    energy_usage = "10kW",
    animation = transparent_pic,
    crafting_categories = {"crafting", "advanced-crafting", "crafting-with-fluid"},
    crafting_speed = 0.75,
    energy_source =
    {
      type = "electric",
      usage_priority = "secondary-input",
      emissions = 0,
      drain = "10kW"
    },
    energy_usage = "10MW",
    ingredient_count = 0,
    order = "z",
    selectable_in_game = false
  },
  {
    type = "electric-pole",
    name = "transformator-connection_src_0",
    icon = "__base__/graphics/icons/substation.png",
    flags = {"placeable-neutral", "player-creation"},
    max_health = 200,
    corpse = "medium-remnants",
    resistances = con_resistances,
	collision_mask = {"ghost-layer"},
    collision_box = {{-0.9, -0.9}, {0.9, 0.9}},
    selection_box = {{-1, -1}, {1, 1}},
    drawing_box = {{-1, -1.5}, {1, 1}},
    maximum_wire_distance = 3,
    supply_area_distance = 1,
	order = "z",
    pictures = con_pictures,
    connection_points =
    {
      {
        shadow =
        {
          copper = {2.5, -0.4},
          green = {2.5, -0.4},
          red = {2.5, -0.4}
        },
        wire =
        {
          copper = {0.0, -3},
          green = {0.0,-3},
          red = {0.0,-3}
        }
      },
    },
    copper_wire_picture = con_copper_wire_picture,
    green_wire_picture = con_green_wire_picture,
    radius_visualisation_picture = con_radius_visualisation_picture,
    red_wire_picture = con_red_wire_picture,
    wire_shadow_picture = con_wire_shadow_picture
  },
  {
    type = "electric-pole",
    name = "transformator-connection_src_2",
    icon = "__base__/graphics/icons/substation.png",
    flags = {"placeable-neutral", "player-creation"},
    max_health = 200,
    corpse = "medium-remnants",
    resistances = con_resistances,
	collision_mask = {"ghost-layer"},
    collision_box = {{-0.9, -0.9}, {0.9, 0.9}},
    selection_box = {{-1, -1}, {1, 1}},
    drawing_box = {{-1, -1.5}, {1, 1}},
    maximum_wire_distance = 3,
    supply_area_distance = 1,
	order = "z",
    pictures = con_pictures,
    connection_points =
    {
      {
        shadow =
        {
          copper = {1.3, 0.2},
          green = {1.3, 0.2},
          red = {1.3, 0.2}
        },
        wire =
        {
          copper = {0.6, -1.8},
          green = {0.6, -1.8},
          red = {0.6, -1.8}
        }
      },
    },
    copper_wire_picture = con_copper_wire_picture,
    green_wire_picture = con_green_wire_picture,
    radius_visualisation_picture = con_radius_visualisation_picture,
    red_wire_picture = con_red_wire_picture,
    wire_shadow_picture = con_wire_shadow_picture
  },
  {
    type = "electric-pole",
    name = "transformator-connection_src_4",
    icon = "__base__/graphics/icons/substation.png",
    flags = {"placeable-neutral", "player-creation"},
    max_health = 200,
    corpse = "medium-remnants",
    resistances = con_resistances,
	collision_mask = {"ghost-layer"},
    collision_box = {{-0.9, -0.9}, {0.9, 0.9}},
    selection_box = {{-1, -1}, {1, 1}},
    drawing_box = {{-1, -1.5}, {1, 1}},
    maximum_wire_distance = 3,
    supply_area_distance = 1,
	order = "z",
    pictures = con_pictures,
    connection_points =
    {
      {
        shadow =
        {
          copper = {2.5, 1.75},
          green = {2.5, 1.75},
          red = {2.5, 1.75}
        },
        wire =
        {
          copper = {0.0, -0.2},
          green = {0.0, -0.2},
          red = {0.0, -0.2}
        }
      },
    },
    copper_wire_picture = con_copper_wire_picture,
    green_wire_picture = con_green_wire_picture,
    radius_visualisation_picture = con_radius_visualisation_picture,
    red_wire_picture = con_red_wire_picture,
    wire_shadow_picture = con_wire_shadow_picture
  },
  {
    type = "electric-pole",
    name = "transformator-connection_src_6",
    icon = "__base__/graphics/icons/substation.png",
    flags = {"placeable-neutral", "player-creation"},
    max_health = 200,
    corpse = "medium-remnants",
    resistances = con_resistances,
	collision_mask = {"ghost-layer"},
    collision_box = {{-0.9, -0.9}, {0.9, 0.9}},
    selection_box = {{-1, -1}, {1, 1}},
    drawing_box = {{-1, -1.5}, {1, 1}},
    maximum_wire_distance = 3,
    supply_area_distance = 1,
	order = "z",
    pictures = con_pictures,
    connection_points =
    {
      {
        shadow =
        {
          copper = {2.0, 0.6},
          green = {2.0, 0.6},
          red = {2.0, 0.6}
        },
        wire =
        {
          copper = {-0.75, -1.75},
          green = {-0.75, -1.75},
          red = {-0.75, -1.75}
        }
      },
    },
    copper_wire_picture = con_copper_wire_picture,
    green_wire_picture = con_green_wire_picture,
    radius_visualisation_picture = con_radius_visualisation_picture,
    red_wire_picture = con_red_wire_picture,
    wire_shadow_picture = con_wire_shadow_picture
  },
  {
    type = "electric-pole",
    name = "transformator-connection_tar_0",
    icon = "__base__/graphics/icons/substation.png",
    flags = {"placeable-neutral", "player-creation"},
    max_health = 200,
    corpse = "medium-remnants",
    resistances = con_resistances,
	collision_mask = {"ghost-layer"},
    collision_box = {{-0.9, -0.9}, {0.9, 0.9}},
    selection_box = {{-1, -1}, {1, 1}},
    drawing_box = {{-1, -1.5}, {1, 1}},
    maximum_wire_distance = 3,
    supply_area_distance = 1,
	order = "z",
    pictures = con_pictures,
    connection_points =
    {
      {
        shadow =
        {
          copper = {3.0, 1.5},
          green = {3.0, 1.5},
          red = {3.0, 1.5}
        },
        wire =
        {
          copper = {0.0, -0.75},
          green = {0.0, -0.75},
          red = {0.0, -0.75}
        }
      },
    },
    copper_wire_picture = con_copper_wire_picture,
    green_wire_picture = con_green_wire_picture,
    radius_visualisation_picture = con_radius_visualisation_picture,
    red_wire_picture = con_red_wire_picture,
    wire_shadow_picture = con_wire_shadow_picture
  },
  {
    type = "electric-pole",
    name = "transformator-connection_tar_2",
    icon = "__base__/graphics/icons/substation.png",
    flags = {"placeable-neutral", "player-creation"},
    max_health = 200,
    corpse = "medium-remnants",
    resistances = con_resistances,
	collision_mask = {"ghost-layer"},
    collision_box = {{-0.9, -0.9}, {0.9, 0.9}},
    selection_box = {{-1, -1}, {1, 1}},
    drawing_box = {{-1, -1.5}, {1, 1}},
    maximum_wire_distance = 3,
    supply_area_distance = 1,
	order = "z",
    pictures = con_pictures,
    connection_points =
    {
      {
        shadow =
        {
          copper = {2.8, 0.5},
          green = {2.8, 0.5},
          red = {2.8, 0.5}
        },
        wire =
        {
          copper = {-0.8, -2.1},
          green = {-0.8, -2.1},
          red = {-0.8, -2.1}
        }
      },
    },
    copper_wire_picture = con_copper_wire_picture,
    green_wire_picture = con_green_wire_picture,
    radius_visualisation_picture = con_radius_visualisation_picture,
    red_wire_picture = con_red_wire_picture,
    wire_shadow_picture = con_wire_shadow_picture
  },
  {
    type = "electric-pole",
    name = "transformator-connection_tar_4",
    icon = "__base__/graphics/icons/substation.png",
    flags = {"placeable-neutral", "player-creation"},
    max_health = 200,
    corpse = "medium-remnants",
    resistances = con_resistances,
	collision_mask = {"ghost-layer"},
    collision_box = {{-0.9, -0.9}, {0.9, 0.9}},
    selection_box = {{-1, -1}, {1, 1}},
    drawing_box = {{-1, -1.5}, {1, 1}},
    maximum_wire_distance = 3,
    supply_area_distance = 1,
	order = "z",
    pictures = con_pictures,
    connection_points =
    {
      {
        shadow =
        {
          copper = {3.0, -0.2},
          green = {3.0, -0.2},
          red = {3.0, -0.2}
        },
        wire =
        {
          copper = {0.0, -3.0},
          green = {0.0, -3.0},
          red = {0.0, -3.0}
        }
      },
    },
    copper_wire_picture = con_copper_wire_picture,
    green_wire_picture = con_green_wire_picture,
    radius_visualisation_picture = con_radius_visualisation_picture,
    red_wire_picture = con_red_wire_picture,
    wire_shadow_picture = con_wire_shadow_picture
  },
  {
    type = "electric-pole",
    name = "transformator-connection_tar_6",
    icon = "__base__/graphics/icons/substation.png",
    flags = {"placeable-neutral", "player-creation"},
    max_health = 200,
    corpse = "medium-remnants",
    resistances = con_resistances,
	collision_mask = {"ghost-layer"},
    collision_box = {{-0.9, -0.9}, {0.9, 0.9}},
    selection_box = {{-1, -1}, {1, 1}},
    drawing_box = {{-1, -1.5}, {1, 1}},
    maximum_wire_distance = 3,
    supply_area_distance = 1,
	order = "z",
    pictures = con_pictures,
    connection_points =
    {
      {
        shadow =
        {
          copper = {1.8, 0.2},
          green = {1.8, 0.2},
          red = {1.8, 0.2}
        },
        wire =
        {
          copper = {0.6, -2.0},
          green = {0.6, -2.0},
          red = {0.6, -2.0}
        }
      },
    },
    copper_wire_picture = con_copper_wire_picture,
    green_wire_picture = con_green_wire_picture,
    radius_visualisation_picture = con_radius_visualisation_picture,
    red_wire_picture = con_red_wire_picture,
    wire_shadow_picture = con_wire_shadow_picture
  }
})