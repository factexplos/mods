require "defines"

--[[
global.transformators <- list of transformators

transformator:
	trafo     -- the transformator itself
	producer  -- the power producing part
	consumer  -- the power consuming part
	direction -- direction when first placed
	
all local functions are prefixed with "Transformator"
	
--]]

-- Make sure our functions get called for specific events.
-- A robot built one.
script.on_event(defines.events.on_robot_built_entity, function(event) OnBuilt(event.created_entity) end)
-- A player built one.
script.on_event(defines.events.on_built_entity, function(event) OnBuilt(event.created_entity) end)

-- A player removed one.
script.on_event(defines.events.on_preplayer_mined_item, function(event) OnRemoved(event.entity) end)
-- A robot removed one.
script.on_event(defines.events.on_robot_pre_mined, function(event) OnRemoved(event.entity) end)
-- It died.
script.on_event(defines.events.on_entity_died, function(event) OnRemoved(event.entity) end)

local function TransformatorIsTransformator(entity)
	if entity.name == "transformator" then
		return true
	else
		return false
	end
end

local function TransformatorGetSourcePosition(direction)
	if direction == 0 then		return {x = 0, y = 1}   -- north
	elseif direction == 2 then	return {x = -1, y = 0}  -- ost
	elseif direction == 4 then	return {x = 0, y = -1}  -- south
	else						return {x = 1, y = 0}   -- west
	end
end

local function TransformatorGetTargetPosition(direction)
	if direction == 0 then		return {x = 0, y = -1}   -- north
	elseif direction == 2 then	return {x = 1, y = 00}  -- ost
	elseif direction == 4 then	return {x = 0, y = 1}  -- south
	else						return {x = -1, y = 0}   -- west
	end
end
function OnBuilt(entity)
      --game.createentity{name = "pole-light", position = entity.position, force = entity.force}
	if TransformatorIsTransformator(entity) then
		local struct = {trafo = entity}
		struct.direction = entity.direction
		local srcpos = TransformatorGetSourcePosition(struct.direction)
		local tarpos = TransformatorGetTargetPosition(struct.direction)
		local srcconpos = {x = srcpos.x, y = srcpos.y}
		local tarconpos = {x = tarpos.x, y = tarpos.y}
		srcconpos.x = 2 * srcconpos.x + entity.position.x
		srcconpos.y = 2 * srcconpos.y + entity.position.y
		tarconpos.x = 2 * tarconpos.x + entity.position.x
		tarconpos.y = 2 * tarconpos.y + entity.position.y
		srcpos.x = srcpos.x + entity.position.x
		srcpos.y = srcpos.y + entity.position.y
		tarpos.x = tarpos.x + entity.position.x
		tarpos.y = tarpos.y + entity.position.y
		
		local create = game.player.surface.create_entity
		local producer = create{name = "powerproducer", position =
			tarpos, force = entity.force}
		struct.producer = producer
		local consumer = create{name = "powerconsumer", position =
			srcpos, force = entity.force}
		struct.consumer = consumer
		local consumer_pole = create{name = "transformator-connection".."_src_"..entity.direction,
			position = srcconpos, force = entity.force}
		struct.consumer_pole = consumer_pole
		local producer_pole = create{name = "transformator-connection".."_tar_"..entity.direction,
			position = tarconpos, force = entity.force}
		struct.producer_pole = producer_pole
		struct.lastdraw = 0
		table.insert(global.transformators, struct)
	end
end

function OnRemoved(entity)
	if TransformatorIsTransformator(entity) then
		local found_struct
		local index
		for key, struct in pairs(global.transformators) do
			if struct.trafo == entity then
				found_struct = struct
				index = key
				break
			end
		end
		table.remove(global.transformators, index)
		found_struct.producer.destroy()
		found_struct.producer = nil
		found_struct.consumer.destroy()
		found_struct.consumer = nil
		found_struct.producer_pole.destroy()
		found_struct.producer_pole = nil
		found_struct.consumer_pole.destroy()
		found_struct.consumer_pole = nil
	end
end

script.on_init(function(event)
	if global.transformators == nil then
		global.transformators = {}
	end
end)

script.on_event(defines.events.on_player_rotated_entity, function(event)
	local entity = event.entity;
	if TransformatorIsTransformator(entity) then
		for key, struct in pairs(global.transformators) do
			if struct.trafo == entity then
				entity.direction = struct.direction
				break
			end
		end
	end
end)

script.on_event(defines.events.on_tick, function(event)
	--if (game.tick % 18 ~= 0)
	--then
	--	return
	--end
	local efficiency = 0.98 -- efficiency of 98% (realistic for grid transformers)
	local max_amount = 20 -- 360
	for key, struct in pairs(global.transformators) do
		--local newbox = {type = "water", amount = 10, temperature = 25}
		--struct.producer.fluidbox[1] = newbox
		local target_amount = 0
		if struct.producer.fluidbox[1] ~= nil then
			target_amount = struct.producer.fluidbox[1].amount
		end
		local source_energy = struct.consumer.energy -- we can take so much energy from the source
		local target_energy = (max_amount - target_amount) * 10.0 * 1e3 / efficiency -- so much more energy can be stored
		local energy_to_move
		if source_energy > target_energy then
			energy_to_move = target_energy
		else
			energy_to_move = source_energy
		end

		if struct.lastdraw == nil then
			struct.lastdraw = 0
		end
		if energy_to_move > struct.lastdraw then
			energy_to_move = energy_to_move * 0.05 + 0.95 * struct.lastdraw
		end

		--game.player.print(tostring(source_energy).." and "..tostring(target_amount))
		--energy_to_move = 0

		struct.lastdraw = energy_to_move
		if energy_to_move ~= 0 then
			local liquid_amount = energy_to_move / 10.0 / 1e3
			--if target_amount ~= 0 then
			--	game.player.print(target_amount)
			--end
			local new_amount = target_amount + liquid_amount * efficiency 
			if new_amount > max_amount then
				new_amount = max_amount -- if rounding gone wrong
			end
			struct.producer.fluidbox[1] = {type="water", amount=new_amount, temperature=25}
			struct.consumer.energy = struct.consumer.energy - energy_to_move
		end
	end
end)