table.insert(
  data.raw["technology"]["alien-technology"].effects,
  {type = "unlock-recipe",recipe = "superconductor"})
table.insert(
  data.raw["technology"]["alien-technology"].effects,
  {type = "unlock-recipe",recipe = "superconducting-wire"})
table.insert(
  data.raw["technology"]["alien-technology"].effects,
  {type = "unlock-recipe",recipe = "superconducting-alien-artifact"})

if data.raw["recipe"]["sulfur-GDIW"] or
    data.raw["recipe"]["sulfur-m"] then
  table.insert(
    data.raw["technology"]["alien-technology"].effects,
    {type = "unlock-recipe",recipe = "superconductor-m"})
end