data:extend(
{
  {
    type = "recipe",
    name = "superconductor",
    category = "chemistry",
    enabled = false,
    energy_required = 4,
    ingredients =
    {
      {type="fluid", name="sulfuric-acid", amount=1},
      {type="fluid", name="water", amount=5},
      {"rare-earth", 5},
      {"copper-plate", 2}
    },
    result= "superconductor"
  },
  {
    type = "recipe",
    name = "superconducting-wire",
    enabled = false,
    energy_required = 10,
    category = "advanced-crafting",
    ingredients =
    {
      {"plastic-bar", 5},
      {"superconductor", 5}
    },
    result = "superconducting-wire",
    result_count = 2
  },
  {
    type = "recipe",
    name = "superconducting-alien-artifact",
    enabled = false,
    energy_required = 20,
    category = "advanced-crafting",
    ingredients =
    {
      {"superconducting-wire", 4},
      {"processing-unit", 1},
    },
    result = "alien-artifact"
  }
})

recipe_list =
{
  "superconductor",
  "superconducting-wire",
  "superconducting-alien-artifact"
}

-- If GDIW is present, add mirrored superconductor recipe
if data.raw["recipe"]["sulfur-GDIW"] then

  data.raw["recipe"]["superconductor"].icon =
      "__Crafted Artifacts__/graphics/icons/superconductor-l.png"
  superconductor_m = util.table.deepcopy(data.raw["recipe"]["superconductor"])
  superconductor_m.name = "superconductor-m"
  superconductor_m.icon = "__Crafted Artifacts__/graphics/icons/superconductor-l-m.png"
  superconductor_m.ingredients =
  {
    {type="fluid", name="water", amount=5},
    {type="fluid", name="sulfuric-acid", amount=1},
    {"rare-earth", 5},
    {"copper-plate", 2}
  }

  data:extend({ superconductor_m })

  table.insert(recipe_list, "superconductor-m")
end

-- Add recipes to productivity module whitelist
for km, vm in pairs(data.raw.module) do
  if vm.name:find("productivity%-module") and vm.limitation then
    for _, recipe in ipairs(recipe_list) do
      table.insert(vm.limitation, recipe)
    end
  end
end