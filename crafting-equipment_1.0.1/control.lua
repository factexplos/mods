require "defines"
 
craftingBoost = {0.3,0.5,0.75,400}
fluidCrafting = {}
advCrafting = {}
smelting = {}
chemCrafting = {}

function fluid_crafting(player_index,player,toggle)
	if toggle == "disable" and fluidCrafting[player_index] then
		player.force.recipes["express-transport-belt-manual"].enabled = false
		player.force.recipes["express-splitter-manual"].enabled = false
		player.force.recipes["processing-unit-manual"].enabled = false
		player.force.recipes["electric-engine-unit-manual"].enabled = false
		player.force.recipes["concrete-manual"].enabled = false	
		fluidCrafting[player_index] = false
	elseif toggle == "enable" and not fluidCrafting[player_index] then
		player.force.recipes["express-transport-belt-manual"].enabled = player.force.recipes["express-transport-belt"].enabled
		player.force.recipes["express-splitter-manual"].enabled = player.force.recipes["express-splitter"].enabled
		player.force.recipes["processing-unit-manual"].enabled = player.force.recipes["processing-unit"].enabled
		player.force.recipes["electric-engine-unit-manual"].enabled = player.force.recipes["electric-engine-unit"].enabled
		player.force.recipes["concrete-manual"].enabled = player.force.recipes["concrete"].enabled
		fluidCrafting[player_index] = true
	end
end
 
function advanced_crafting(player_index,player,toggle)
	if toggle == "disable" and advCrafting[player_index] then
		player.force.recipes["engine-unit-manual"].enabled = false
		player.force.recipes["engine-unit"].enabled = true
		advCrafting[player_index] = false
	elseif toggle == "enable" and not advCrafting[player_index] then
		player.force.recipes["engine-unit-manual"].enabled = true
		player.force.recipes["engine-unit"].enabled = false
		advCrafting[player_index] = true
	end
end

function smelting_crafting(player_index,player,toggle)
	if toggle == "disable" and smelting[player_index] then
		player.force.recipes["iron-plate-manual"].enabled = false
		player.force.recipes["copper-plate-manual"].enabled = false
		player.force.recipes["stone-brick-manual"].enabled = false
		player.force.recipes["steel-plate-manual"].enabled = false
		smelting[player_index] = false
	elseif toggle == "enable" and not smelting[player_index] then
		player.force.recipes["iron-plate-manual"].enabled = player.force.recipes["iron-plate"].enabled
		player.force.recipes["copper-plate-manual"].enabled = player.force.recipes["copper-plate"].enabled
		player.force.recipes["stone-brick-manual"].enabled =player.force.recipes["stone-brick"].enabled
		player.force.recipes["steel-plate-manual"].enabled = player.force.recipes["steel-plate"].enabled
		smelting[player_index] = true
	end
end

function chemical_crafting(player_index,player,toggle)
	if toggle == "disable" and chemCrafting[player_index] then
		player.force.recipes["heavy-oil-cracking-manual"].enabled = false
		player.force.recipes["light-oil-cracking-manual"].enabled = false
		player.force.recipes["sulfuric-acid-manual"].enabled = false
		player.force.recipes["plastic-bar-manual"].enabled = false
		player.force.recipes["solid-fuel-from-light-oil-manual"].enabled = false
		player.force.recipes["solid-fuel-from-petroleum-gas-manual"].enabled = false
		player.force.recipes["solid-fuel-from-heavy-oil-manual"].enabled = false
		player.force.recipes["sulfur-manual"].enabled = false
		player.force.recipes["lubricant-manual"].enabled = false
		player.force.recipes["flame-thrower-ammo-manual"].enabled = false
		player.force.recipes["explosives-manual"].enabled = false
		player.force.recipes["battery-manual"].enabled = false
		chemCrafting[player_index] = false
	elseif toggle == "enable" and not chemCrafting[player_index] then
		player.force.recipes["heavy-oil-cracking-manual"].enabled = player.force.recipes["heavy-oil-cracking"].enabled
		player.force.recipes["light-oil-cracking-manual"].enabled = player.force.recipes["light-oil-cracking"].enabled
		player.force.recipes["sulfuric-acid-manual"].enabled = player.force.recipes["sulfuric-acid"].enabled
		player.force.recipes["plastic-bar-manual"].enabled = player.force.recipes["plastic-bar"].enabled
		player.force.recipes["solid-fuel-from-light-oil-manual"].enabled = player.force.recipes["solid-fuel-from-light-oil"].enabled
		player.force.recipes["solid-fuel-from-petroleum-gas-manual"].enabled = player.force.recipes["solid-fuel-from-petroleum-gas"].enabled
		player.force.recipes["solid-fuel-from-heavy-oil-manual"].enabled = player.force.recipes["solid-fuel-from-heavy-oil"].enabled
		player.force.recipes["sulfur-manual"].enabled = player.force.recipes["sulfur"].enabled
		player.force.recipes["lubricant-manual"].enabled = player.force.recipes["lubricant"].enabled
		player.force.recipes["flame-thrower-ammo-manual"].enabled = player.force.recipes["flame-thrower-ammo"].enabled
		player.force.recipes["explosives-manual"].enabled = player.force.recipes["explosives"].enabled
		player.force.recipes["battery-manual"].enabled = player.force.recipes["battery"].enabled
		chemCrafting[player_index] = true
	end
end
 
script.on_event(defines.events.on_player_created, function(event)
	if player_index == nil then player_index = 1 end
	fluidCrafting[player_index] = false
	advCrafting[player_index] = false
	smelting[player_index] = false
	chemCrafting[player_index] = false
end)

script.on_event(defines.events.on_tick, function(event)
	for player_index,player in pairs(game.players) do
		if player.connected then
			local craftingMultiplier = 0
			local canCraftAdv = false
			local canCraftFluid = false
			local canSmelt = false
			local canCraftChem = false
			if player.character then
				if player.get_inventory(defines.inventory.player_armor)[1].valid_for_read then
					if player.get_inventory(defines.inventory.player_armor)[1].has_grid then
						local equipment = player.get_inventory(defines.inventory.player_armor)[1].grid.equipment
						for k,v in pairs(equipment) do
							if player.crafting_queue_size > 0 then
								if v.name == "crafting-equipment-mk1" then 
									craftingMultiplier = craftingMultiplier + (v.energy/v.max_energy)*craftingBoost[1]
									v.energy = v.energy - (v.energy/v.max_energy)*10000/60
								elseif v.name == "crafting-equipment-mk2" then 
									craftingMultiplier = craftingMultiplier + (v.energy/v.max_energy)*craftingBoost[2]
									v.energy = v.energy - (v.energy/v.max_energy)*15000/60
								elseif v.name == "crafting-equipment-mk3" then 			
									craftingMultiplier = craftingMultiplier + (v.energy/v.max_energy)*craftingBoost[3]
									v.energy = v.energy - (v.energy/v.max_energy)*20000/60								
								elseif v.name == "crafting-combined-equipment" then 			
									craftingMultiplier = craftingMultiplier + (v.energy/v.max_energy)*craftingBoost[4]
									v.energy = v.energy - (v.energy/v.max_energy)*45000/60
								end
							end
							if v.name == "crafting-combined-equipment" then
								canCraftFluid = true
								canCraftAdv = true
								canSmelt = true
								canCraftChem = true
							elseif v.name == "crafting-with-fluid-equipment" then
								canCraftFluid = true
								canCraftAdv = true
							elseif v.name == "crafting-advanced-equipment" then canCraftAdv = true 
							elseif v.name == "smelting-equipment" then canSmelt = true 
							elseif v.name == "chemical-equipment" then canCraftChem = true end
						end
					end
				end
			end
			player.force.manual_crafting_speed_modifier = craftingMultiplier
			if canCraftFluid then fluid_crafting(player_index,player,"enable") else fluid_crafting(player_index,player,"disable") end
			if canCraftAdv then advanced_crafting(player_index,player,"enable") else advanced_crafting(player_index,player,"disable") end
			if canSmelt then smelting_crafting(player_index,player,"enable") else smelting_crafting(player_index,player,"disable") end
			if canCraftChem then chemical_crafting(player_index,player,"enable") else chemical_crafting(player_index,player,"disable") end
		end
	end
end)

script.on_event(defines.events.on_research_finished, function(event)
	for player_index,player in pairs(game.players) do
		if fluidCrafting[player_index] then
			player.force.recipes["express-transport-belt-manual"].enabled = player.force.recipes["express-transport-belt"].enabled
			player.force.recipes["express-splitter-manual"].enabled = player.force.recipes["express-splitter"].enabled
			player.force.recipes["processing-unit-manual"].enabled = player.force.recipes["processing-unit"].enabled
			player.force.recipes["electric-engine-unit-manual"].enabled = player.force.recipes["electric-engine-unit"].enabled
			player.force.recipes["concrete-manual"].enabled = player.force.recipes["concrete"].enabled
		end
		if smelting[player_index] then		
			player.force.recipes["iron-plate-manual"].enabled = player.force.recipes["iron-plate"].enabled
			player.force.recipes["copper-plate-manual"].enabled = player.force.recipes["copper-plate"].enabled
			player.force.recipes["stone-brick-manual"].enabled =player.force.recipes["stone-brick"].enabled
			player.force.recipes["steel-plate-manual"].enabled = player.force.recipes["steel-plate"].enabled
		end
		if advCrafting[player_index] then
			player.force.recipes["engine-unit-manual"].enabled = true
			player.force.recipes["engine-unit"].enabled = false
		end
		if chemCrafting[player_index] then
			player.force.recipes["heavy-oil-cracking-manual"].enabled = player.force.recipes["heavy-oil-cracking"].enabled
			player.force.recipes["light-oil-cracking-manual"].enabled = player.force.recipes["light-oil-cracking"].enabled
			player.force.recipes["sulfuric-acid-manual"].enabled = player.force.recipes["sulfuric-acid"].enabled
			player.force.recipes["plastic-bar-manual"].enabled = player.force.recipes["plastic-bar"].enabled
			player.force.recipes["solid-fuel-from-light-oil-manual"].enabled = player.force.recipes["solid-fuel-from-light-oil"].enabled
			player.force.recipes["solid-fuel-from-petroleum-gas-manual"].enabled = player.force.recipes["solid-fuel-from-petroleum-gas"].enabled
			player.force.recipes["solid-fuel-from-heavy-oil-manual"].enabled = player.force.recipes["solid-fuel-from-heavy-oil"].enabled
			player.force.recipes["sulfur-manual"].enabled = player.force.recipes["sulfur"].enabled
			player.force.recipes["flame-thrower-ammo-manual"].enabled = player.force.recipes["flame-thrower-ammo"].enabled
		end
	end
end)