require("config")

require("prototypes.items")

require("prototypes.recipes-equipment")
require("prototypes.recipes-fluid")
require("prototypes.recipes-advanced")
require("prototypes.recipes-smelting")
require("prototypes.recipes-chemical")

require("prototypes.technologies")

require("prototypes.equipment")

require("prototypes.item-groups")