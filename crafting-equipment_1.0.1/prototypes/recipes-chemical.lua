data:extend({
 
  {
    type = "recipe",
    name = "heavy-oil-cracking-manual",
    enabled = false,
    energy_required = 5,
    ingredients =
    {
      {type="item", name="jerrycan-water", amount=3},
      {type="item", name="jerrycan-heavy-oil", amount=4}
    },
    results=
    {
      {type="item", name="jerrycan-light-oil", amount=3},
	  {type="item", name="jerrycan", amount=4}
    },
    icon = "__base__/graphics/icons/fluid/heavy-oil-cracking.png",
    subgroup = "fluid-recipes-manual",
    order = "b[fluid-chemistry]-a[heavy-oil-cracking]-b[manual]"
  },
    {
    type = "recipe",
    name = "light-oil-cracking-manual",
    enabled = false,
    energy_required = 5,
    ingredients =
    {
      {type="item", name="jerrycan-water", amount=3},
      {type="item", name="jerrycan-light-oil", amount=3}
    },
    results=
    {
      {type="item", name="jerrycan-petroleum-gas", amount=2},
	  {type="item", name="jerrycan", amount=4}
    },
    main_product= "",
    icon = "__base__/graphics/icons/fluid/light-oil-cracking.png",
    subgroup = "fluid-recipes-manual",
    order = "b[fluid-chemistry]-b[light-oil-cracking]-b[manual]"
  },
    {
    type = "recipe",
    name = "sulfuric-acid-manual",
    energy_required = 1,
    enabled = false,
    ingredients =
    {
      {type="item", name="sulfur", amount=5},
      {type="item", name="iron-plate", amount=1},
      {type="item", name="jerrycan-water", amount=8}
    },
    results=
    {
      {type="item", name="jerrycan-sulfuric-acid", amount=4},
	  {type="item", name="jerrycan", amount=4}
    },
	icon = "__base__/graphics/icons/fluid/sulfuric-acid.png",
    subgroup = "fluid-recipes-manual",
    order = "a[fluid]-f[sulfuric-acid]-b[manual]"
  },
  
  {
    type = "recipe",
    name = "plastic-bar-manual",
    energy_required = 2,
    enabled = false,
    ingredients =
    {
      {type="item", name="jerrycan-petroleum-gas", amount=5},
      {type="item", name="coal", amount=2}
    },
    results=
    {
      {type="item", name="plastic-bar", amount=4},
	  {type="item", name="jerrycan", amount=5}
    },
	icon = "__base__/graphics/icons/plastic-bar.png",
    subgroup = "raw-material",
    order = "g[plastic-bar]-b[manual]"
  },
  {
    type = "recipe",
    name = "solid-fuel-from-light-oil-manual",
    energy_required = 3,
    ingredients =
    {
      {type="item", name="jerrycan-light-oil", amount=1}
    },
    results=
    {
      {type="item", name="solid-fuel", amount=1},
	  {type="item", name="jerrycan", amount=1}
    },
    icon = "__base__/graphics/icons/solid-fuel-from-light-oil.png",
    subgroup = "fluid-recipes-manual",
    enabled = false,
    order = "b[fluid-chemistry]-c[solid-fuel-from-light-oil]-b[manual]"
  },
  {
    type = "recipe",
    name = "solid-fuel-from-petroleum-gas-manual",
    energy_required = 3,
    ingredients =
    {
      {type="item", name="jerrycan-petroleum-gas", amount=2}
    },
    results=
    {
      {type="item", name="solid-fuel", amount=1},
	  {type="item", name="jerrycan", amount=2}
    },
    icon = "__base__/graphics/icons/solid-fuel-from-petroleum-gas.png",
    subgroup = "fluid-recipes-manual",
    enabled = false,
    order = "b[fluid-chemistry]-d[solid-fuel-from-petroleum-gas]-b[manual]"
  },
  {
    type = "recipe",
    name = "solid-fuel-from-heavy-oil-manual",
    energy_required = 3,
    ingredients =
    {
      {type="item", name="jerrycan-heavy-oil", amount=2}
    },
    results=
    {
      {type="item", name="solid-fuel", amount=1},
	  {type="item", name="jerrycan", amount=2}
    },
    icon = "__base__/graphics/icons/solid-fuel-from-heavy-oil.png",
    subgroup = "fluid-recipes-manual",
    enabled = false,
    order = "b[fluid-chemistry]-e[solid-fuel-from-heavy-oil]-b[manual]"
  },
  {
    type = "recipe",
    name = "sulfur-manual",
    energy_required = 2,
    enabled = false,
    ingredients =
    {
      {type="item", name="jerrycan-water", amount=5},
      {type="item", name="jerrycan-petroleum-gas", amount=5}
    },
    results=
    {
      {type="item", name="sulfur", amount=4},
	  {type="item", name="jerrycan", amount=10}
    },
	icon = "__base__/graphics/icons/sulfur.png",
    subgroup = "raw-material",
    order = "f[sulfur]-b[manual]"
  },  
  {
    type = "recipe",
    name = "lubricant-manual",
    enabled = false,
    energy_required = 1,
    ingredients =
    {
      {type="item", name="jerrycan-heavy-oil", amount=1}
    },
    results=
    {
      {type="item", name="jerrycan-lubricant", amount=1}
    },
	icon = "__base__/graphics/icons/fluid/lubricant.png",
    subgroup = "fluid-recipes-manual",
    order = "e[lubricant]-b[manual]"
  },
    {
    type = "recipe",
    name = "flame-thrower-ammo-manual",
    enabled = false,
    energy_required = 3,
    ingredients =
    {
      {type="item", name="iron-plate", amount=5},
      {type="item", name="jerrycan-light-oil", amount=2},
      {type="item", name="jerrycan-heavy-oil", amount=2}
    },
    results = {
	  {type="item", name="flame-thrower-ammo", amount=1},
	  {type="item", name="jerrycan", amount=4}},
	icon = "__base__/graphics/icons/flame-thrower-ammo.png",
    subgroup = "ammo",
    order = "e[flame-thrower]-b[manual]"
  },
  {
    type = "recipe",
    name = "explosives-manual",
    energy_required = 5,
    enabled = false,
    ingredients =
    {
      {type="item", name="sulfur", amount=1},
      {type="item", name="coal", amount=1},
      {type="item", name="jerrycan-water", amount=1},
    },
    results= {
	  {"explosives",1},
	  {"jerrycan",1}},
	icon = "__base__/graphics/icons/explosives.png",
    subgroup = "intermediate-product",
    order = "h[explosives]-b[manual]"
  },
  {
    type = "recipe",
    name = "battery-manual",
    energy_required = 5,
    enabled = false,
    ingredients =
    {
      {type="item", name="jerrycan-sulfuric-acid", amount=4},
      {"iron-plate", 2},
      {"copper-plate", 2}
    },
    results= {{"battery",2},{"jerrycan",4}},
	icon = "__base__/graphics/icons/battery.png",
    subgroup = "intermediate-product",
    order = "i[battery]-b[manual]"
  }
})