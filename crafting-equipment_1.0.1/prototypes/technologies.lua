data:extend(
{
  {
    type = "technology",
    name = "crafting-equipment-mk1",
    icon = "__crafting-equipment__/graphics/crafting-1-tech.png",
    prerequisites = {"armor-making-3"},
    effects =
    {
      {
        type = "unlock-recipe",
        recipe = "crafting-equipment-mk1"
      }
    },
    unit =
    {
      count = 50,
      ingredients = {{"science-pack-1", 1}, {"science-pack-2", 1}},
      time = 15
    },
    order = "g-q-a"
  },
  {
    type = "technology",
    name = "crafting-equipment-mk2",
    icon = "__crafting-equipment__/graphics/crafting-2-tech.png",
    prerequisites = {"crafting-equipment-mk1","automation-2"},
    effects =
    {
      {
        type = "unlock-recipe",
        recipe = "crafting-equipment-mk2"
      }
    },
    unit =
    {
      count = 150,
      ingredients = {{"science-pack-1", 1}, {"science-pack-2", 1}, {"science-pack-3", 1}},
      time = 30
    },
    order = "g-q-b"
  },
  {
    type = "technology",
    name = "crafting-equipment-mk3",
    icon = "__crafting-equipment__/graphics/crafting-3-tech.png",
    prerequisites = {"crafting-equipment-mk2","automation-3"},
    effects =
    {
      {
        type = "unlock-recipe",
        recipe = "crafting-equipment-mk3"
      }
    },
    unit =
    {
      count = 300,
      ingredients = {{"science-pack-1", 2}, {"science-pack-2", 2}, {"science-pack-3", 2}, {"alien-science-pack", 2}},
      time = 60
    },
    order = "g-q-c"
  },
  {
    type = "technology",
    name = "crafting-advanced-equipment",
    icon = "__crafting-equipment__/graphics/crafting-advanced-tech.png",
    prerequisites = {"armor-making-3","automation-2","engine"},
    effects =
    {
      {
        type = "unlock-recipe",
        recipe = "crafting-advanced-equipment"
      }
    },
    unit =
    {
      count = 150,
      ingredients = {{"science-pack-1", 1}, {"science-pack-2", 1}},
      time = 30
    },
    order = "g-q-d"
  },
  {
    type = "technology",
    name = "smelting-equipment",
    icon = "__crafting-equipment__/graphics/smelting-tech.png",
    prerequisites = {"armor-making-3","advanced-material-processing-2"},
    effects =
    {
      {
        type = "unlock-recipe",
        recipe = "smelting-equipment"
      }
    },
    unit =
    {
      count = 150,
      ingredients = {{"science-pack-1", 1}, {"science-pack-2", 1}},
      time = 30
    },
    order = "g-q-e"
  },
  {
    type = "technology",
    name = "crafting-with-fluid-equipment",
    icon = "__crafting-equipment__/graphics/crafting-fluid-tech.png",
    prerequisites = {"automation-3","crafting-advanced-equipment"},
    effects =
    {
      {
        type = "unlock-recipe",
        recipe = "crafting-with-fluid-equipment"
      },
      {
        type = "unlock-recipe",
        recipe = "jerrycan"
      },
      {
        type = "unlock-recipe",
        recipe = "jerrycan-crude-oil"
      },
      {
        type = "unlock-recipe",
        recipe = "jerrycan-heavy-oil"
      },
      {
        type = "unlock-recipe",
        recipe = "jerrycan-light-oil"
      },
      {
        type = "unlock-recipe",
        recipe = "jerrycan-petroleum-gas"
      },
      {
        type = "unlock-recipe",
        recipe = "jerrycan-lubricant"
      },
      {
        type = "unlock-recipe",
        recipe = "jerrycan-sulfuric-acid"
      },
      {
        type = "unlock-recipe",
        recipe = "jerrycan-water"
      }
    },
    unit =
    {
      count = 200,
      ingredients = {{"science-pack-1", 1}, {"science-pack-2", 1}, {"science-pack-3", 1}},
      time = 30
    },
    order = "g-q-f"
  },
  {
    type = "technology",
    name = "chemical-equipment",
    icon = "__crafting-equipment__/graphics/crafting-chemical-tech.png",
    prerequisites = {"armor-making-3","advanced-material-processing-2"},
    effects =
    {
      {
        type = "unlock-recipe",
        recipe = "chemical-equipment"
      },
      {
        type = "unlock-recipe",
        recipe = "jerrycan"
      },
      {
        type = "unlock-recipe",
        recipe = "jerrycan-crude-oil"
      },
      {
        type = "unlock-recipe",
        recipe = "jerrycan-heavy-oil"
      },
      {
        type = "unlock-recipe",
        recipe = "jerrycan-light-oil"
      },
      {
        type = "unlock-recipe",
        recipe = "jerrycan-petroleum-gas"
      },
      {
        type = "unlock-recipe",
        recipe = "jerrycan-lubricant"
      },
      {
        type = "unlock-recipe",
        recipe = "jerrycan-sulfuric-acid"
      },
      {
        type = "unlock-recipe",
        recipe = "jerrycan-water"
      }
    },
    unit =
    {
      count = 200,
      ingredients = {{"science-pack-1", 1}, {"science-pack-2", 1}, {"science-pack-3", 1}},
      time = 30
    },
    order = "g-q-g"
  },
  {
    type = "technology",
    name = "crafting-combined-equipment",
    icon = "__crafting-equipment__/graphics/crafting-combined-tech.png",
    prerequisites = {"crafting-equipment-mk3","crafting-with-fluid-equipment","smelting-equipment","chemical-equipment"},
    effects =
    {
      {
        type = "unlock-recipe",
        recipe = "crafting-combined-equipment"
      }
    },
    unit =
    {
      count = 450,
      ingredients = {{"science-pack-1", 2}, {"science-pack-2", 2}, {"science-pack-3",2},{"alien-science-pack",2}},
      time = 60
    },
    order = "g-q-f"
  }
})
 