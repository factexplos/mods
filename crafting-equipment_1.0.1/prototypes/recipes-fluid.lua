data:extend({
 
  {
    type = "recipe",
    name = "jerrycan",
    enabled = false,
	energy_required = 1,
    ingredients = 
    {
      {"plastic-bar",1},
    },
    result = "jerrycan"
  },
  {
    type = "recipe",
    name = "jerrycan-crude-oil",
    category = "crafting-with-fluid",
    enabled = false,
    ingredients =
    {
      {"jerrycan", 1},
      {type="fluid", name="crude-oil", amount=1.25},
    },
    result = "jerrycan-crude-oil"
  },
  {
    type = "recipe",
    name = "jerrycan-heavy-oil",
    category = "crafting-with-fluid",
    enabled = false,
    ingredients =
    {
      {"jerrycan", 1},
      {type="fluid", name="heavy-oil", amount=1.25},
    },
    result = "jerrycan-heavy-oil"
  },
  {
    type = "recipe",
    name = "jerrycan-light-oil",
    category = "crafting-with-fluid",
    enabled = false,
    ingredients =
    {
      {"jerrycan", 1},
      {type="fluid", name="light-oil", amount=1.25},
    },
    result = "jerrycan-light-oil"
  },
  {
    type = "recipe",
    name = "jerrycan-petroleum-gas",
    category = "crafting-with-fluid",
    enabled = false,
    ingredients =
    {
      {"jerrycan", 1},
      {type="fluid", name="petroleum-gas", amount=1.25},
    },
    result = "jerrycan-petroleum-gas"
  },
  {
    type = "recipe",
    name = "jerrycan-lubricant",
    category = "crafting-with-fluid",
    enabled = false,
    ingredients =
    {
      {"jerrycan", 1},
      {type="fluid", name="lubricant", amount=1.25},
    },
    result = "jerrycan-lubricant"
  },
  {
    type = "recipe",
    name = "jerrycan-sulfuric-acid",
    category = "crafting-with-fluid",
    enabled = false,
    ingredients =
    {
      {"jerrycan", 1},
      {type="fluid", name="sulfuric-acid", amount=1.25},
    },
    result = "jerrycan-sulfuric-acid"
  },
  {
    type = "recipe",
    name = "jerrycan-water",
    category = "crafting-with-fluid",
    enabled = false,
    ingredients =
    {
      {"jerrycan", 1},
      {type="fluid", name="water", amount=1.25},
    },
    result = "jerrycan-water"
  },
  {
    type = "recipe",
    name = "express-transport-belt-manual",
    enabled = false,
    ingredients =
    {
      {"iron-gear-wheel", 5},
      {"fast-transport-belt", 1},
      {"jerrycan-lubricant", 2},
    },
	results=
    {
      {type="item", name="express-transport-belt", amount=1},
      {type="item", name="jerrycan", amount=2}
    },
    icon = "__base__/graphics/icons/express-transport-belt.png",
    subgroup = "belt",
    order = "a[transport-belt]-c[express-transport-belt]"
  },
  {
    type = "recipe",
    name = "express-splitter-manual",
    enabled = false,
    energy_required = 2,
    ingredients =
    {
      {"fast-splitter", 1},
      {"iron-gear-wheel", 10},
      {"advanced-circuit", 10},
      {"jerrycan-lubricant", 8}
    },
	results=
    {
      {type="item", name="express-splitter", amount=1},
      {type="item", name="jerrycan", amount=8}
    },
    icon = "__base__/graphics/icons/express-splitter.png",
    subgroup = "belt",
    order = "c[splitter]-c[express-splitter]"
  },
  {
    type = "recipe",
    name = "processing-unit-manual",
    enabled = false,
    energy_required = 30,
    ingredients =
    {
      {"electronic-circuit", 40},
      {"advanced-circuit", 4},
      {"jerrycan-sulfuric-acid", 1}
    },
	results=
    {
      {type="item", name="processing-unit", amount=2},
      {type="item", name="jerrycan", amount=1}
    },
    icon = "__base__/graphics/icons/processing-unit.png",
    subgroup = "intermediate-product",
    order = "e[processing-unit]"
  },
   {
    type = "recipe",
    name = "electric-engine-unit-manual",
    enabled = false,
    energy_required = 20,
    ingredients =
    {
       {"engine-unit", 1},
       {"jerrycan-lubricant", 2},
       {"electronic-circuit", 2}
    },  
	results=
    {
       {type="item", name="electric-engine-unit", amount=1},
       {type="item", name="jerrycan", amount=2}
    },
    icon = "__base__/graphics/icons/electric-engine-unit.png",
    subgroup = "intermediate-product",
    order = "g[electric-engine-unit]"
   },
  {
    type = "recipe",
    name = "concrete-manual",
    energy_required = 10,
    enabled = false,
    ingredients =
    {
      {"stone-brick", 5},
      {"iron-ore", 1},
      {"jerrycan-water",8}
    },
      results=
    {
      {type="item", name="concrete", amount=10},
      {type="item", name="jerrycan", amount=8}
    },
    icon = "__base__/graphics/icons/concrete.png",
    subgroup = "terrain",
    order = "b[concrete]"
  }
})