data.raw["item-subgroup"]["fluid-recipes"].order = "a-a"
data.raw["item-subgroup"]["barrel"].order = "d-a"
 
 data:extend(
{
 {
    type = "item-subgroup",
    name = "fluid-recipes-manual",
    group = "intermediate-products",
    order = "a-b"
  },
  {
    type = "item-subgroup",
    name = "jerrycan",
    group = "intermediate-products",
    order = "d-b"
  }
 })