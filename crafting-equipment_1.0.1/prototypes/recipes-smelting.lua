data:extend({
 
  {
    type = "recipe",
    name = "iron-plate-manual",
    enabled = false,
	energy_required = 3.5,
    ingredients = 
    {
      {"iron-ore",1},
    },
    result = "iron-plate"
  },
    {
    type = "recipe",
    name = "copper-plate-manual",
    enabled = false,
	energy_required = 3.5,
    ingredients = 
    {
      {"copper-ore",1},
    },
    result = "copper-plate"
  },
    {
    type = "recipe",
    name = "steel-plate-manual",
    enabled = false,
	energy_required = 17.5,
    ingredients = 
    {
      {"iron-plate",5},
    },
    result = "steel-plate"
  },
    {
    type = "recipe",
    name = "stone-brick-manual",
    enabled = false,
	energy_required = 3.5,
    ingredients = 
    {
      {"stone",2},
    },
    result = "stone-brick"
  }
})