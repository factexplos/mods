data:extend({
 
  {
    type = "recipe",
    name = "engine-unit-manual",
    enabled = false,
	energy_required = 20,
    ingredients = 
    {
      {"steel-plate",1},
	  {"iron-gear-wheel",1},
	  {"pipe",2}
    },
    result = "engine-unit"
  }
})