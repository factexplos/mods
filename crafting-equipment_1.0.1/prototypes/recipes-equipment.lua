data:extend({
 {
    type = "recipe",
    name = "crafting-equipment-mk1",
    enabled = false,
	energy_required = 5,
    ingredients = 
    {
      {"assembling-machine-1",4},
      {"electronic-circuit",5},
    },
    result = "crafting-equipment-mk1"
  }, 
  {
    type = "recipe",
    name = "crafting-equipment-mk2",
    enabled = false,
	energy_required = 10,
    ingredients = 
    {
      {"assembling-machine-2",4},
      {"advanced-circuit",10},
	  {"crafting-equipment-mk1",4}
    },
    result = "crafting-equipment-mk2"
  },
  {
    type = "recipe",
    name = "crafting-equipment-mk3",
    enabled = false,
	energy_required = 15,
    ingredients = 
    {
      {"assembling-machine-3",4},
      {"processing-unit",15},
	  {"crafting-equipment-mk2",4}
    },
    result = "crafting-equipment-mk3"
  },
  {
    type = "recipe",
    name = "crafting-advanced-equipment",
    enabled = false,
	energy_required = 10,
    ingredients = 
    {
      {"assembling-machine-2",2},
      {"advanced-circuit",5}
    },
    result = "crafting-advanced-equipment"
  },
  {
    type = "recipe",
    name = "smelting-equipment",
    enabled = false,
	energy_required = 30,
    ingredients = 
    {
      {"electric-furnace",3},
      {"advanced-circuit",25}
    },
    result = "smelting-equipment"
  },
  {
    type = "recipe",
    name = "crafting-with-fluid-equipment",
    enabled = false,
	energy_required = 30,
    ingredients = 
    {
      {"assembling-machine-3",5},
      {"processing-unit",25},
	  {"crafting-advanced-equipment",1}
    },
    result = "crafting-with-fluid-equipment"
  },
  {
    type = "recipe",
    name = "chemical-equipment",
    enabled = false,
	energy_required = 30,
    ingredients = 
    {
      {"chemical-plant",4},
      {"advanced-circuit",25}
    },
    result = "chemical-equipment"
  },
  {
    type = "recipe",
    name = "crafting-combined-equipment",
    enabled = false,
	energy_required = 60,
    ingredients = 
    {
      {"chemical-equipment",2},
      {"crafting-with-fluid-equipment",2},
	  {"smelting-equipment",2},
	  {"crafting-equipment-mk3",2}	 ,
	  {"effectivity-module-3",5}
    },
    result = "crafting-combined-equipment"
  }
})