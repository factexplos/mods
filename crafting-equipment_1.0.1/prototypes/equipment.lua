data:extend(
{
  {
    type = "energy-shield-equipment",
    name = "crafting-equipment-mk1",
    sprite =
    {
      filename = "__crafting-equipment__/graphics/crafting-1.png",
      width = 32,
      height = 32,
      priority = "medium"
    },    
    shape =
    {
      width = 2,
      height = 2,
      type = "full"
    },
    energy_source =
    {
      type = "electric",
      buffer_capacity = "50kJ",
      input_flow_limit = "10kW",
      usage_priority = "secondary-input"
	 },
	max_shield_value = 0,
	energy_per_shield = "0J"
  },
    {
    type = "energy-shield-equipment",
    name = "crafting-equipment-mk2",
    sprite =
    {
      filename = "__crafting-equipment__/graphics/crafting-2.png",
      width = 32,
      height = 32,
      priority = "medium"
    },
    shape =
    {
      width = 2,
      height = 2,
      type = "full"
    },
    energy_source =
    {
      type = "electric",
      buffer_capacity = "100kJ",
      input_flow_limit = "15kW",
      usage_priority = "secondary-input"
    },
	max_shield_value = 0,
	energy_per_shield = "0J"
  },
    {
    type = "energy-shield-equipment",
    name = "crafting-equipment-mk3",
    sprite =
    {
      filename = "__crafting-equipment__/graphics/crafting-3.png",
      width = 32,
      height = 32,
      priority = "medium"
    },
    shape =
    {
      width = 2,
      height = 2,
      type = "full"
    },
    energy_source =
    {
      type = "electric",
      buffer_capacity = "150kJ",
      input_flow_limit = "20kW",
      usage_priority = "secondary-input"
    },
	max_shield_value = 0,
	energy_per_shield = "0J"
  },
  {
    type = "night-vision-equipment",
    name = "crafting-with-fluid-equipment",
    sprite =
    {
      filename = "__crafting-equipment__/graphics/crafting-fluid.png",
      width = 48,
      height = 48,
      priority = "medium"
    },
    shape =
    {
      width = 3,
      height = 3,
      type = "full"
    },
    energy_source =
    {
      type = "electric",
      buffer_capacity = "1J",
      input_flow_limit = "0W",
      output_flow_limit = "0W",
      usage_priority = "terciary"
    },
    energy_input = "0W",
    tint = {r = 0, g = 0, b = 0, a = 0.2}
  },
  {
    type = "night-vision-equipment",
    name = "crafting-advanced-equipment",
    sprite =
    {
      filename = "__crafting-equipment__/graphics/crafting-advanced.png",
      width = 48,
      height = 48,
      priority = "medium"
    },
    shape =
    {
      width = 3,
      height = 3,
      type = "full"
    },
    energy_source =
    {
      type = "electric",
      buffer_capacity = "1J",
      input_flow_limit = "0W",
      output_flow_limit = "0W",
      usage_priority = "terciary"
    },
    energy_input = "0W",
    tint = {r = 0, g = 0, b = 0, a = 0.2}
  },
  {
    type = "night-vision-equipment",
    name = "smelting-equipment",
    sprite =
    {
      filename = "__crafting-equipment__/graphics/smelting.png",
      width = 48,
      height = 48,
      priority = "medium"
    },
    shape =
    {
      width = 3,
      height = 3,
      type = "full"
    },
    energy_source =
    {
      type = "electric",
      buffer_capacity = "1J",
      input_flow_limit = "0W",
      output_flow_limit = "0W",
      usage_priority = "terciary"
    },
    energy_input = "0W",
    tint = {r = 0, g = 0, b = 0, a = 0.2}
  },
    {
    type = "night-vision-equipment",
    name = "chemical-equipment",
    sprite =
    {
      filename = "__crafting-equipment__/graphics/crafting-chemical.png",
      width = 48,
      height = 48,
      priority = "medium"
    },
    shape =
    {
      width = 3,
      height = 3,
      type = "full"
    },
    energy_source =
    {
      type = "electric",
      buffer_capacity = "1J",
      input_flow_limit = "0W",
      output_flow_limit = "0W",
      usage_priority = "terciary"
    },
    energy_input = "0W",
    tint = {r = 0, g = 0, b = 0, a = 0.2}
  },
    {
    type = "energy-shield-equipment",
    name = "crafting-combined-equipment",
    sprite =
    {
      filename = "__crafting-equipment__/graphics/crafting-combined.png",
      width = 32,
      height = 32,
      priority = "medium"
    },
    shape =
    {
      width = 3,
      height = 3,
      type = "full"
    },
    energy_source =
    {
      type = "electric",
      buffer_capacity = "450kJ",
      input_flow_limit = "45kW",
      usage_priority = "secondary-input"
    },
	max_shield_value = 0,
	energy_per_shield = "0J"
  }
})