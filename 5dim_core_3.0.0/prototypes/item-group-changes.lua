--Armor

data.raw.gun["flame-thrower"].subgroup = "armor-flame";
data.raw.gun["flame-thrower"].order = "a";
data.raw.item["land-mine"].subgroup = "armor-capsule";
data.raw.item["land-mine"].order = "a";
data.raw.gun["rocket-launcher"].subgroup = "armor-rocket";
data.raw.gun["rocket-launcher"].order = "a";
data.raw.gun["shotgun"].subgroup = "armor-shotgun";
data.raw.gun["shotgun"].order = "a";
data.raw.gun["combat-shotgun"].subgroup = "armor-shotgun";
data.raw.gun["combat-shotgun"].order = "b";
data.raw.gun["submachine-gun"].subgroup = "armor-bullet";
data.raw.gun["submachine-gun"].order = "b";
data.raw.gun["pistol"].subgroup = "armor-bullet";
data.raw.gun["pistol"].order = "a";
data.raw.ammo["piercing-bullet-magazine"].subgroup = "armor-bullet";
data.raw.ammo["piercing-bullet-magazine"].order = "d";
data.raw.ammo["basic-bullet-magazine"].subgroup = "armor-bullet";
data.raw.ammo["basic-bullet-magazine"].order = "c";
data.raw.ammo["flame-thrower-ammo"].subgroup = "armor-flame";
data.raw.ammo["flame-thrower-ammo"].order = "b";
data.raw.ammo["explosive-rocket"].subgroup = "armor-rocket";
data.raw.ammo["explosive-rocket"].order = "c";
data.raw.ammo["rocket"].subgroup = "armor-rocket";
data.raw.ammo["rocket"].order = "b";
data.raw.ammo["piercing-shotgun-shell"].subgroup = "armor-shotgun";
data.raw.ammo["piercing-shotgun-shell"].order = "d";
data.raw.ammo["shotgun-shell"].subgroup = "armor-shotgun";
data.raw.ammo["shotgun-shell"].order = "c";
data.raw.capsule["basic-grenade"].subgroup = "armor-capsule";
data.raw.capsule["basic-grenade"].order = "a";
data.raw.capsule["poison-capsule"].subgroup = "armor-capsule";
data.raw.capsule["poison-capsule"].order = "b";
data.raw.capsule["slowdown-capsule"].subgroup = "armor-capsule";
data.raw.capsule["slowdown-capsule"].order = "c";
data.raw.capsule["defender-capsule"].subgroup = "armor-capsule";
data.raw.capsule["defender-capsule"].order = "d";
data.raw.capsule["distractor-capsule"].subgroup = "armor-capsule";
data.raw.capsule["distractor-capsule"].order = "e";
data.raw.capsule["destroyer-capsule"].subgroup = "armor-capsule";
data.raw.capsule["destroyer-capsule"].order = "f";
data.raw.capsule["basic-electric-discharge-defense-remote"].subgroup = "armor-capsule";
data.raw.capsule["basic-electric-discharge-defense-remote"].order = "g";
data.raw.armor["basic-armor"].subgroup = "armor-armor";
data.raw.armor["basic-armor"].order = "a";
data.raw.armor["heavy-armor"].subgroup = "armor-armor";
data.raw.armor["heavy-armor"].order = "b";
data.raw.armor["basic-modular-armor"].subgroup = "armor-armor";
data.raw.armor["basic-modular-armor"].order = "c";
data.raw.armor["power-armor"].subgroup = "armor-armor";
data.raw.armor["power-armor"].order = "d";
data.raw.armor["power-armor-mk2"].subgroup = "armor-armor";
data.raw.armor["power-armor-mk2"].order = "e";
data.raw.item["solar-panel-equipment"].subgroup = "armor-util";
data.raw.item["solar-panel-equipment"].order = "a";
data.raw.item["fusion-reactor-equipment"].subgroup = "armor-util";
data.raw.item["fusion-reactor-equipment"].order = "b";
data.raw.item["battery-equipment"].subgroup = "armor-util";
data.raw.item["battery-equipment"].order = "c";
data.raw.item["battery-mk2-equipment"].subgroup = "armor-util";
data.raw.item["battery-mk2-equipment"].order = "d";
data.raw.item["basic-exoskeleton-equipment"].subgroup = "armor-util";
data.raw.item["basic-exoskeleton-equipment"].order = "e";
data.raw.item["personal-roboport-equipment"].subgroup = "armor-util";
data.raw.item["personal-roboport-equipment"].order = "f";
data.raw.item["night-vision-equipment"].subgroup = "armor-util";
data.raw.item["night-vision-equipment"].order = "g";
data.raw.item["energy-shield-equipment"].subgroup = "armor-dmg";
data.raw.item["energy-shield-equipment"].order = "a";
data.raw.item["energy-shield-mk2-equipment"].subgroup = "armor-dmg";
data.raw.item["energy-shield-mk2-equipment"].order = "b";
data.raw.item["basic-laser-defense-equipment"].subgroup = "armor-dmg";
data.raw.item["basic-laser-defense-equipment"].order = "c";
data.raw.item["basic-electric-discharge-defense-equipment"].subgroup = "armor-dmg";
data.raw.item["basic-electric-discharge-defense-equipment"].order = "d";

-- Automatization

data.raw.item["basic-inserter"].subgroup = "inserters-speed1";
data.raw.item["basic-inserter"].order = "a";
data.raw.item["long-handed-inserter"].subgroup = "inserters-speed1";
data.raw.item["long-handed-inserter"].order = "c";
data.raw.item["fast-inserter"].subgroup = "inserters-speed2";
data.raw.item["fast-inserter"].order = "a";
data.raw.item["smart-inserter"].subgroup = "inserters-smart";
data.raw.item["smart-inserter"].order = "a";
data.raw.item["burner-inserter"].subgroup = "inserters-burner";
data.raw.item["assembling-machine-1"].subgroup = "assembling-machine";
data.raw.item["assembling-machine-1"].order = "a";
data.raw.item["assembling-machine-2"].subgroup = "assembling-machine";
data.raw.item["assembling-machine-2"].order = "b";
data.raw.item["assembling-machine-3"].subgroup = "assembling-machine";
data.raw.item["assembling-machine-3"].order = "c";

--Defense

data.raw.item["laser-turret"].subgroup = "defense-laser";
data.raw.item["laser-turret"].order = "b";
data.raw.item["gun-turret"].subgroup = "defense-gun";
data.raw.item["gun-turret"].order = "b";
data.raw.item["stone-wall"].subgroup = "defense-wall";
data.raw.item["stone-wall"].order = "a";
data.raw.item["gate"].subgroup = "defense-gate";
data.raw.item["gate"].order = "a";
data.raw.item["radar"].subgroup = "defense-radar";
data.raw.item["radar"].order = "a";


--Decoration

data.raw.item["concrete"].subgroup = "decoration-floor";
data.raw.item["concrete"].order = "a-b";
data.raw.item["stone-brick"].subgroup = "decoration-floor";
data.raw.item["stone-brick"].order = "a-a";

--Energy

data.raw.item["big-electric-pole"].subgroup = "energy-pole";
data.raw.item["big-electric-pole"].order = "d";
data.raw.item["medium-electric-pole"].subgroup = "energy-pole";
data.raw.item["medium-electric-pole"].order = "b";
data.raw.item["small-electric-pole"].subgroup = "energy-pole";
data.raw.item["small-electric-pole"].order = "a";
data.raw.item["solar-panel"].subgroup = "energy-solar-panel";
data.raw.item["solar-panel"].order = "a";
data.raw.item["basic-accumulator"].subgroup = "energy-accumulator";
data.raw.item["basic-accumulator"].order = "a"
data.raw.item["boiler"].subgroup = "energy-boiler"
data.raw.item["boiler"].order = "a"
data.raw.item["steam-engine"].subgroup = "energy-engine-1"
data.raw.item["steam-engine"].order = "a"
data.raw.item["small-lamp"].subgroup = "energy-lamp"
data.raw.item["small-lamp"].order = "a"
data.raw.item["offshore-pump"].subgroup = "energy-offshore-pump"
data.raw.item["offshore-pump"].order = "a"
data.raw.item["small-pump"].subgroup = "energy-small-pump"
data.raw.item["small-pump"].order = "a"
data.raw.item["substation"].subgroup = "energy-pole"
data.raw.item["substation"].order = "f"

--Intermediate

data.raw.item["iron-stick"].subgroup = "intermediate-misc"
data.raw.item["iron-stick"].order = "a"
data.raw.item["engine-unit"].subgroup = "intermediate-misc"
data.raw.item["engine-unit"].order = "b"
data.raw.item["electric-engine-unit"].subgroup = "intermediate-misc"
data.raw.item["electric-engine-unit"].order = "c"
data.raw.item["explosives"].subgroup = "intermediate-misc"
data.raw.item["explosives"].order = "f"
data.raw.item["battery"].subgroup = "intermediate-misc"
data.raw.item["battery"].order = "e"
data.raw.item["flying-robot-frame"].subgroup = "intermediate-misc"
data.raw.item["flying-robot-frame"].order = "d"
data.raw.tool["science-pack-1"].subgroup = "intermediate-lab"
data.raw.tool["science-pack-1"].order = "a"
data.raw.tool["science-pack-2"].subgroup = "intermediate-lab"
data.raw.tool["science-pack-2"].order = "b"
data.raw.tool["science-pack-3"].subgroup = "intermediate-lab"
data.raw.tool["science-pack-3"].order = "c"
data.raw.tool["alien-science-pack"].subgroup = "intermediate-lab"
data.raw.tool["alien-science-pack"].order = "d"
data.raw.item["electronic-circuit"].subgroup = "intermediate-chip"
data.raw.item["electronic-circuit"].order = "a"
data.raw.item["advanced-circuit"].subgroup = "intermediate-chip"
data.raw.item["advanced-circuit"].order = "b"
data.raw.item["processing-unit"].subgroup = "intermediate-chip"
data.raw.item["processing-unit"].order = "c"
data.raw.item["iron-gear-wheel"].subgroup = "intermediate-gear"
data.raw.item["iron-gear-wheel"].order = "a"
data.raw.item["low-density-structure"].subgroup = "intermediate-silo"
data.raw.item["low-density-structure"].order = "b"
data.raw.item["rocket-fuel"].subgroup = "intermediate-silo"
data.raw.item["rocket-fuel"].order = "c"
data.raw.item["rocket-control-unit"].subgroup = "intermediate-silo"
data.raw.item["rocket-control-unit"].order = "d"
data.raw.item["rocket-part"].subgroup = "intermediate-silo"
data.raw.item["rocket-part"].order = "e"
data.raw.item["satellite"].subgroup = "intermediate-silo"
data.raw.item["satellite"].order = "f"
data.raw.item["rocket-silo"].subgroup = "intermediate-silo"
data.raw.item["rocket-silo"].order = "a"

--Liquids

data.raw.item["chemical-plant"].subgroup = "liquid-plant"
data.raw.item["chemical-plant"].order = "a"
data.raw.item["oil-refinery"].subgroup = "liquid-refinery"
data.raw.item["oil-refinery"].order = "a"
data.raw.item["pumpjack"].subgroup = "liquid-pump"
data.raw.item["pumpjack"].order = "a"
data.raw.item["storage-tank"].subgroup = "liquid-store"
data.raw.item["storage-tank"].order = "a"
data.raw.recipe["sulfuric-acid"].subgroup = "liquid-recipe"
data.raw.recipe["basic-oil-processing"].subgroup = "liquid-recipe"
data.raw.recipe["advanced-oil-processing"].subgroup = "liquid-recipe"
data.raw.recipe["heavy-oil-cracking"].subgroup = "liquid-recipe"
data.raw.recipe["light-oil-cracking"].subgroup = "liquid-recipe"
data.raw.recipe["sulfuric-acid"].subgroup = "liquid-recipe"
data.raw.recipe["solid-fuel-from-light-oil"].subgroup = "liquid-recipe"
data.raw.recipe["solid-fuel-from-petroleum-gas"].subgroup = "liquid-recipe"
data.raw.recipe["solid-fuel-from-heavy-oil"].subgroup = "liquid-recipe"
data.raw.recipe["lubricant"].subgroup = "liquid-recipe"
data.raw.recipe["fill-crude-oil-barrel"].subgroup = "liquid-recipe2"
data.raw.recipe["empty-crude-oil-barrel"].subgroup = "liquid-recipe2"

--Logistic

data.raw.item["logistic-robot"].subgroup = "logistic-robot"
data.raw.item["logistic-robot"].order = "a"
data.raw.item["construction-robot"].subgroup = "logistic-robot-c"
data.raw.item["construction-robot"].order = "a"
data.raw.item["roboport"].subgroup = "logistic-roboport"
data.raw.item["roboport"].order = "a"
data.raw.item["logistic-chest-passive-provider"].subgroup = "logistic-pasive"
data.raw.item["logistic-chest-passive-provider"].order = "a"
data.raw.item["logistic-chest-active-provider"].subgroup = "logistic-active"
data.raw.item["logistic-chest-active-provider"].order = "a"
data.raw.item["logistic-chest-storage"].subgroup = "logistic-storage"
data.raw.item["logistic-chest-storage"].order = "a"
data.raw.item["logistic-chest-requester"].subgroup = "logistic-requester"
data.raw.item["logistic-chest-requester"].order = "a"
data.raw.item["copper-cable"].subgroup = "logistic-wire"
data.raw.item["copper-cable"].order = "a"
data.raw.item["red-wire"].subgroup = "logistic-wire"
data.raw.item["red-wire"].order = "b"
data.raw.item["green-wire"].subgroup = "logistic-wire"
data.raw.item["green-wire"].order = "c"
data.raw.item["basic-beacon"].subgroup = "logistic-beacon"
data.raw.item["basic-beacon"].order = "a"
data.raw.item["arithmetic-combinator"].subgroup = "logistic-comb"
data.raw.item["arithmetic-combinator"].order = "a"
data.raw.item["decider-combinator"].subgroup = "logistic-comb"
data.raw.item["decider-combinator"].order = "b"
data.raw.item["constant-combinator"].subgroup = "logistic-comb"
data.raw.item["constant-combinator"].order = "c"
data.raw["deconstruction-item"]["deconstruction-planner"].subgroup = "logistic-plan"
data.raw["blueprint"]["blueprint"].subgroup = "logistic-plan"

--Mining

data.raw.item["basic-mining-drill"].subgroup = "mining-speed"
data.raw.item["basic-mining-drill"].order = "b"
data.raw.item["burner-mining-drill"].subgroup = "mining-speed"
data.raw.item["burner-mining-drill"].order = "a"
data.raw["mining-tool"]["iron-axe"].subgroup = "pick"
data.raw["mining-tool"]["steel-axe"].subgroup = "pick"
data.raw["repair-tool"]["repair-pack"].subgroup = "repair"
data.raw["repair-tool"]["repair-pack"].order = "a"
data.raw.item["lab"].subgroup = "lab"
data.raw.item["lab"].order = "a"

--Module

data.raw.module["speed-module"].subgroup = "speed";
data.raw.module["speed-module"].order = "a";
data.raw.module["speed-module-2"].subgroup = "speed";
data.raw.module["speed-module-2"].order = "b";
data.raw.module["speed-module-3"].subgroup = "speed";
data.raw.module["speed-module-3"].order = "c";
data.raw.module["productivity-module"].subgroup = "productivity";
data.raw.module["productivity-module"].order = "a";
data.raw.module["productivity-module-2"].subgroup = "productivity";
data.raw.module["productivity-module-2"].order = "b";
data.raw.module["productivity-module-3"].subgroup = "productivity";
data.raw.module["productivity-module-3"].order = "c";
data.raw.module["effectivity-module"].subgroup = "effectivity";
data.raw.module["effectivity-module"].order = "a";
data.raw.module["effectivity-module-2"].subgroup = "effectivity";
data.raw.module["effectivity-module-2"].order = "b";
data.raw.module["effectivity-module-3"].subgroup = "effectivity";
data.raw.module["effectivity-module-3"].order = "c";

--Resources

data.raw.item["stone-furnace"].subgroup = "furnace-coal"
data.raw.item["stone-furnace"].order = "a"
data.raw.item["steel-furnace"].subgroup = "furnace-coal"
data.raw.item["steel-furnace"].order = "b"
data.raw.item["electric-furnace"].subgroup = "furnace-electric"
data.raw.item["electric-furnace"].order = "a";

--Plates

data.raw.item["iron-plate"].subgroup = "plates-plates"
data.raw.item["iron-plate"].order = "aa"
data.raw.item["copper-plate"].subgroup = "plates-plates"
data.raw.item["copper-plate"].order = "ab"
data.raw.item["steel-plate"].subgroup = "plates-misc"
data.raw.item["steel-plate"].order = "a"
data.raw.item["sulfur"].subgroup = "plates-misc";
data.raw.item["sulfur"].order = "c"
data.raw.item["plastic-bar"].subgroup = "plates-misc"
data.raw.item["plastic-bar"].order = "b"
data.raw.item["wood"].subgroup = "plates-misc"
data.raw.item["empty-barrel"].subgroup = "plates-misc"
data.raw.item["empty-barrel"].order = "h"
data.raw.recipe["empty-barrel"].subgroup = "plates-misc"
data.raw.recipe["empty-barrel"].order = "h"
data.raw.item["crude-oil-barrel"].subgroup = "plates-misc"
data.raw.item["raw-wood"].subgroup = "plates-misc"
data.raw.item["raw-wood"].order = "d"

--Trains

data.raw.item["train-stop"].subgroup = "trains-misc";
data.raw.item["train-stop"].order = "a";
data.raw.item["rail-signal"].subgroup = "trains-misc";
data.raw.item["rail-signal"].order = "b";
data.raw.item["rail-chain-signal"].subgroup = "trains-misc";
data.raw.item["rail-chain-signal"].order = "c";
data.raw.item["straight-rail"].subgroup = "trains-rails";
data.raw.item["straight-rail"].order = "a";
data.raw.item["curved-rail"].subgroup = "trains-rails";
data.raw.item["curved-rail"].order = "b";
data.raw.item["diesel-locomotive"].subgroup = "trains-locomotive";
data.raw.item["diesel-locomotive"].order = "a";
data.raw.item["cargo-wagon"].subgroup = "trains-rails";
data.raw.item["cargo-wagon"].order = "a";

--Transport

data.raw.item["basic-splitter"].subgroup = "transport-splitters"
data.raw.item["fast-splitter"].subgroup = "transport-splitters"
data.raw.item["express-splitter"].subgroup = "transport-splitters"
data.raw.item["basic-transport-belt"].subgroup = "transport-belt"
data.raw.item["fast-transport-belt"].subgroup = "transport-belt"
data.raw.item["express-transport-belt"].subgroup = "transport-belt"
data.raw.item["basic-transport-belt-to-ground"].subgroup = "transport-ground"
data.raw.item["fast-transport-belt-to-ground"].subgroup = "transport-ground"
data.raw.item["express-transport-belt-to-ground"].subgroup = "transport-ground"
data.raw.item["pipe"].subgroup = "transport-pipe"
data.raw.item["pipe-to-ground"].subgroup = "transport-pipe-ground"
data.raw.item["basic-splitter"].order = "a"
data.raw.item["fast-splitter"].order = "b"
data.raw.item["express-splitter"].order = "c"
data.raw.item["basic-transport-belt"].order = "a"
data.raw.item["fast-transport-belt"].order = "b"
data.raw.item["express-transport-belt"].order = "c"
data.raw.item["basic-transport-belt-to-ground"].order = "a"
data.raw.item["fast-transport-belt-to-ground"].order = "b"
data.raw.item["express-transport-belt-to-ground"].order = "c"
data.raw.item["pipe"].order = "a"
data.raw.item["pipe-to-ground"].order = "a"
data.raw.item["wooden-chest"].subgroup = "store-solid"
data.raw.item["wooden-chest"].order = "a"
data.raw.item["iron-chest"].subgroup = "store-solid"
data.raw.item["iron-chest"].order = "b"
data.raw.item["steel-chest"].subgroup = "store-solid"
data.raw.item["steel-chest"].order = "c"
data.raw.item["smart-chest"].subgroup = "store-solid"
data.raw.item["smart-chest"].order = "d"


--Vehicles

data.raw.item["tank"].subgroup = "vehicles-tank"
data.raw.item["tank"].order = "a"
data.raw.item["car"].subgroup = "vehicles-car"
data.raw.item["car"].order = "a"
data.raw.ammo["cannon-shell"].subgroup = "vehicles-tank"
data.raw.ammo["cannon-shell"].order = "y"
data.raw.ammo["explosive-cannon-shell"].subgroup = "vehicles-tank"
data.raw.ammo["explosive-cannon-shell"].order = "z"