piano_player_sprite =
  {
      filename = "__Piano_Player__/piano_sheet-3.png",
      priority = "low",
      width = 128,
      height = 128,
      frame_count = 25,
      --direction_count = 25,
      line_length = 5,
      shift = {0.5,0.1},
      x = 0,
      y = 0,
      animation_speed = 0.3
    }

blank = 
{
      filename = "__Piano_Player__/piano_sheet-3.png",
      priority = "low",
      width = 1,
      height = 1,
      frame_count = 1,
      --direction_count = 25,
      line_length = 5,
      shift = {0.5,-0.40},
      x = 0,
      y = 0
    }

songlist =
    {
      sound = {
        {
          filename = "__Piano_Player__/rolls/pr_mapleleafrag.ogg",
        },
        {
          filename = "__Piano_Player__/rolls/pr_aragtimeskedaddle.ogg",
        },
        {
          filename = "__Piano_Player__/rolls/pr_bowerybuck.ogg",
        },
        {
          filename = "__Piano_Player__/rolls/pr_originalrags.ogg",
          volume = 1.5,
        },
        {
          filename = "__Piano_Player__/rolls/pr_docbrownscakewalk.ogg",
        },
        {
          filename = "__Piano_Player__/rolls/pr_dillpicklesrag.ogg",
          volume = 1,
        },
        {
          filename = "__Piano_Player__/rolls/pr_palmleafrag.ogg",
          volume = 1,
        },
        {
          filename = "__Piano_Player__/rolls/pr_easywinners.ogg",
          volume = 1.5,
        },
        {
          filename = "__Piano_Player__/rolls/pr_buzzerrag.ogg",
          volume = 1,
        },
        {
          filename = "__Piano_Player__/rolls/pr_blackcatrag.ogg",
          volume = 1,
        },
        {
          filename = "__Piano_Player__/rolls/pr_possumandtaters.ogg",
          volume = 1,
        },
        {
          filename = "__Piano_Player__/rolls/pr_porcupinerag.ogg",
          volume = 1,
        },
        {
          filename = "__Piano_Player__/rolls/pr_sensation.ogg",
          volume = 1,
        },
        {
          filename = "__Piano_Player__/rolls/pr_themusicboxrag.ogg",
          volume = 1,
        },
        {
          filename = "__Piano_Player__/rolls/pr_kansascityrag.ogg",
          volume = 1,
        },

            },
      audible_distance_modifier = 1,
      max_sounds_per_type = 1,
      apparent_volume = 10,
          }


data:extend(
{

{
    type = "item",
    name = "piano-player",
    icon = "__Piano_Player__/piano-icon.png",
    flags = {"goes-to-quickbar"},
    subgroup = "logistic-network",
    order = "c[signal]-b[piano]",
    place_result = "piano-player",
    stack_size = 50
  },

  {
    type = "recipe",
    name = "piano-player",
    ingredients =
    {
      {"electronic-circuit", 1},
      {"iron-gear-wheel", 1},
      {"iron-plate", 1}
    },
    result = "piano-player"
  },
{
    type = "roboport",
    name = "piano-player",
    icon = "__Piano_Player__/piano-icon.png",
    flags = {"placeable-player", "player-creation"},
    minable = {hardness = 0.2, mining_time = 0.5, result = "piano-player"},
    max_health = 100,
    corpse = "big-remnants",
    collision_box = {{-1.2, -1.2}, {1.2, 1.2}},
    selection_box = {{-1.5, -1.5}, {1.5, 1.5}},
    dying_explosion = "medium-explosion",
    energy_source =
    {
      type = "electric",
      usage_priority = "secondary-input",
      input_flow_limit = "10MW",
      buffer_capacity = "100MJ"
    },
    recharge_minimum = "10MJ",
    energy_usage = "500kW",
    -- per one charge slot
    charging_energy = "500kW",
    logistics_radius = 0,
    construction_radius = 0,
    charge_approach_distance = 2,
    robot_slots_count = 0,
    material_slots_count = 0,
    stationing_offset = {0, 0},
    charging_offsets =
    {
      {-1.6, -1}, {-0.8, -1}, {0, -1}, {0.8, -1}, {1.6, -1},
    },
    base = blank,
    base_patch = blank,
    base_animation = piano_player_sprite,
    door_animation_up = blank,
    door_animation_down = blank,
    recharging_animation = blank,
    vehicle_impact_sound =  {
    { filename = "__Piano_Player__/sounds/piano-crash-1.ogg"},
    { filename = "__Piano_Player__/sounds/piano-crash-2.ogg"},
    { filename = "__Piano_Player__/sounds/piano-crash-3.ogg"},
  },
    working_sound = songlist,
    recharging_light = {intensity = 0, size = 0},
    request_to_open_door_timeout = 0,
    spawn_and_station_height = -0.1,
    radius_visualisation_picture = blank,
    construction_radius_visualisation_picture = blank,
    open_door_trigger_effect =
    {
      {
        type = "play-sound",
        sound = { filename = "__base__/sound/roboport-door.ogg", volume = 0 }
      },
    },
    close_door_trigger_effect =
    {
      {
        type = "play-sound",
        sound = { filename = "__base__/sound/roboport-door.ogg", volume = 0 }
      },
    },
  },



})
