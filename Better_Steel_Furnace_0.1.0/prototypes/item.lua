data:extend({
{
    type = "item",
    name = "better-steel-furnace",
    icon = "__Better_Steel_Furnace__/steel-furnace.png",
    flags = {"goes-to-quickbar"},
    subgroup = "smelting-machine",
    order = "b[better-steel-furnace]",
    place_result = "better-steel-furnace",
    stack_size = 20
  },
  {
    type = "item",
    name = "better-electric-furnace",
    icon = "__Better_Steel_Furnace__/graphics/electric-furnace.png",
    flags = {"goes-to-quickbar"},
    subgroup = "smelting-machine",
    order = "c[better-electric-furnace]",
    place_result = "better-electric-furnace",
    stack_size = 20
  }
 }) 