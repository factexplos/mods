data:extend({

{
    type = "recipe",
    name = "better-steel-furnace",
    ingredients = {{"steel-plate", 11}, {"stone-brick", 12}},
    result = "better-steel-furnace",
    energy_required = 5,
    enabled = false
  },
  {
    type = "recipe",
    name = "better-electric-furnace",
    ingredients = {{"steel-plate", 17}, {"advanced-circuit", 6}, {"stone-brick", 11}},
    result = "better-electric-furnace",
    energy_required = 5,
    enabled = false
  }
})