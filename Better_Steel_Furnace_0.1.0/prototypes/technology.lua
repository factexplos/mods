data:extend({
{
    type = "technology",
    name = "advanced-steel-processing",
    icon = "__base__/graphics/technology/advanced-material-processing.png",
    effects =
    {
      {
        type = "unlock-recipe",
        recipe = "better-steel-furnace",
      },
	  {
	    type = "unlock-recipe",
		recipe = "better-electric-furnace"
	  }
    },
    prerequisites = {"advanced-material-processing"},
    unit =
    {
      count = 70,
      ingredients =
      {
        {"science-pack-1", 1},
        {"science-pack-2", 1},
      },
      time = 30
    },
    order = "c-c-a"
  
}
  })