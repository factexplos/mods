lead =
{
  name = "lead-ore",
  icon = "__5dim_ores__/graphics/icon/lead-ore.png",
  hardness = 0.7,
  mining_time = 1.5,
  tint = {r = 0.5, g = 0.5, b = 0.5},
  map_color = {r=0.250, g=0.250, b=0.250},
  item =
  {
    create = true,
    stack_size = 200
  },
  stage_mult = 10,
  sprite =
  {
    sheet = 1
  },
  enabled = true,
  autoplace =
  {
    control = "lead-ore",
    sharpness = 1,
    richness_multiplier = 12000,
    richness_base = 300,
    size_control_multiplier = 0.06,
    peaks =
    {
      {
        influence = 0.2,
      },
      {
        influence = 0.65,
        noise_layer = "lead-ore",
        noise_octaves_difference = -2.4,
        noise_persistence = 0.35,
        starting_area_weight_optimal = 0,
        starting_area_weight_range = 0,
        starting_area_weight_max_range = 2,
      },
      {
        influence = 0.7,
        noise_layer = "lead-ore",
        noise_octaves_difference = -4,
        noise_persistence = 0.45,
        starting_area_weight_optimal = 1,
        starting_area_weight_range = 0,
        starting_area_weight_max_range = 2,
      },
    }
  }
}
  data:extend({
-- Item
  {
    type = "item",
    name = "lead-plate",
    icon = "__5dim_core__/graphics/icon/lead-plate.png",
    flags = {"goes-to-main-inventory"},
    subgroup = "plates-plates",
    order = "ad",
    stack_size = 200
  },

--Recipe
  {
    type = "recipe",
    name = "5d-lead-plate",
    category = "smelting",
    energy_required = 2,
    ingredients = {{ "lead-ore", 3}},
    result = "lead-plate"
  },
})