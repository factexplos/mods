data:extend({
  {
    type = "recipe",
    name = "5d-electric-pole-6",
    enabled = "false",
    ingredients =
    {
      {"advanced-circuit", 20},
      {"processing-unit", 1},
      {"steel-plate", 100},
      {"iron-plate", 100},
      {"5d-gold-wire", 5}
    },
    result = "5d-electric-pole-6"
  },
  {
    type = "recipe",
    name = "5d-electric-pole-5",
    enabled = "false",
    ingredients =
    {
      {"electronic-circuit", 2},
      {"steel-plate", 4},
      {"aluminium-plate", 4},
      {"copper-plate", 4}
    },
    result = "5d-electric-pole-5"
  },
  {
    type = "recipe",
    name = "5d-electric-pole-4",
    enabled = "false",
    ingredients =
    {
      {"steel-plate", 4},
      {"iron-plate", 4},
      {"copper-plate", 4}
    },
    result = "5d-electric-pole-4"
  },
  {
    type = "recipe",
    name = "5d-offshore-pump-2",
    enabled = "false",
    ingredients =
    {
      {"aluminium-plate", 20},
      {"lead-plate", 20},
      {"pipe", 10},
      {"offshore-pump", 1},
    },
    result = "5d-offshore-pump-2"
  },
  {
    type = "recipe",
    name = "5d-offshore-pump",
    enabled = "false",
    ingredients =
    {
      {"aluminium-plate", 20},
      {"zinc-plate", 20},
      {"steel-plate", 20},
      {"pipe", 10},
      {"offshore-pump", 1},
    },
    result = "5d-offshore-pump"
  },
  {
    type = "recipe",
    name = "5d-lamp",
    enabled = "false",
    ingredients =
    {
      {"electronic-circuit", 5},
      {"small-lamp", 1},
      {"iron-plate", 5},
      {"steel-plate", 2},
    },
    result = "5d-lamp"
  },
  {
    type = "recipe",
    name = "5d-boiler-2",
    enabled = "false",
    ingredients = 
	{
		{"zinc-plate", 20},
		{"5d-boiler", 1},
		{"pipe", 3},
	},
    result = "5d-boiler-2"
  },
  {
    type = "recipe",
    name = "5d-boiler",
    enabled = "false",
    ingredients = 
	{
		{"steel-furnace", 1},
		{"boiler", 1},
		{"pipe", 3},
	},
    result = "5d-boiler"
  },
  {
    type = "recipe",
    name = "5d-basic-accumulator-3",
    energy_required = 10,
    enabled = "false",
    ingredients =
    {
      {"lead-plate", 20},
      {"5d-aluminium-wire", 20},
      {"battery", 7},
      {"5d-basic-accumulator-2", 1},
    },
    result = "5d-basic-accumulator-3"
  },
  {
    type = "recipe",
    name = "5d-basic-accumulator-2",
    energy_required = 10,
    enabled = "false",
    ingredients =
    {
      {"lead-plate", 10},
      {"iron-plate", 4},
      {"battery", 7},
      {"basic-accumulator", 1},
    },
    result = "5d-basic-accumulator-2"
  },
  {
    type = "recipe",
    name = "5d-steam-engine-3",
    enabled = "false",
    ingredients =
    {
      {"5d-gold-wire", 5},
      {"5d-aluminium-gear-wheel", 12},
      {"pipe", 12},
      {"zinc-plate", 12},
      {"5d-steam-engine-2", 1},
    },
    result = "5d-steam-engine-3"
  },
  {
    type = "recipe",
    name = "5d-steam-engine-2",
    enabled = "false",
    ingredients =
    {
      {"electronic-circuit", 8},
      {"5d-aluminium-gear-wheel", 5},
      {"pipe", 8},
      {"5d-tin-plate", 15},
      {"steam-engine", 1},
    },
    result = "5d-steam-engine-2"
  },
  {
    type = "recipe",
    name = "5d-solar-panel-3",
    energy_required = 10,
    enabled = "false",
    ingredients =
    {
      {"steel-plate", 10},
      {"5d-gold-wire", 5},
      {"aluminium-plate", 5},
      {"5d-solar-panel-2", 1},
    },
    result = "5d-solar-panel-3"
  },
  {
    type = "recipe",
    name = "5d-solar-panel-2",
    energy_required = 10,
    enabled = "false",
    ingredients =
    {
      {"5d-tin-plate", 30},
      {"electronic-circuit", 15},
      {"aluminium-plate", 15},
      {"solar-panel", 1},
    },
    result = "5d-solar-panel-2"
  },
  {
    type = "recipe",
    name = "5d-small-pump-2",
    energy_required = 2,
    enabled = "false",
    ingredients =
    {
      {"electric-engine-unit", 3},
      {"lead-plate", 50},
      {"5d-small-pump", 1},
      {"pipe", 3}
    },
    result= "5d-small-pump-2"
  },
  {
    type = "recipe",
    name = "5d-small-pump",
    energy_required = 2,
    enabled = "false",
    ingredients =
    {
      {"electric-engine-unit", 2},
      {"zinc-plate", 10},
      {"small-pump", 1},
      {"pipe", 2}
    },
    result= "5d-small-pump"
  },
})