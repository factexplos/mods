data:extend({
  {
    type = "recipe",
    name = "5d-inserter-speed1-range3",
    enabled ="false",
    ingredients =
    {
      {"electronic-circuit", 20},
      {"iron-gear-wheel", 20},
      {"iron-plate", 1}
    },
    result = "5d-inserter-speed1-range3"
  },
  {
    type = "recipe",
    name = "5d-inserter-speed1-range2-close",
    enabled ="false",
    ingredients =
    {
      {"long-handed-inserter", 1},
      {"electronic-circuit", 5},
      {"iron-gear-wheel", 5},
    },
    result = "5d-inserter-speed1-range2-close"
  },
  {
    type = "recipe",
    name = "5d-inserter-speed1-range1-close",
    enabled ="false",
    ingredients =
    {
      {"basic-inserter", 1},
      {"iron-gear-wheel", 5},
      {"electronic-circuit", 5}
    },
    result = "5d-inserter-speed1-range1-close"
  },
  {
    type = "recipe",
    name = "5d-inserter-smart-speed3-range1-close",
    enabled ="false",
    ingredients =
    {
      {"5d-inserter-speed3-range1", 1},
      {"electronic-circuit", 5},
      {"iron-gear-wheel", 5},
    },
    result = "5d-inserter-smart-speed3-range1-close"
  },
  {
    type = "recipe",
    name = "5d-inserter-smart-speed3-range1",
    enabled ="false",
    ingredients =
    {
      {"electronic-circuit", 5},
      {"fast-inserter", 1}
    },
    result = "5d-inserter-smart-speed3-range1"
  },
  {
    type = "recipe",
    name = "5d-inserter-smart-speed2-range2-close",
    enabled ="false",
    ingredients =
    {
      {"5d-inserter-speed2-range2", 1},
      {"electronic-circuit", 5},
      {"iron-gear-wheel", 5},
    },
    result = "5d-inserter-smart-speed2-range2-close"
  },
  {
    type = "recipe",
    name = "5d-inserter-smart-speed2-range2",
    enabled ="false",
    ingredients =
    {
      {"iron-gear-wheel", 10},
      {"iron-plate", 10},
      {"fast-inserter", 1}
    },
    result = "5d-inserter-smart-speed2-range2"
  },
  {
    type = "recipe",
    name = "5d-inserter-smart-speed2-range1-close",
    enabled ="false",
    ingredients =
    {
      {"fast-inserter", 1},
      {"electronic-circuit", 5},
      {"iron-gear-wheel", 5},
    },
    result = "5d-inserter-smart-speed2-range1-close"
  },
  {
    type = "recipe",
    name = "5d-fast-inserter-right-90d-close",
    enabled ="false",
    ingredients = 
    {
      {"5d-fast-inserter-right-90d", 1},
      {"iron-gear-wheel", 10}
    },
    result = "5d-fast-inserter-right-90d-close"
  },
  {
    type = "recipe",
    name = "5d-fast-inserter-right-90d",
    enabled ="false",
    ingredients = 
    {
      {"fast-inserter", 1},
      {"electronic-circuit", 5}
    },
    result = "5d-fast-inserter-right-90d"
  },
  {
    type = "recipe",
    name = "5d-fast-inserter-left-90d-close",
    enabled ="false",
    ingredients = 
    {
      {"5d-fast-inserter-left-90d", 1},
      {"iron-gear-wheel", 10}
    },
    result = "5d-fast-inserter-left-90d-close"
  },
  {
    type = "recipe",
    name = "5d-fast-inserter-left-90d",
    enabled ="false",
    ingredients = 
    {
      {"fast-inserter", 1},
      {"electronic-circuit", 5}
    },
    result = "5d-fast-inserter-left-90d"
  },
  {
    type = "recipe",
    name = "5d-extreme-inserter-right-90d-close",
    enabled ="false",
    ingredients = 
    {
      {"5d-extreme-inserter-right-90d", 1},
      {"iron-gear-wheel", 10}
    },
    result = "5d-extreme-inserter-right-90d-close"
  },
  {
    type = "recipe",
    name = "5d-extreme-inserter-right-90d",
    enabled ="false",
    ingredients = 
    {
      {"fast-inserter", 1},
      {"electronic-circuit", 5}
    },
    result = "5d-extreme-inserter-right-90d"
  },
  {
    type = "recipe",
    name = "5d-extreme-inserter-left-90d-close",
    enabled ="false",
    ingredients = 
    {
      {"5d-extreme-inserter-left-90d", 1},
      {"iron-gear-wheel", 10}
    },
    result = "5d-extreme-inserter-left-90d-close"
  },
  {
    type = "recipe",
    name = "5d-extreme-inserter-left-90d",
    enabled ="false",
    ingredients = 
    {
      {"fast-inserter", 1},
      {"electronic-circuit", 5}
    },
    result = "5d-extreme-inserter-left-90d"
  },
{
    type = "recipe",
    name = "5d-basic-inserter-right-90d-close",
    enabled ="false",
    ingredients = 
    {
      {"5d-basic-inserter-right-90d", 1},
      {"iron-gear-wheel", 10}
    },
    result = "5d-basic-inserter-right-90d-close",
  },
{
    type = "recipe",
    name = "5d-basic-inserter-right-90d",
    enabled ="false",
    ingredients = 
    {
      {"basic-inserter", 1},
      {"iron-gear-wheel", 1},
      {"electronic-circuit", 1}
    },
    result = "5d-basic-inserter-right-90d",
  },
{
    type = "recipe",
    name = "5d-basic-inserter-left-90d-close",
    enabled ="false",
    ingredients = 
    {
      {"5d-basic-inserter-left-90d", 1},
      {"iron-gear-wheel", 10}
    },
    result = "5d-basic-inserter-left-90d-close",
  },
{
    type = "recipe",
    name = "5d-basic-inserter-left-90d",
    enabled ="false",
    ingredients = 
    {
      {"basic-inserter", 1},
      {"iron-gear-wheel", 1},
      {"electronic-circuit", 1}
    },
    result = "5d-basic-inserter-left-90d",
  },
  {
    type = "recipe",
    name = "5d-chemical-plant-3",
    energy_required = 10,
    enabled = false,
    ingredients =
    {
      {"copper-plate", 50},
      {"5d-tin-plate", 50},
      {"5d-pipe-mk3", 10},
      {"5d-gold-circuit", 5},
      {"5d-chemical-plant-2", 1},
    },
    result= "5d-chemical-plant-3"
  },
  {
    type = "recipe",
    name = "5d-chemical-plant-2",
    energy_required = 10,
    enabled = false,
    ingredients =
    {
      {"steel-plate", 20},
      {"lead-plate", 50},
      {"5d-pipe-mk2", 10},
      {"chemical-plant", 1},
    },
    result= "5d-chemical-plant-2"
  },
  {
    type = "recipe",
    name = "5d-assembling-machine-5",
    enabled = "false",
    ingredients =
    {
      {"speed-module-3", 4},
      {"zinc-plate", 9},
      {"5d-gold-circuit", 3},
      {"5d-assembling-machine-4", 1}
    },
    result = "5d-assembling-machine-5"
  },
  {
    type = "recipe",
    name = "5d-assembling-machine-4",
    enabled = "false",
    ingredients =
    {
      {"speed-module-2", 4},
      {"5d-tin-gear-wheel", 4},
      {"iron-plate", 9},
      {"electronic-circuit", 3},
      {"assembling-machine-3", 1}
    },
    result = "5d-assembling-machine-4"
  },
  {
    type = "recipe",
    name = "5d-oil-refinery-3",
    energy_required = 20,
    ingredients =
    {
      {"steel-plate", 15},
      {"lead-plate", 25},
      {"zinc-plate", 35},
      {"5d-zinc-gear-wheel", 5},
      {"stone-brick", 10},
      {"5d-gold-circuit", 5},
      {"5d-pipe-mk3", 20},
      {"5d-oil-refinery-2", 1},
    },
    result = "5d-oil-refinery-3",
    enabled = false
  },
  {
    type = "recipe",
    name = "5d-oil-refinery-2",
    energy_required = 20,
    ingredients =
    {
      {"steel-plate", 15},
      {"aluminium-plate", 35},
      {"5d-aluminium-gear-wheel", 20},
      {"stone-brick", 5},
      {"electronic-circuit", 20},
      {"5d-pipe-mk2", 15},
      {"oil-refinery", 1},
    },
    result = "5d-oil-refinery-2",
    enabled = false
  },
  {
    type = "recipe",
    name = "5d-inserter-speed3-range3-close",
    enabled ="false",
    ingredients =
    {
      {"5d-inserter-speed3-range3", 1},
      {"electronic-circuit", 5},
      {"iron-gear-wheel", 5},
    },
    result = "5d-inserter-speed3-range3-close"
  },
  {
    type = "recipe",
    name = "5d-inserter-speed3-range3",
    enabled ="false",
    ingredients =
    {
      {"iron-gear-wheel", 20},
      {"iron-plate", 20},
      {"5d-inserter-speed3-range1", 1}
    },
    result = "5d-inserter-speed3-range3"
  },
  {
    type = "recipe",
    name = "5d-inserter-speed3-range2-close",
    enabled ="false",
    ingredients =
	{
      {"5d-inserter-speed3-range2", 1},
      {"electronic-circuit", 5},
      {"iron-gear-wheel", 5},
    },
    result = "5d-inserter-speed3-range2-close"
  },
  {
    type = "recipe",
    name = "5d-inserter-speed3-range2",
    enabled ="false",
    ingredients =
    {
      {"iron-gear-wheel", 10},
      {"iron-plate", 10},
      {"5d-inserter-speed3-range1", 1}
    },
    result = "5d-inserter-speed3-range2"
  },
  {
    type = "recipe",
    name = "5d-inserter-speed3-range1-close",
    enabled ="false",
    ingredients =
    {
      {"5d-inserter-speed3-range1", 1},
      {"electronic-circuit", 5},
      {"iron-gear-wheel", 5},
    },
    result = "5d-inserter-speed3-range1-close"
  },
  {
    type = "recipe",
    name = "5d-inserter-speed3-range1",
    enabled ="false",
    ingredients =
    {
      {"electronic-circuit", 5},
      {"fast-inserter", 1}
    },
    result = "5d-inserter-speed3-range1"
  },
  {
    type = "recipe",
    name = "5d-inserter-speed2-range3-close",
    enabled ="false",
    ingredients =
    {
      {"5d-inserter-speed2-range3", 1},
      {"electronic-circuit", 5},
      {"iron-gear-wheel", 5},
    },
    result = "5d-inserter-speed2-range3-close"
  },
  {
    type = "recipe",
    name = "5d-inserter-speed2-range3",
    enabled ="false",
    ingredients =
    {
      {"iron-gear-wheel", 20},
      {"iron-plate", 20},
      {"fast-inserter", 1}
    },
    result = "5d-inserter-speed2-range3"
  },
  {
    type = "recipe",
    name = "5d-inserter-speed2-range2-close",
    enabled ="false",
    ingredients =
    {
      {"5d-inserter-speed2-range2", 1},
      {"electronic-circuit", 5},
      {"iron-gear-wheel", 5},
    },
    result = "5d-inserter-speed2-range2-close"
  },
  {
    type = "recipe",
    name = "5d-inserter-speed2-range2",
    enabled ="false",
    ingredients =
    {
      {"iron-gear-wheel", 10},
      {"iron-plate", 10},
      {"fast-inserter", 1}
    },
    result = "5d-inserter-speed2-range2"
  },
  {
    type = "recipe",
    name = "5d-inserter-speed2-range1-close",
    enabled ="false",
    ingredients =
    {
      {"fast-inserter", 1},
      {"electronic-circuit", 5},
      {"iron-gear-wheel", 5},
    },
    result = "5d-inserter-speed2-range1-close"
  },
  {
    type = "recipe",
    name = "5d-inserter-speed1-range3-close",
    enabled ="false",
    ingredients =
    {
      {"5d-inserter-speed1-range3", 1},
      {"electronic-circuit", 5},
      {"iron-gear-wheel", 5},
    },
    result = "5d-inserter-speed1-range3-close"
  },
})