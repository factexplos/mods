for i, force in pairs(game.forces) do 
 force.reset_recipes()
 force.reset_technologies()
end

{
 "entity":
 [
  ["energy-condenser", "energy-condenser-new"],
 ]
}