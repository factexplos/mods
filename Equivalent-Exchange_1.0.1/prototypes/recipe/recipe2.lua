data:extend(
	{
		{
			type = "recipe",
			name = "EMC112",
			enabled = "false",
			ingredients = 
				{
					{"stone-wall",1},
				},
			result = "EMC",
			result_count = 10,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-112",
			enabled = "false",
			ingredients = 
				{
					{"EMC",11},
				},
			result = "stone-wall",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC113",
			enabled = "false",
			ingredients = 
				{
					{"gun-turret",1},
				},
			result = "EMC",
			result_count = 87,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-113",
			enabled = "false",
			ingredients = 
				{
					{"EMC",96},
				},
			result = "gun-turret",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC115",
			enabled = "false",
			ingredients = 
				{
					{"stone-furnace",1},
				},
			result = "EMC",
			result_count = 25,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-115",
			enabled = "false",
			ingredients = 
				{
					{"EMC",28},
				},
			result = "stone-furnace",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC116",
			enabled = "false",
			ingredients = 
				{
					{"burner-mining-drill",1},
				},
			result = "EMC",
			result_count = 47,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-116",
			enabled = "false",
			ingredients = 
				{
					{"EMC",52},
				},
			result = "burner-mining-drill",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC117",
			enabled = "false",
			ingredients = 
				{
					{"basic-mining-drill",1},
				},
			result = "EMC",
			result_count = 100,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-117",
			enabled = "false",
			ingredients = 
				{
					{"EMC",110},
				},
			result = "basic-mining-drill",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC118",
			enabled = "false",
			ingredients = 
				{
					{"steel-furnace",1},
				},
			result = "EMC",
			result_count = 300,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-118",
			enabled = "false",
			ingredients = 
				{
					{"EMC",330},
				},
			result = "steel-furnace",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC119",
			enabled = "false",
			ingredients = 
				{
					{"electric-furnace",1},
				},
			result = "EMC",
			result_count = 735,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-119",
			enabled = "false",
			ingredients = 
				{
					{"EMC",809},
				},
			result = "electric-furnace",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC120",
			enabled = "false",
			ingredients = 
				{
					{"assembling-machine-1",1},
				},
			result = "EMC",
			result_count = 95,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-120",
			enabled = "false",
			ingredients = 
				{
					{"EMC",105},
				},
			result = "assembling-machine-1",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC121",
			enabled = "false",
			ingredients = 
				{
					{"assembling-machine-2",1},
				},
			result = "EMC",
			result_count = 190,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-121",
			enabled = "false",
			ingredients = 
				{
					{"EMC",209},
				},
			result = "assembling-machine-2",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC122",
			enabled = "false",
			ingredients = 
				{
					{"assembling-machine-3",1},
				},
			result = "EMC",
			result_count = 1670,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-122",
			enabled = "false",
			ingredients = 
				{
					{"EMC",1837},
				},
			result = "assembling-machine-3",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC123",
			enabled = "false",
			ingredients = 
				{
					{"lab",1},
				},
			result = "EMC",
			result_count = 165,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-123",
			enabled = "false",
			ingredients = 
				{
					{"EMC",182},
				},
			result = "lab",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC124",
			enabled = "false",
			ingredients = 
				{
					{"basic-beacon",1},
				},
			result = "EMC",
			result_count = 1565,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-124",
			enabled = "false",
			ingredients = 
				{
					{"EMC",1722},
				},
			result = "basic-beacon",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC125",
			enabled = "false",
			ingredients = 
				{
					{"radar",1},
				},
			result = "EMC",
			result_count = 125,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-125",
			enabled = "false",
			ingredients = 
				{
					{"EMC",138},
				},
			result = "radar",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC126",
			enabled = "false",
			ingredients = 
				{
					{"small-electric-pole",1},
				},
			result = "EMC",
			result_count = 3,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-126",
			enabled = "false",
			ingredients = 
				{
					{"EMC",3},
				},
			result = "small-electric-pole",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC127",
			enabled = "false",
			ingredients = 
				{
					{"medium-electric-pole",1},
				},
			result = "EMC",
			result_count = 60,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-127",
			enabled = "false",
			ingredients = 
				{
					{"EMC",66},
				},
			result = "medium-electric-pole",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC128",
			enabled = "false",
			ingredients = 
				{
					{"big-electric-pole",1},
				},
			result = "EMC",
			result_count = 150,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-128",
			enabled = "false",
			ingredients = 
				{
					{"EMC",165},
				},
			result = "big-electric-pole",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC129",
			enabled = "false",
			ingredients = 
				{
					{"boiler",1},
				},
			result = "EMC",
			result_count = 30,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-129",
			enabled = "false",
			ingredients = 
				{
					{"EMC",33},
				},
			result = "boiler",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC130",
			enabled = "false",
			ingredients = 
				{
					{"steam-engine",1},
				},
			result = "EMC",
			result_count = 62,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-130",
			enabled = "false",
			ingredients = 
				{
					{"EMC",68},
				},
			result = "steam-engine",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC131",
			enabled = "false",
			ingredients = 
				{
					{"basic-accumulator",1},
				},
			result = "EMC",
			result_count = 165,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-131",
			enabled = "false",
			ingredients = 
				{
					{"EMC",182},
				},
			result = "basic-accumulator",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC132",
			enabled = "false",
			ingredients = 
				{
					{"small-lamp",1},
				},
			result = "EMC",
			result_count = 25,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-132",
			enabled = "false",
			ingredients = 
				{
					{"EMC",28},
				},
			result = "small-lamp",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC133",
			enabled = "false",
			ingredients = 
				{
					{"straight-rail",1},
				},
			result = "EMC",
			result_count = 16,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-133",
			enabled = "false",
			ingredients = 
				{
					{"EMC",18},
				},
			result = "straight-rail",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC134",
			enabled = "false",
			ingredients = 
				{
					{"curved-rail",1},
				},
			result = "EMC",
			result_count = 65,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-134",
			enabled = "false",
			ingredients = 
				{
					{"EMC",72},
				},
			result = "curved-rail",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC135",
			enabled = "false",
			ingredients = 
				{
					{"train-stop",1},
				},
			result = "EMC",
			result_count = 187,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-135",
			enabled = "false",
			ingredients = 
				{
					{"EMC",206},
				},
			result = "train-stop",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC136",
			enabled = "false",
			ingredients = 
				{
					{"rail-signal",1},
				},
			result = "EMC",
			result_count = 37,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-136",
			enabled = "false",
			ingredients = 
				{
					{"EMC",40},
				},
			result = "rail-signal",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC137",
			enabled = "false",
			ingredients = 
				{
					{"diesel-locomotive",1},
				},
			result = "EMC",
			result_count = 875,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-137",
			enabled = "false",
			ingredients = 
				{
					{"EMC",963},
				},
			result = "diesel-locomotive",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC138",
			enabled = "false",
			ingredients = 
				{
					{"cargo-wagon",1},
				},
			result = "EMC",
			result_count = 250,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-138",
			enabled = "false",
			ingredients = 
				{
					{"EMC",275},
				},
			result = "cargo-wagon",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC139",
			enabled = "false",
			ingredients = 
				{
					{"pipe-to-ground",1},
				},
			result = "EMC",
			result_count = 37,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-139",
			enabled = "false",
			ingredients = 
				{
					{"EMC",41},
				},
			result = "pipe-to-ground",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC140",
			enabled = "false",
			ingredients = 
				{
					{"offshore-pump",1},
				},
			result = "EMC",
			result_count = 32,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-140",
			enabled = "false",
			ingredients = 
				{
					{"EMC",35},
				},
			result = "offshore-pump",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC141",
			enabled = "false",
			ingredients = 
				{
					{"storage-tank",1},
				},
			result = "EMC",
			result_count = 225,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-141",
			enabled = "false",
			ingredients = 
				{
					{"EMC",248},
				},
			result = "storage-tank",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC142",
			enabled = "false",
			ingredients = 
				{
					{"oil-refinery",1},
				},
			result = "EMC",
			result_count = 675,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-142",
			enabled = "false",
			ingredients = 
				{
					{"EMC",743},
				},
			result = "oil-refinery",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC143",
			enabled = "false",
			ingredients = 
				{
					{"chemical-plant",1},
				},
			result = "EMC",
			result_count = 225,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-143",
			enabled = "false",
			ingredients = 
				{
					{"EMC",248},
				},
			result = "chemical-plant",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC144",
			enabled = "false",
			ingredients = 
				{
					{"pumpjack",1},
				},
			result = "EMC",
			result_count = 575,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-144",
			enabled = "false",
			ingredients = 
				{
					{"EMC",633},
				},
			result = "pumpjack",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC145",
			enabled = "false",
			ingredients = 
				{
					{"small-pump",1},
				},
			result = "EMC",
			result_count = 98,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-145",
			enabled = "false",
			ingredients = 
				{
					{"EMC",108},
				},
			result = "small-pump",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC146",
			enabled = "false",
			ingredients = 
				{
					{"substation",1},
				},
			result = "EMC",
			result_count = 535,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-146",
			enabled = "false",
			ingredients = 
				{
					{"EMC",589},
				},
			result = "substation",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC147",
			enabled = "false",
			ingredients = 
				{
					{"rail-chain-signal",1},
				},
			result = "EMC",
			result_count = 38,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-147",
			enabled = "false",
			ingredients = 
				{
					{"EMC",42},
				},
			result = "rail-chain-signal",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC148",
			enabled = "false",
			ingredients = 
				{
					{"arithmetic-combinator",1},
				},
			result = "EMC",
			result_count = 75,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-148",
			enabled = "false",
			ingredients = 
				{
					{"EMC",83},
				},
			result = "arithmetic-combinator",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC149",
			enabled = "false",
			ingredients = 
				{
					{"decider-combinator",1},
				},
			result = "EMC",
			result_count = 75,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-149",
			enabled = "false",
			ingredients = 
				{
					{"EMC",83},
				},
			result = "decider-combinator",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC150",
			enabled = "false",
			ingredients = 
				{
					{"constant-combinator",1},
				},
			result = "EMC",
			result_count = 38,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-150",
			enabled = "false",
			ingredients = 
				{
					{"EMC",42},
				},
			result = "constant-combinator",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC151",
			enabled = "false",
			ingredients = 
				{
					{"concrete",1},
				},
			result = "EMC",
			result_count = 7,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-151",
			enabled = "false",
			ingredients = 
				{
					{"EMC",8},
				},
			result = "concrete",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC152",
			enabled = "false",
			ingredients = 
				{
					{"gate",1},
				},
			result = "EMC",
			result_count = 75,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-152",
			enabled = "false",
			ingredients = 
				{
					{"EMC",83},
				},
			result = "gate",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC154",
			enabled = "false",
			ingredients = 
				{
					{"rocket-silo",1},
				},
			result = "EMC",
			result_count = 111700,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-154",
			enabled = "false",
			ingredients = 
				{
					{"EMC",22870},
					{"EMC",50000},
					{"EMC",50000},
				},
			result = "rocket-silo",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC155",
			enabled = "false",
			ingredients = 
				{
					{"personal-roboport-equipment",1},
				},
			result = "EMC",
			result_count = 5588,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-155",
			enabled = "false",
			ingredients = 
				{
					{"EMC",6147},
				},
			result = "personal-roboport-equipment",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC156",
			enabled = "false",
			ingredients = 
				{
					{"energy-condenser-new",1},
				},
			result = "EMC",
			result_count = 683,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-156",
			enabled = "false",
			ingredients = 
				{
					{"EMC",751},
				},
			result = "energy-condenser-new",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC157",
			enabled = "false",
			ingredients = 
				{
					{"low-density-structure",1},
				},
			result = "EMC",
			result_count = 318,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-157",
			enabled = "false",
			ingredients = 
				{
					{"EMC" ,350},
				},
			result = "low-density-structure",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC158",
			enabled = "false",
			ingredients = 
				{
					{"rocket-fuel",1},
				},
			result = "EMC",
			result_count = 60,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-158",
			enabled = "false",
			ingredients = 
				{
					{"EMC",66},
				},
			result = "rocket-fuel",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC159",
			enabled = "false",
			ingredients = 
				{
					{"rocket-control-unit",1},
				},
			result = "EMC",
			result_count = 682,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-159",
			enabled = "false",
			ingredients = 
				{
					{"EMC",750},
				},
			result = "rocket-control-unit",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC160",
			enabled = "false",
			ingredients = 
				{
					{"satellite",1},
				},
			result = "EMC",
			result_count = 121550,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-160",
			enabled = "false",
			ingredients = 
				{
					{"EMC",33705},
					{"EMC",50000},
					{"EMC",50000},
				},
			result = "satellite",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC161",
			enabled = "false",
			ingredients = 
				{
					{"repair-pack",1},
				},
			result = "EMC",
			result_count = 15,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-161",
			enabled = "false",
			ingredients = 
				{
					{"EMC",17},
				},
			result = "repair-pack",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC162",
			enabled = "false",
			ingredients = 
				{
					{"rocket-launcher",1},
				},
			result = "EMC",
			result_count = 100,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-162",
			enabled = "false",
			ingredients = 
				{
					{"EMC",110},
				},
			result = "rocket-launcher",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC163",
			enabled = "false",
			ingredients = 
				{
					{"effectivity-module",1},
				},
			result = "EMC",
			result_count = 333,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-163",
			enabled = "false",
			ingredients = 
				{
					{"EMC",366},
				},
			result = "effectivity-module",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC164",
			enabled = "false",
			ingredients = 
				{
					{"effectivity-module-2",1},
				},
			result = "EMC",
			result_count = 3346,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-164",
			enabled = "false",
			ingredients = 
				{
					{"EMC",3681},
				},
			result = "effectivity-module-2",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC165",
			enabled = "false",
			ingredients = 
				{
					{"effectivity-module-3",1},
				},
			result = "EMC",
			result_count = 18788,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-165",
			enabled = "false",
			ingredients = 
				{
					{"EMC",20666},
				},
			result = "effectivity-module-3",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
	}
)