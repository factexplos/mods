data:extend(						--33 between 23 and 24 for filling reasons
	{
		{
			type = "recipe",
			name = "purified-matter-MK1",
			enabled = "false",
			ingredients = 
				{
					{"EMC",100},
				},
			result = "half-pure-matter",
			category = {"EMC",}
		},
		{
			type = "recipe",
			name = "purified-matter-MK2",
			enabled = "false",
			ingredients = 
				{
					{"EMC",10000},
				},
			result = "pure-matter",
			category = "EMC"
		},
		{
			type = "recipe",
			name = "purified-matter-MK3",
			enabled = "false",
			ingredients = 
				{
					{"EMC",50000},
				},
			result = "compressed-pure-matter",
			category = "EMC"
		},
		{
			type = "recipe",
			name = "energy-condenser",
			enabled = "false",
			ingredients = 
				{
					{"copper-plate", 30},
					{"iron-plate",30},
					{"steel-chest",1},
				},
			result = "energy-condenser-new",
			catagory = "EMC"
		},
		{
			type = "recipe",
			name = "EMC1",
			enabled = "false",
			ingredients = 
				{
					{"raw-wood",1},
				},
			result = "EMC",
			result_count = 1,
			hidden = false,
			category = "items-to-EMC",
			energy_required = 1,
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-1",
			enabled = "false",
			ingredients = 
				{
					{"EMC",1},
				},
			result = "raw-wood",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC2",
			enabled = "false",
			ingredients = 
				{
					{"coal",1},
				},
			result = "EMC",
			result_count = 5,
			hidden = false,
			category = "items-to-EMC",
			energy_required = 1,
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-2",
			enabled = "false",
			ingredients = 
				{
					{"EMC",5},
				},
			result = "coal",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC3",
			enabled = "false",
			ingredients = 
				{
					{"iron-ore",1},
				},
			result = "EMC",
			result_count = 5,
			hidden = false,
			category = "items-to-EMC",
			energy_required = 1,
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-3",
			enabled = "false",
			ingredients = 
				{
					{"EMC",5},
				},
			result = "iron-ore",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC4",
			enabled = "false",
			ingredients = 
				{
					{"copper-ore",1},
				},
			result = "EMC",
			result_count = 5,
			hidden = false,
			category = "items-to-EMC",
			energy_required = 1,
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-4",
			enabled = "false",
			ingredients = 
				{
					{"EMC",5},
				},
			result = "copper-ore",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC5",
			enabled = "false",
			ingredients = 
				{
					{"stone",1},
				},
			result = "EMC",
			result_count = 5,
			hidden = false,
			category = "items-to-EMC",
			energy_required = 1,
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-5",
			enabled = "false",
			ingredients = 
				{
					{"EMC",5},
				},
			result = "stone",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC6",
			enabled = "false",
			ingredients = 
				{
					{"raw-fish",1},
				},
			result = "EMC",
			result_count = 1,
			hidden = false,
			category = "items-to-EMC",
			energy_required = 1,
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-6",
			enabled = "false",
			ingredients = 
				{
					{"EMC",1},
				},
			result = "raw-fish",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC8",
			enabled = "false",
			ingredients = 
				{
					{"alien-artifact",1},
				},
			result = "EMC",
			result_count = 100,
			hidden = false,
			category = "items-to-EMC",
			energy_required = 1,
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-8",
			enabled = "false",
			ingredients = 
				{
					{"EMC",100},
				},
			result = "alien-artifact",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC9",
			enabled = "false",
			ingredients = 
				{
					{"wood",1},
				},
			result = "EMC",
			result_count = 1,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-9",
			enabled = "false",
			ingredients = 
				{
					{"EMC",1},
				},
			result = "wood",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC10",
			enabled = "false",
			ingredients = 
				{
					{"iron-plate",1},
				},
			result = "EMC",
			result_count = 5,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-10",
			enabled = "false",
			ingredients = 
				{
					{"EMC",5},
				},
			result = "iron-plate",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC11",
			enabled = "false",
			ingredients = 
				{
					{"copper-plate",1},
				},
			result = "EMC",
			result_count = 5,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-11",
			enabled = "false",
			ingredients = 
				{
					{"EMC",5},
				},
			result = "copper-plate",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items"
		},
		{
			type = "recipe",
			name = "EMC12",
			enabled = "false",
			ingredients = 
				{
					{"steel-plate",1},
				},
			result = "EMC",
			result_count = 25,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-12",
			enabled = "false",
			ingredients = 
				{
					{"EMC",25},
				},
			result = "steel-plate",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC13",
			enabled = "false",
			ingredients = 
				{
					{"stone-brick",1},
				},
			result = "EMC",
			result_count = 10,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-13",
			enabled = "false",
			ingredients = 
				{
					{"EMC",11},
				},
			result = "stone-brick",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC14",
			enabled = "false",
			ingredients = 
				{
					{"iron-stick",1},
				},
			result = "EMC",
			result_count = 2,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-14",
			enabled = "false",
			ingredients = 
				{
					{"EMC",2},
				},
			result = "iron-stick",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC15",
			enabled = "false",
			ingredients = 
				{
					{"iron-gear-wheel",1},
				},
			result = "EMC",
			result_count = 2,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-15",
			enabled = "false",
			ingredients = 
				{
					{"EMC",2},
				},
			result = "iron-gear-wheel",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC16",
			enabled = "false",
			ingredients = 
				{
					{"copper-cable",1},
				},
			result = "EMC",
			result_count = 2,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-16",
			enabled = "false",
			ingredients = 
				{
					{"EMC",2},
				},
			result = "copper-cable",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items"
		},
		{
			type = "recipe",
			name = "EMC17",
			enabled = "false",
			ingredients = 
				{
					{"electronic-circuit", 1},
				},
			result = "EMC",
			result_count = 12,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-17",
			enabled = "false",
			ingredients = 
				{
					{"EMC",13},
				},
			result = "electronic-circuit",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC18",
			enabled = "false",
			ingredients = 
				{
					{"sulfur",1},
				},
			result = "EMC",
			result_count = 7,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-18",
			enabled = "false",
			ingredients = 
				{
					{"EMC",7},
				},
			result = "sulfur",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC19",
			enabled = "false",
			ingredients = 
				{
					{"plastic-bar",1},
				},
			result = "EMC",
			result_count = 8,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-19",
			enabled = "false",
			ingredients = 
				{
					{"EMC",8},
				},
			result = "plastic-bar",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC20",
			enabled = "false",
			ingredients = 
				{
					{"battery",1},
				},
			result = "EMC",
			result_count = 31,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-20",
			enabled = "false",
			ingredients = 
				{
					{"EMC",34},
				},
			result = "battery",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC21",
			enabled = "false",
			ingredients = 
				{
					{"advanced-circuit",1},
				},
			result = "EMC",
			result_count = 52,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-21",
			enabled = "false",
			ingredients = 
				{
					{"EMC",57},
				},
			result = "advanced-circuit",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC22",
			enabled = "false",
			ingredients = 
				{
					{"processing-unit",1},
				},
			result = "EMC",
			result_count = 359,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-22",
			enabled = "false",
			ingredients = 
				{
					{"EMC",395},
				},
			result = "processing-unit",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC23",
			enabled = "false",
			ingredients = 
				{
					{"pipe", 1},
				},
			result = "EMC",
			result_count = 5,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-23",
			enabled = "false",
			ingredients = 
				{
					{"EMC",5},
				},
			result = "pipe",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC33",
			enabled = "false",
			ingredients = 
				{
					{"engine-unit",1},
				},
			result = "EMC",
			result_count = 37,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-33",
			enabled = "false",
			ingredients = 
				{
					{"EMC",41},
				},
			result = "engine-unit",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC24",
			enabled = "false",
			ingredients = 
				{
					{"electric-engine-unit",1},
				},
			result = "EMC",
			result_count = 68,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-24",
			enabled = "false",
			ingredients = 
				{
					{"EMC",75},
				},
			result = "electric-engine-unit",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC25",
			enabled = "false",
			ingredients = 
				{
					{"flying-robot-frame",1},
				},
			result = "EMC",
			result_count = 193,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-25",
			enabled = "false",
			ingredients = 
				{
					{"EMC",212},
				},
			result = "flying-robot-frame",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC26",
			enabled = "false",
			ingredients = 
				{
					{"science-pack-1",1},
				},
			result = "EMC",
			result_count = 7,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-26",
			enabled = "false",
			ingredients = 
				{
					{"EMC",7},
				},
			result = "science-pack-1",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC27",
			enabled = "false",
			ingredients = 
				{
					{"basic-inserter",1},
				},
			result = "EMC",
			result_count = 20,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-27",
			enabled = "false",
			ingredients = 
				{
					{"EMC",22},
				},
			result = "basic-inserter",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC28",
			enabled = "false",
			ingredients = 
				{
					{"basic-transport-belt",1},
				},
			result = "EMC",
			result_count = 3,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},	
		{
			type = "recipe",
			name = "EMC-to-item-28",
			enabled = "false",
			ingredients = 
				{
					{"EMC",3},
				},
			result = "basic-transport-belt",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC29",
			enabled = "false",
			ingredients = 
				{
					{"science-pack-2",1},
				},
			result = "EMC",
			result_count = 23,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-29",
			enabled = "false",
			ingredients = 
				{
					{"EMC",25},
				},
			result = "science-pack-2",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC30",
			enabled = "false",
			ingredients = 
				{
					{"fast-inserter",1},
				},
			result = "EMC",
			result_count = 55,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-30",
			enabled = "false",
			ingredients = 
				{
					{"EMC",60},
				},
			result = "fast-inserter",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC31",
			enabled = "false",
			ingredients = 
				{
					{"smart-inserter",1},
				},
			result = "EMC",
			result_count = 105,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-31",
			enabled = "false",
			ingredients = 
				{
					{"EMC",115},
				},
			result = "smart-inserter",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC32",
			enabled = "false",
			ingredients = 
				{
					{"science-pack-3",1},
				},
			result = "EMC",
			result_count = 213,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-32",
			enabled = "false",
			ingredients = 
				{
					{"EMC",234},
				},
			result = "science-pack-3",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC34",
			enabled = "false",
			ingredients = 
				{
					{"alien-science-pack",1},
				},
			result = "EMC",
			result_count = 10,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-34",
			enabled = "false",
			ingredients = 
				{
					{"EMC",11},
				},
			result = "alien-science-pack",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC35",
			enabled = "false",
			ingredients = 
				{
					{"empty-barrel",1},
				},
			result = "EMC",
			result_count = 25,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-35",
			enabled = "false",
			ingredients = 
				{
					{"EMC",28},
				},
			result = "empty-barrel",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC36",
			enabled = "false",
			ingredients = 
				{
					{"explosives",1},
				},
			result = "EMC",
			result_count = 13,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-36",
			enabled = "false",
			ingredients = 
				{
					{"EMC",14},
				},
			result = "explosives",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC37",
			enabled = "false",
			ingredients = 
				{
					{"iron-axe",1},
				},
			result = "EMC",
			result_count = 20,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-37",
			enabled = "false",
			ingredients = 
				{
					{"EMC",22},
				},
			result = "iron-axe",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC38",
			enabled = "false",
			ingredients = 
				{
					{"steel-axe",1},
				},
			result = "EMC",
			result_count = 130,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-38",
			enabled = "false",
			ingredients = 
				{
					{"EMC",141},
				},
			result = "steel-axe",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC39",
			enabled = "false",
			ingredients = 
				{
					{"pistol",1},
				},
			result = "EMC",
			result_count = 50,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-39",
			enabled = "false",
			ingredients = 
				{
					{"EMC",55},
				},
			result = "pistol",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC40",
			enabled = "false",
			ingredients = 
				{
					{"submachine-gun",1},
				},
			result = "EMC",
			result_count = 100,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-40",
			enabled = "false",
			ingredients = 
				{
					{"EMC",110},
				},
			result = "submachine-gun",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC41",
			enabled = "false",
			ingredients = 
				{
					{"flame-thrower",1},
				},
			result = "EMC",
			result_count = 150,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-41",
			enabled = "false",
			ingredients = 
				{
					{"EMC",165},
				},
			result = "flame-thrower",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC42",
			enabled = "false",
			ingredients = 
				{
					{"land-mine",1},
				},
			result = "EMC",
			result_count = 13,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-42",
			enabled = "false",
			ingredients = 
				{
					{"EMC",14},
				},
			result = "land-mine",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC43",
			enabled = "false",
			ingredients = 
				{
					{"shotgun",1},
				},
			result = "EMC",
			result_count = 140,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-43",
			enabled = "false",
			ingredients = 
				{
					{"EMC",154},
				},
			result = "shotgun",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC44",
			enabled = "false",
			ingredients = 
				{
					{"combat-shotgun",1},
				},
			result = "EMC",
			result_count = 442,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-44",
			enabled = "false",
			ingredients = 
				{
					{"EMC",486},
				},
			result = "combat-shotgun",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC45",
			enabled = "false",
			ingredients = 
				{
					{"basic-grenade",1},
				},
			result = "EMC",
			result_count = 75,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-45",
			enabled = "false",
			ingredients = 
				{
					{"EMC",83},
				},
			result = "basic-grenade",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC46",
			enabled = "false",
			ingredients = 
				{
					{"piercing-bullet-magazine",1},
				},
			result = "EMC",
			result_count = 50,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-46",
			enabled = "false",
			ingredients = 
				{
					{"EMC",55},
				},
			result = "piercing-bullet-magazine",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC47",
			enabled = "false",
			ingredients = 
				{
					{"defender-capsule",1},
				},
			result = "EMC",
			result_count = 82,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-47",
			enabled = "false",
			ingredients = 
				{
					{"EMC",90},
				},
			result = "defender-capsule",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC48",
			enabled = "false",
			ingredients = 
				{
					{"poison-capsule",1},
				},
			result = "EMC",
			result_count = 162,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-48",
			enabled = "false",
			ingredients = 
				{
					{"EMC",178},
				},
			result = "poison-capsule",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC49",
			enabled = "false",
			ingredients = 
				{
					{"slowdown-capsule",1},
				},
			result = "EMC",
			result_count = 100,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-49",
			enabled = "false",
			ingredients = 
				{
					{"EMC",110},
				},
			result = "slowdown-capsule",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC50",
			enabled = "false",
			ingredients = 
				{
					{"distractor-capsule",1},
				},
			result = "EMC",
			result_count = 486,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-50",
			enabled = "false",
			ingredients = 
				{
					{"EMC",535},
				},
			result = "distractor-capsule",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC51",
			enabled = "false",
			ingredients = 
				{
					{"speed-module",1},
				},
			result = "EMC",
			result_count = 322,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-51",
			enabled = "false",
			ingredients = 
				{
					{"EMC",354},
				},
			result = "speed-module",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC52",
			enabled = "false",
			ingredients = 
				{
					{"destroyer-capsule",1},
				},
			result = "EMC",
			result_count = 2266,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-52",
			enabled = "false",
			ingredients = 
				{
					{"EMC",2493},
				},
			result = "destroyer-capsule",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC53",
			enabled = "false",
			ingredients = 
				{
					{"basic-electric-discharge-defense-remote",1},
				},
			result = "EMC",
			result_count = 12,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-53",
			enabled = "false",
			ingredients = 
				{
					{"EMC",13},
				},
			result = "basic-electric-discharge-defense-remote",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC54",
			enabled = "false",
			ingredients = 
				{
					{"tank",1},
				},
			result = "EMC",
			result_count = 2147,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-54",
			enabled = "false",
			ingredients = 
				{
					{"EMC",2391},
				},
			result = "tank",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC55",
			enabled = "false",
			ingredients = 
				{
					{"basic-bullet-magazine",1},
				},
			result = "EMC",
			result_count = 10,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-55",
			enabled = "false",
			ingredients = 
				{
					{"EMC",11},
				},
			result = "basic-bullet-magazine",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC56",
			enabled = "false",
			ingredients = 
				{
					{"shotgun-shell",1},
				},
			result = "EMC",
			result_count = 20,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-56",
			enabled = "false",
			ingredients = 
				{
					{"EMC",22},
				},
			result = "shotgun-shell",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC57",
			enabled = "false",
			ingredients = 
				{
					{"piercing-shotgun-shell",1},
				},
			result = "EMC",
			result_count = 60,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-57",
			enabled = "false",
			ingredients = 
				{
					{"EMC",66},
				},
			result = "piercing-shotgun-shell",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC58",
			enabled = "false",
			ingredients = 
				{
					{"rocket",1},
				},
			result = "EMC",
			result_count = 49,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-58",
			enabled = "false",
			ingredients = 
				{
					{"EMC",54},
				},
			result = "rocket",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC59",
			enabled = "false",
			ingredients = 
				{
					{"explosive-rocket",1},
				},
			result = "EMC",
			result_count = 117,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-59",
			enabled = "false",
			ingredients = 
				{
					{"EMC",129},
				},
			result = "explosive-rocket",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC60",
			enabled = "false",
			ingredients = 
				{
					{"flame-thrower-ammo",1},
				},
			result = "EMC",
			result_count = 40,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-60",
			enabled = "false",
			ingredients = 
				{
					{"EMC",44},
				},
			result = "flame-thrower-ammo",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC61",
			enabled = "false",
			ingredients = 
				{
					{"cannon-shell",1},
				},
			result = "EMC",
			result_count = 130,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-61",
			enabled = "false",
			ingredients = 
				{
					{"EMC",143},
				},
			result = "cannon-shell",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC62",
			enabled = "false",
			ingredients = 
				{
					{"basic-armor",1},
				},
			result = "EMC",
			result_count = 200,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-62",
			enabled = "false",
			ingredients = 
				{
					{"EMC",220},
				},
			result = "basic-armor",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC63",
			enabled = "false",
			ingredients = 
				{
					{"heavy-armor",1},
				},
			result = "EMC",
			result_count = 1750,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-63",
			enabled = "false",
			ingredients = 
				{
					{"EMC",1925},
				},
			result = "heavy-armor",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC64",
			enabled = "false",
			ingredients = 
				{
					{"basic-modular-armor",1},
				},
			result = "EMC",
			result_count = 4606,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-64",
			enabled = "false",
			ingredients = 
				{
					{"EMC",5067},
				},
			result = "basic-modular-armor",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC65",
			enabled = "false",
			ingredients = 
				{
					{"power-armor",1},
				},
			result = "EMC",
			result_count = 41480,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-65",
			enabled = "false",
			ingredients = 
				{
					{"EMC",15928},
				},
			result = "power-armor",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC67",
			enabled = "false",
			ingredients = 
				{
					{"speed-module-2",1},
				},
			result = "EMC",
			result_count = 3346,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-67",
			enabled = "false",
			ingredients = 
				{
					{"EMC",3681},
				},
			result = "speed-module-2",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC68",
			enabled = "false",
			ingredients = 
				{
					{"speed-module-3",1},
				},
			result = "EMC",
			result_count = 18787,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-68",
			enabled = "false",
			ingredients = 
				{
					{"EMC",20666},
				},
			result = "speed-module-3",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC69",
			enabled = "false",
			ingredients = 
				{
					{"power-armor-mk2",1},
				},
			result = "EMC",
			result_count = 249243,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-69",
			enabled = "false",
			ingredients = 
				{
					{"EMC",24167},
					{"EMC",50000},
					{"EMC",50000},
					{"EMC",50000},
					{"EMC",50000},
					{"EMC",50000},
				},
			result = "power-armor-mk2",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC70",
			enabled = "false",
			ingredients = 
				{
					{"night-vision-equipment",1},
				},
			result = "EMC",
			result_count = 510,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-70",
			enabled = "false",
			ingredients = 
				{
					{"EMC",17},
				},
			result = "night-vision-equipment",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC71",
			enabled = "false",
			ingredients = 
				{
					{"battery-equipment",1},
				},
			result = "EMC",
			result_count = 405,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-71",
			enabled = "false",
			ingredients = 
				{
					{"EMC",446},
				},
			result = "battery-equipment",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC72",
			enabled = "false",
			ingredients = 
				{
					{"battery-mk2-equipment",1},
				},
			result = "EMC",
			result_count = 11235,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-72",
			enabled = "false",
			ingredients = 
				{
					{"EMC",12359},
				},
			result = "battery-mk2-equipment",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC73",
			enabled = "false",
			ingredients = 
				{
					{"energy-shield-equipment",1},
				},
			result = "EMC",
			result_count = 510,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-73",
			enabled = "false",
			ingredients = 
				{
					{"EMC",561},
				},
			result = "energy-shield-equipment",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC74",
			enabled = "false",
			ingredients = 
				{
					{"energy-shield-mk2-equipment",1},
				},
			result = "EMC",
			result_count = 8692,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-74",
			enabled = "false",
			ingredients = 
				{
					{"EMC",9561},
				},
			result = "energy-shield-mk2-equipment",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC75",
			enabled = "false",
			ingredients = 
				{
					{"solar-panel",1},
				},
			result = "EMC",
			result_count = 337,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-75",
			enabled = "false",
			ingredients = 
				{
					{"EMC",371},
				},
			result = "solar-panel",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC76",
			enabled = "false",
			ingredients = 
				{
					{"solar-panel-equipment",1},
				},
			result = "EMC",
			result_count = 2171,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-76",
			enabled = "false",
			ingredients = 
				{
					{"EMC",2388},
				},
			result = "solar-panel-equipment",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC77",
			enabled = "false",
			ingredients = 
				{
					{"fusion-reactor-equipment",1},
				},
			result = "EMC",
			result_count = 38925,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-77",
			enabled = "false",
			ingredients = 
				{
					{"EMC",42818},
				},
			result = "fusion-reactor-equipment",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC78",
			enabled = "false",
			ingredients = 
				{
					{"laser-turret",1},
				},
			result = "EMC",
			result_count = 280,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-78",
			enabled = "false",
			ingredients = 
				{
					{"EMC",309},
				},
			result = "laser-turret",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC79",
			enabled = "false",
			ingredients = 
				{
					{"basic-laser-defense-equipment",1},
				},
			result = "EMC",
			result_count = 1886,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-79",
			enabled = "false",
			ingredients = 
				{
					{"EMC",2075},
				},
			result = "basic-laser-defense-equipment",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC80",
			enabled = "false",
			ingredients = 
				{
					{"basic-electric-discharge-defense-equipment",1},
				},
			result = "EMC",
			result_count = 5101,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-80",
			enabled = "false",
			ingredients = 
				{
					{"EMC",5611},
				},
			result = "basic-electric-discharge-defense-equipment",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC81",
			enabled = "false",
			ingredients = 
				{
					{"basic-exoskeleton-equipment",1},
				},
			result = "EMC",
			result_count = 2941,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-81",
			enabled = "false",
			ingredients = 
				{
					{"EMC",3235},
				},
			result = "basic-exoskeleton-equipment",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC82",
			enabled = "false",
			ingredients = 
				{
					{"productivity-module",1},
				},
			result = "EMC",
			result_count = 322,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-82",
			enabled = "false",
			ingredients = 
				{
					{"EMC",354},
				},
			result = "productivity-module",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC83",
			enabled = "false",
			ingredients = 
				{
					{"productivity-module-2",1},
				},
			result = "EMC",
			result_count = 3346,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-83",
			enabled = "false",
			ingredients = 
				{
					{"EMC",3681},
				},
			result = "productivity-module-2",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC84",
			enabled = "false",
			ingredients = 
				{
					{"productivity-module-3",1},
				},
			result = "EMC",
			result_count = 18787,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-84",
			enabled = "false",
			ingredients = 
				{
					{"EMC",20666},
				},
			result = "productivity-module-3",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC85",
			enabled = "false",
			ingredients = 
				{
					{"car",1},
				},
			result = "EMC",
			result_count = 525,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-85",
			enabled = "false",
			ingredients = 
				{
					{"EMC",578},
				},
			result = "car",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC86",
			enabled = "false",
			ingredients = 
				{
					{"red-wire",1},
				},
			result = "EMC",
			result_count = 15,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-86",
			enabled = "false",
			ingredients = 
				{
					{"EMC",17},
				},
			result = "red-wire",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC87",
			enabled = "false",
			ingredients = 
				{
					{"green-wire",1},
				},
			result = "EMC",
			result_count = 15,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-87",
			enabled = "false",
			ingredients = 
				{
					{"EMC",17},
				},
			result = "green-wire",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC88",
			enabled = "false",
			ingredients = 
				{
					{"logistic-robot",1},
				},
			result = "EMC",
			result_count = 297,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-88",
			enabled = "false",
			ingredients = 
				{
					{"EMC",327},
				},
			result = "logistic-robot",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC89",
			enabled = "false",
			ingredients = 
				{
					{"construction-robot",1},
				},
			result = "EMC",
			result_count = 297,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-89",
			enabled = "false",
			ingredients = 
				{
					{"EMC",327},
				},
			result = "construction-robot",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC90",
			enabled = "false",
			ingredients = 
				{
					{"roboport",1},
				},
			result = "EMC",
			result_count = 3577,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-90",
			enabled = "false",
			ingredients = 
				{
					{"EMC",3935},
				},
			result = "roboport",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC91",
			enabled = "false",
			ingredients = 
				{
					{"blueprint",1},
				},
			result = "EMC",
			result_count = 52,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-91",
			enabled = "false",
			ingredients = 
				{
					{"EMC",57},
				},
			result = "blueprint",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC92",
			enabled = "false",
			ingredients = 
				{
					{"deconstruction-planner",1},
				},
			result = "EMC",
			result_count = 52,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-92",
			enabled = "false",
			ingredients = 
				{
					{"EMC",57},
				},
			result = "deconstruction-planner",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC93",
			enabled = "false",
			ingredients = 
				{
					{"solid-fuel",1},
				},
			result = "EMC",
			result_count = 6,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-93",
			enabled = "false",
			ingredients = 
				{
					{"EMC",7},
				},
			result = "solid-fuel",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC94",
			enabled = "false",
			ingredients = 
				{
					{"basic-transport-belt-to-ground",1},
				},
			result = "EMC",
			result_count = 34,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-94",
			enabled = "false",
			ingredients = 
				{
					{"EMC",37},
				},
			result = "basic-transport-belt-to-ground",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC95",
			enabled = "false",
			ingredients = 
				{
					{"fast-transport-belt",1},
				},
			result = "EMC",
			result_count = 16,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-95",
			enabled = "false",
			ingredients = 
				{
					{"EMC",18},
				},
			result = "fast-transport-belt",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC96",
			enabled = "false",
			ingredients = 
				{
					{"fast-transport-belt-to-ground",1},
				},
			result = "EMC",
			result_count = 109,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-96",
			enabled = "false",
			ingredients = 
				{
					{"EMC",120},
				},
			result = "fast-transport-belt-to-ground",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC97",
			enabled = "false",
			ingredients = 
				{
					{"fast-splitter",1},
				},
			result = "EMC",
			result_count = 215,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-97",
			enabled = "false",
			ingredients = 
				{
					{"EMC",237},
				},
			result = "fast-splitter",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC98",
			enabled = "false",
			ingredients = 
				{
					{"basic-splitter",1},
				},
			result = "EMC",
			result_count = 102,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-98",
			enabled = "false",
			ingredients = 
				{
					{"EMC",112},
				},
			result = "basic-splitter",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC99",
			enabled = "false",
			ingredients = 
				{
					{"express-transport-belt",1},
				},
			result = "EMC",
			result_count = 34,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-99",
			enabled = "false",
			ingredients = 
				{
					{"EMC",37},
				},
			result = "express-transport-belt",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC100",
			enabled = "false",
			ingredients = 
				{
					{"express-transport-belt-to-ground",1},
				},
			result = "EMC",
			result_count = 109,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-100",
			enabled = "false",
			ingredients = 
				{
					{"EMC",120},
				},
			result = "express-transport-belt-to-ground",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC101",
			enabled = "false",
			ingredients = 
				{
					{"express-splitter",1},
				},
			result = "EMC",
			result_count = 684,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-101",
			enabled = "false",
			ingredients = 
				{
					{"EMC",752},
				},
			result = "express-splitter",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC102",
			enabled = "false",
			ingredients = 
				{
					{"burner-inserter",1},
				},
			result = "EMC",
			result_count = 7,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-102",
			enabled = "false",
			ingredients = 
				{
					{"EMC",8},
				},
			result = "burner-inserter",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC103",
			enabled = "false",
			ingredients = 
				{
					{"long-handed-inserter",1},
				},
			result = "EMC",
			result_count = 27,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-103",
			enabled = "false",
			ingredients = 
				{
					{"EMC",30},
				},
			result = "long-handed-inserter",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC104",
			enabled = "false",
			ingredients = 
				{
					{"wooden-chest",1},
				},
			result = "EMC",
			result_count = 2,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-104",
			enabled = "false",
			ingredients = 
				{
					{"EMC",2},
				},
			result = "wooden-chest",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC105",
			enabled = "false",
			ingredients = 
				{
					{"iron-chest",1},
				},
			result = "EMC",
			result_count = 40,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-105",
			enabled = "false",
			ingredients = 
				{
					{"EMC",44},
				},
			result = "iron-chest",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC106",
			enabled = "false",
			ingredients = 
				{
					{"steel-chest",1},
				},
			result = "EMC",
			result_count = 200,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-106",
			enabled = "false",
			ingredients = 
				{
					{"EMC",220},
				},
			result = "steel-chest",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC107",
			enabled = "false",
			ingredients = 
				{
					{"smart-chest",1},
				},
			result = "EMC",
			result_count = 237,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-107",
			enabled = "false",
			ingredients = 
				{
					{"EMC",260},
				},
			result = "smart-chest",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC108",
			enabled = "false",
			ingredients = 
				{
					{"logistic-chest-active-provider",1},
				},
			result = "EMC",
			result_count = 289,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-108",
			enabled = "false",
			ingredients = 
				{
					{"EMC",318},
				},
			result = "logistic-chest-active-provider",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC109",
			enabled = "false",
			ingredients = 
				{
					{"logistic-chest-passive-provider",1},
				},
			result = "EMC",
			result_count = 289,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-109",
			enabled = "false",
			ingredients = 
				{
					{"EMC",318},
				},
			result = "logistic-chest-passive-provider",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC110",
			enabled = "false",
			ingredients = 
				{
					{"logistic-chest-storage",1},
				},
			result = "EMC",
			result_count = 289,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-110",
			enabled = "false",
			ingredients = 
				{
					{"EMC",318},
				},
			result = "logistic-chest-storage",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
		{
			type = "recipe",
			name = "EMC111",
			enabled = "false",
			ingredients = 
				{
					{"logistic-chest-requester",1},
				},
			result = "EMC",
			result_count = 289,
			hidden = false,
			category = "items-to-EMC",
			subgroup = "items-to-EMC",
		},
		{
			type = "recipe",
			name = "EMC-to-item-111",
			enabled = "false",
			ingredients = 
				{
					{"EMC",318},
				},
			result = "logistic-chest-requester",
			result_count = 1,
			category = "EMC-to-items",
			subgroup = "EMC-to-items",
		},
	}
)