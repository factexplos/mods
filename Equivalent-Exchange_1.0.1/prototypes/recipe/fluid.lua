data:extend(
	{
		{
			type = "recipe",
			name = "EE-fluid-1",
			enabled = "false",
			ingredients = 
				{
					{type="fluid", name="crude-oil", amount=1},
				},
			result = "EMC",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-fluids-subgroup"
		},
		{
			type = "recipe",
			name = "EE-to-fluid-1",
			enabled = "false",
			ingredients = 
				{
					{type="item", name="EMC", amount=1},
				},
			results=
				{
					{type="fluid", name="crude-oil", amount=1}
				},
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-fluids-subgroup"
		},
		{
			type = "recipe",
			name = "EE-fluid-2",
			enabled = "false",
			ingredients = 
				{
					{type="fluid", name="petroleum-gas", amount=1},
				},
			result = "EMC",
			result_count = 4,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-fluids-subgroup"
		},
		{
			type = "recipe",
			name = "EE-to-fluid-2",
			enabled = "false",
			ingredients = 
				{
					{type="item", name="EMC", amount=4},
				},
		    results=
				{
				  {type="fluid", name="petroleum-gas", amount=1}
				},
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-fluids-subgroup"
		},
		{
			type = "recipe",
			name = "EE-fluid-3",
			enabled = "false",
			ingredients = 
				{
					{type="fluid", name="heavy-oil", amount=1},
				},
			result = "EMC",
			result_count = 3,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-fluids-subgroup"
		},
		{
			type = "recipe",
			name = "EE-to-fluid-3",
			enabled = "false",
			ingredients = 
				{
					{type="item", name="EMC", amount=3},
				},
		    results=
				{
				  {type="fluid", name="heavy-oil", amount=1}
				},
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-fluids-subgroup"
		},
		{
			type = "recipe",
			name = "EE-fluid-4",
			enabled = "false",
			ingredients = 
				{
					{type="fluid", name="light-oil", amount=1},
				},
			result = "EMC",
			result_count = 3,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-fluids-subgroup"
		},
		{
			type = "recipe",
			name = "EE-to-fluid-4",
			enabled = "false",
			ingredients = 
				{
					{type="item", name="EMC", amount=3},
				},
		    results=
				{
				  {type="fluid", name="light-oil", amount=1}
				},
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-fluids-subgroup"
		},
		{
			type = "recipe",
			name = "EE-fluid-5",
			enabled = "false",
			ingredients = 
				{
					{type="fluid", name="lubricant", amount=1},
				},
			result = "EMC",
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-fluids-subgroup"
		},
		{
			type = "recipe",
			name = "EE-to-fluid-5",
			enabled = "false",
			ingredients = 
				{
					{type="item", name="EMC", amount=3},
				},
			results=
				{
				  {type="fluid", name="lubricant", amount=1}
				},
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-fluids-subgroup"
		},
		{
			type = "recipe",
			name = "EE-fluid-6",
			enabled = "false",
			ingredients = 
				{
					{type="fluid", name="water", amount=1},
				},
			result = "EMC",
			result_count = 1,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-fluids-subgroup"
		},
		{
			type = "recipe",
			name = "EE-to-fluid-6",
			enabled = "false",
			ingredients = 
				{
					{type="item", name="EMC", amount=1},
				},
		    results=
				{
				  {type="fluid", name="water", amount=1}
				},
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-fluids-subgroup"
		},
		{
			type = "recipe",
			name = "EE-fluid-7",
			enabled = "false",
			ingredients = 
				{
					{type="fluid", name="sulfuric-acid", amount=1},
				},
			result = "EMC",
			result_count = 11,
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-fluids-subgroup"
		},
		{
			type = "recipe",
			name = "EE-to-fluid-7",
			enabled = "false",
			ingredients = 
				{
					{type="item", name="EMC", amount=11},
				},
		    results=
				{
				  {type="fluid", name="sulfuric-acid", amount=1}
				},
			hidden = false,
			category = "EMC-to-items",
			subgroup = "EMC-to-fluids-subgroup"
		},
	}
)