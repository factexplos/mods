data:extend({
	{
      type = "technology",
      name = "energy-condensing-1",
      icon = "__Equivalent-Exchange__/graphics/tech_energycondenser.png",
	  upgrade = true,
      effects =
      {                                                 --EMC7&33&66&114 is unused
        {
            type = "unlock-recipe",
            recipe = "energy-condenser",
        },
		{
			type = "unlock-recipe",
			recipe = "EMC1",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC2",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC3",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC4",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC5",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC6",
		},
--[[	{
			type = "unlock-recipe",
			recipe = "EMC7",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC8",
		},
--]]	{
			type = "unlock-recipe",
			recipe = "EMC9",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC10",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC11",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC12",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC13",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC14",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC15",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC16",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC17",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC18",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC19",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC20",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC21",
		},
--[[		{
			type = "unlock-recipe",
			recipe = "EMC22",
		},
--]]	{
			type = "unlock-recipe",
			recipe = "EMC23",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC24",
		},
--[[	{
			type = "unlock-recipe",
			recipe = "EMC25",
		},
--]]	{
			type = "unlock-recipe",
			recipe = "EMC26",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC27",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC28",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC29",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC30",
		},
--[[	{
			type = "unlock-recipe",
			recipe = "EMC31",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC32",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC33",
		},
--]]	{
			type = "unlock-recipe",
			recipe = "EMC34",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC35",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC36",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC37",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC38",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC39",
		},
--[[	{
			type = "unlock-recipe",
			recipe = "EMC40",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC41",
		},
--]]	{
			type = "unlock-recipe",
			recipe = "EMC42",
		},
--[[	{
			type = "unlock-recipe",
			recipe = "EMC43",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC44",
		},
--]]	{
--]]	{
			type = "unlock-recipe",
			recipe = "EMC45",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC46",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC47",
		},
--[[	{
			type = "unlock-recipe",
			recipe = "EMC48",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC49",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC50",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC51",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC52",
		},
--]]	{
			type = "unlock-recipe",
			recipe = "EMC53",
		},
--[[	{
			type = "unlock-recipe",
			recipe = "EMC54",
		},
--]]	{
			type = "unlock-recipe",
			recipe = "EMC55",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC56",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC57",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC58",
		},
--[[	{
			type = "unlock-recipe",
			recipe = "EMC59",
		},
--]]	{
			type = "unlock-recipe",
			recipe = "EMC60",
		},
--[[	{
			type = "unlock-recipe",
			recipe = "EMC61",
		},
--]]	{
			type = "unlock-recipe",
			recipe = "EMC62",
		},
--[[	{
			type = "unlock-recipe",
			recipe = "EMC63",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC64",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC65",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC66",
		},
    	{
			type = "unlock-recipe",
			recipe = "EMC67",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC68",
		},
    	{
			type = "unlock-recipe",
			recipe = "EMC69",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC70",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC71",
		},
	    {
			type = "unlock-recipe",
			recipe = "EMC72",
		},
	    {
			type = "unlock-recipe",
			recipe = "EMC73",
		},
    	{
			type = "unlock-recipe",
			recipe = "EMC74",
		},
    	{
			type = "unlock-recipe",
			recipe = "EMC75",
		},
    	{
			type = "unlock-recipe",
			recipe = "EMC76",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC77",
		},
    	{
			type = "unlock-recipe",
			recipe = "EMC78",
		},
    	{
			type = "unlock-recipe",
			recipe = "EMC79",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC80",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC81",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC82",
		},
    	{
			type = "unlock-recipe",
			recipe = "EMC83",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC84",
		},
	    {
			type = "unlock-recipe",
			recipe = "EMC85",
		},
--]]	{
			type = "unlock-recipe",
			recipe = "EMC86",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC87",
		},
--[[	{
			type = "unlock-recipe",
			recipe = "EMC88",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC89",
		},
    	{
			type = "unlock-recipe",
			recipe = "EMC90",
		},
--]]	{
			type = "unlock-recipe",
			recipe = "EMC91",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC92",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC93",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC94",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC95",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC96",
		},
--[[	{
			type = "unlock-recipe",
			recipe = "EMC97",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC98",
		},
--]]	{
			type = "unlock-recipe",
			recipe = "EMC99",
		},
--[[	{
			type = "unlock-recipe",
			recipe = "EMC100",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC101",
		},
--]]	{
			type = "unlock-recipe",
			recipe = "EMC102",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC103",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC104",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC105",
		},
--[[	{
			type = "unlock-recipe",
			recipe = "EMC106",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC107",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC108",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC109",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC110",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC111",
		},
--]]	{
			type = "unlock-recipe",
			recipe = "EMC112",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC113",
		},
--[[		{
			type = "unlock-recipe",
			recipe = "EMC114",
		},
--]]	{
			type = "unlock-recipe",
			recipe = "EMC115",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC116",
		},
--[[	{
			type = "unlock-recipe",
			recipe = "EMC117",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC118",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC119",
		},
--]]	{
			type = "unlock-recipe",
			recipe = "EMC120",
		},
--[[	{
			type = "unlock-recipe",
			recipe = "EMC121",
		},
    	{
			type = "unlock-recipe",
			recipe = "EMC122",
		},
    	{
			type = "unlock-recipe",
			recipe = "EMC123",
		},
    	{
			type = "unlock-recipe",
			recipe = "EMC124",
		},
	    {
			type = "unlock-recipe",
			recipe = "EMC125",
		},
-]]		{
			type = "unlock-recipe",
			recipe = "EMC126",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC127",
		},
--[[	{
			type = "unlock-recipe",
			recipe = "EMC128",
		},
--]]	{
			type = "unlock-recipe",
			recipe = "EMC129",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC130",
		},
--[[	{
			type = "unlock-recipe",
			recipe = "EMC131",
		},
--]]	{
			type = "unlock-recipe",
			recipe = "EMC132",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC133",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC134",
		},
--[[	{
			type = "unlock-recipe",
			recipe = "EMC135",
		},
--]]	{
			type = "unlock-recipe",
			recipe = "EMC136",
		},
--[[	{
			type = "unlock-recipe",
			recipe = "EMC137",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC138",
		},
--]]	{
			type = "unlock-recipe",
			recipe = "EMC139",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC140",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC141",
		},
--[[	{
			type = "unlock-recipe",
			recipe = "EMC142",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC143",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC144",
		},
--]]	{
			type = "unlock-recipe",
			recipe = "EMC145",
		},
--[[	{
			type = "unlock-recipe",
			recipe = "EMC146",
		},
--]]	{
			type = "unlock-recipe",
			recipe = "EMC147",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC148",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC149",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC150",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC151",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC152",
		},
--[[	{
			type = "unlock-recipe",
			recipe = "EMC-to-item-153",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC154",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC155",
		},
--]]	{
			type = "unlock-recipe",
			recipe = "EMC156",
		},
--[[	{
			type = "unlock-recipe",
			recipe = "EMC157",
		},
--]]	{
			type = "unlock-recipe",
			recipe = "EMC158",
		},
--[[	{
			type = "unlock-recipe",
			recipe = "EMC159",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC160",
		},
--]]	{
			type = "unlock-recipe",
			recipe = "EMC161",
		},
--[[	{
			type = "unlock-recipe",
			recipe = "EMC162",
		},
--]]	{
			type = "unlock-recipe",
			recipe = "EMC163",
		},
--[[	{
			type = "unlock-recipe",
			recipe = "EMC164",
		},
		{
			type = "unlock-recipe",
			recipe = "EMC165",
		},
--]]	},
		  prerequisites = {"steel-processing"},
		  unit =
		  {
			count = 50,
			ingredients =
			{
			  {"science-pack-1", 5},
			  {"science-pack-2", 2},
			  {"science-pack-3", 2},
			  {"alien-science-pack", 1}
			},
			time = 20
		  }
	},
	  {
      type = "technology",
      name = "purified-matter-1",
      icon = "__Equivalent-Exchange__/graphics/icon_half-pure-matter.png",
	  upgrade = true,
      effects =
      {
        {
            type = "unlock-recipe",
            recipe = "purified-matter-MK1",
        },
      },
      prerequisites = {"energy-condensing-1"},
	  unit =
      {
        count = 50,
        ingredients =
        {
          {"science-pack-1", 1},
          {"science-pack-2", 1},
          {"science-pack-3", 1},
		  {"alien-science-pack", 1}
        },
        time = 10
      }
	   },
	  {
      type = "technology",
      name = "purified-matter-2",
      icon = "__Equivalent-Exchange__/graphics/icon_pure-matter.png",
	  upgrade = true,
      effects =
      {
        {
            type = "unlock-recipe",
            recipe = "purified-matter-MK2",
        },
      },
      prerequisites = {"purified-matter-1"},
	  unit =
      {
        count = 50,
        ingredients =
        {
          {"science-pack-1", 1},
          {"science-pack-2", 1},
          {"science-pack-3", 1},
		  {"alien-science-pack", 1}
        },
        time = 10
      }
	   },
	   	  {
      type = "technology",
      name = "purified-matter-3",
      icon = "__Equivalent-Exchange__/graphics/icon_compressed-pure-matter.png",
	  upgrade = true,
      effects =
      {
        {
            type = "unlock-recipe",
            recipe = "purified-matter-MK3",
        },
      },
      prerequisites = {"purified-matter-2"},
	  unit =
      {
        count = 50,
        ingredients =
        {
          {"science-pack-1", 1},
          {"science-pack-2", 1},
          {"science-pack-3", 1},
		  {"alien-science-pack", 1}
        },
        time = 10
      }
	   },
	{
      name = "usefulEMC-1",
      type = "technology",
      icon = "__Equivalent-Exchange__/graphics/icon_EMC-to-iron-plate.png",
      effects =
	  {
      {
		type = "unlock-recipe",
		recipe = "EMC-to-item-1",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-2",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-3",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-4",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-5",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-6",
      },
--[[  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-8",
      },
--]]  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-9",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-10",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-11",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-12",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-13",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-14",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-15",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-16",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-17",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-18",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-19",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-20",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-21",
      },
--[[  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-22",
      },
--]]  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-23",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-24",
      },
--[[  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-25",
      },
-]]   {
		type = "unlock-recipe",
		recipe = "EMC-to-item-26",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-27",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-28",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-29",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-30",
      },
--[[  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-31",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-32",
      },
--]]  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-33",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-34",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-35",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-36",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-37",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-38",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-39",
      },
--[[  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-40",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-41",
      },
--]]  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-42",
      },
--[[  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-43",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-44",
      },
--]]  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-45",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-46",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-47",
      },
--[[  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-48",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-49",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-50",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-51",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-52",
      },
--]]  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-53",
      },
--[[  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-54",
      },
--]]  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-55",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-56",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-57",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-58",
      },
--[[  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-59",
      },
--]]  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-60",
      },
--[[  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-61",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-62",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-63",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-64",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-65",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-67",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-68",
      },
      {
		type = "unlock-recipe",
		recipe = "EMC-to-item-69",
      },
      {
		type = "unlock-recipe",
		recipe = "EMC-to-item-70",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-71",
      },
      {
		type = "unlock-recipe",
		recipe = "EMC-to-item-72",
      },
      {
		type = "unlock-recipe",
		recipe = "EMC-to-item-73",
      },
      {
		type = "unlock-recipe",
		recipe = "EMC-to-item-74",
      },
      {
		type = "unlock-recipe",
		recipe = "EMC-to-item-75",
      },
      {
		type = "unlock-recipe",
		recipe = "EMC-to-item-76",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-77",
      },
      {
		type = "unlock-recipe",
		recipe = "EMC-to-item-78",
      },
      {
		type = "unlock-recipe",
		recipe = "EMC-to-item-79",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-80",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-81",
      },
      {
		type = "unlock-recipe",
		recipe = "EMC-to-item-82",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-83",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-84",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-85",
      },
--]]  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-86",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-87",
      },
--[[  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-88",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-89",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-90",
      },
--]]  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-91",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-92",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-93",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-94",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-95",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-96",
      },
--[[  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-97",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-98",
      },
--]]  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-99",
      },
--[[  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-100",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-101",
      },
--]]  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-102",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-103",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-104",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-105",
      },
--[[  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-106",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-107",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-108",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-109",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-110",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-111",
      },
--]]  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-112",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-113",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-115",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-116",
      },
--[[  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-117",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-118",
      },
--]]  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-119",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-120",
      },
--[[  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-121",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-122",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-123",
      },
      {
		type = "unlock-recipe",
		recipe = "EMC-to-item-124",
      },
      {
		type = "unlock-recipe",
		recipe = "EMC-to-item-125",
      },
--]]  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-126",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-127",
      },
--[[  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-128",
      },
--]]  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-129",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-130",
      },
--[[  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-131",
      },
--]]  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-132",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-133",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-134",
      },
--[[  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-135",
      },
--]]  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-136",
      },
--[[  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-137",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-138",
      },
--]]  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-139",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-140",
      },
--[[  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-141",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-142",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-143",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-144",
      },
--]]  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-145",
      },
--[[ {
		type = "unlock-recipe",
		recipe = "EMC-to-item-146",
      },
--]]  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-147",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-148",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-149",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-150",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-151",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-152",
      },
--[[  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-153",
      },
      {
		type = "unlock-recipe",
		recipe = "EMC-to-item-154",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-155",
      },
--]]  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-156",
      },
--[[  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-157",
      },
--]]  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-158",
      },
--[[  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-159",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-160",
      },
--]]  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-161",
      },
--[[  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-162",
      },
--]]  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-163",
      },
--[[  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-164",
      },
	  {
		type = "unlock-recipe",
		recipe = "EMC-to-item-165",
      },
--]]},
      prerequisites = {"energy-condensing-1"},
	  unit =
      {
        count = 50,
        ingredients =
        {
          {"science-pack-1", 20},
          {"science-pack-2", 10},
          {"science-pack-3", 8},
		  {"alien-science-pack", 1}
        },
        time = 20
      }
	},
	{
      type = "technology",
      name = "fluid-to-EMC",
      icon = "__Equivalent-Exchange__/graphics/icon_EMC-fluid-tech.png",
	  upgrade = "true",
      effects =
      {
        {
            type = "unlock-recipe",
            recipe = "EE-fluid-1",
        },
        {
            type = "unlock-recipe",
            recipe = "EE-fluid-2",
        },
		{
            type = "unlock-recipe",
            recipe = "EE-fluid-3",
        },
		{
            type = "unlock-recipe",
            recipe = "EE-fluid-4",
        },
		{
            type = "unlock-recipe",
            recipe = "EE-fluid-5",
        },
		{
            type = "unlock-recipe",
            recipe = "EE-fluid-6",
        },
		{
            type = "unlock-recipe",
            recipe = "EE-fluid-7",
        },
		{
            type = "unlock-recipe",
            recipe = "EE-to-fluid-1",
        },
		{
            type = "unlock-recipe",
            recipe = "EE-to-fluid-2",
        },
		{
            type = "unlock-recipe",
            recipe = "EE-to-fluid-3",
        },
		{
            type = "unlock-recipe",
            recipe = "EE-to-fluid-4",
        },
		{
            type = "unlock-recipe",
            recipe = "EE-to-fluid-5",
        },
		{
            type = "unlock-recipe",
            recipe = "EE-to-fluid-6",
        },
		{
            type = "unlock-recipe",
            recipe = "EE-to-fluid-7",
        },
	  },
      prerequisites = {"energy-condensing-1"},
	  unit =
      {
        count = 50,
        ingredients =
        {
          {"science-pack-1", 1},
          {"science-pack-2", 1},
          {"science-pack-3", 1},
		  {"alien-science-pack", 1}
        },
        time = 10
      }
	},
	{
      type = "technology",
      name = "energy-condensing-2",
      icon = "__Equivalent-Exchange__/graphics/tech_energycondenser.png",
	  upgrade = true,
      effects =
      {
        {
            type = "unlock-recipe",
            recipe = "EMC8",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC22",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC25",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC31",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC32",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC40",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC41",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC43",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC44",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC48",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC49",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC50",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC51",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC59",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC61",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC62",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC70",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC71",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC73",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC75",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC78",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC82",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC85",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC88",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC89",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC98",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC97",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC100",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC101",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC106",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC107",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC108",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC109",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC110",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC111",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC117",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC118",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC119",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC121",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC123",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC125",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC128",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC131",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC135",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC137",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC138",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC141",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC142",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC143",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC144",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC146",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC157",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC159",
        },
      },
      prerequisites = {"energy-condensing-1"},
	  unit =
      {
        count = 50,
        ingredients =
        {
          {"science-pack-1", 1},
          {"science-pack-2", 1},
          {"science-pack-3", 1},
		  {"alien-science-pack", 1}
        },
        time = 10
      }
	},
		{
      name = "usefulEMC-2",
      type = "technology",
      icon = "__Equivalent-Exchange__/graphics/icon_EMC-to-iron-plate.png",
	  upgrade = true,
      effects =
	  {
	    {
            type = "unlock-recipe",
            recipe = "EMC-to-item-8",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-22",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-25",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-31",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-32",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-40",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-41",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-43",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-44",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-48",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-49",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-50",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-51",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-59",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-61",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-62",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-70",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-71",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-73",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-75",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-78",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-82",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-85",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-88",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-89",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-98",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-97",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-100",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-101",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-106",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-107",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-108",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-109",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-110",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-111",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-117",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-118",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-119",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-121",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-123",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-125",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-128",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-131",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-135",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-137",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-138",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-141",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-142",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-143",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-144",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-146",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-157",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-159",
        },
	  },
      prerequisites = {"usefulEMC-1","energy-condensing-2"},
	  unit =
      {
        count = 50,
        ingredients =
        {
          {"science-pack-1", 20},
          {"science-pack-2", 10},
          {"science-pack-3", 8},
		  {"alien-science-pack", 1}
        },
        time = 20
      }
	},
	{
      name = "usefulEMC-3",
      type = "technology",
      icon = "__Equivalent-Exchange__/graphics/icon_EMC-to-iron-plate.png",
	  upgrade = true,
      effects =
	  {
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-52",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-54",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-63",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-64",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-65",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-67",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-68",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-164",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-165",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-69",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-72",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-74",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-76",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-77",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-79",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-80",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-81",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-83",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-84",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-90",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-122",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-124",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-154",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-155",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC-to-item-160",
        },
	  },
      prerequisites = {"usefulEMC-2","energy-condensing-3"},
	  unit =
      {
        count = 50,
        ingredients =
        {
          {"science-pack-1", 20},
          {"science-pack-2", 10},
          {"science-pack-3", 8},
		  {"alien-science-pack", 1}
        },
        time = 20
      }
	},
	{
      type = "technology",
      name = "energy-condensing-3",
      icon = "__Equivalent-Exchange__/graphics/tech_energycondenser.png",
	  upgrade = true,
      effects =
      {
        {
            type = "unlock-recipe",
            recipe = "EMC52",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC54",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC63",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC64",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC65",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC67",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC68",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC164",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC165",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC69",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC72",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC74",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC76",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC77",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC79",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC80",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC81",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC83",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC84",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC90",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC122",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC124",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC154",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC155",
        },
        {
            type = "unlock-recipe",
            recipe = "EMC160",
        },
      },
      prerequisites = {"energy-condensing-2"},
	  unit =
      {
        count = 50,
        ingredients =
        {
          {"science-pack-1", 1},
          {"science-pack-2", 1},
          {"science-pack-3", 1},
		  {"alien-science-pack", 1}
        },
        time = 10
      }
	},
	}
)