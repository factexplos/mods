data:extend({
  {
	type = "item-group",
	name = "EMC",
	icon = "__Equivalent-Exchange__/graphics/icon_EMC64.png",
	inventory_order = "e",
	order = "e"
  },
  {
	type = "item-subgroup",
	name = "EMC",
	icon = "__Equivalent-Exchange__/graphics/icon_EMC64.png",
	group = "EMC",
	inventory_order = "e",
	order = "e"
  },
  {
	type = "item-subgroup",
	name = "EMC-to-fluids-subgroup",
	group = "EMC-to-fluids",
	order = "a"
  },
  {
	type = "item-group",
	name = "EMC-to-fluids",
	icon = "__Equivalent-Exchange__/graphics/icon_EMC-fluid-tech.png",
	inventory_order = "f",
	order = "f"
  },
  {
    type = "item-subgroup",
    name = "items-to-EMC",
    group = "items-to-EMC",
    order = "g"
  },
  {
	type = "item-group",
	name = "items-to-EMC",
	icon = "__Equivalent-Exchange__/graphics/icon_EMC64.png",
	inventory_order = "g",
	order = "g"
  },
  {
    type = "item-subgroup",
    name = "EMC-to-items",
    group = "EMC-to-items",
    order = "g"
  },
  {
	type = "item-group",
	name = "EMC-to-items",
	icon = "__Equivalent-Exchange__/graphics/icon_EMC-to-iron-plate.png",
	inventory_order = "g",
	order = "g"
  },
 }
 )