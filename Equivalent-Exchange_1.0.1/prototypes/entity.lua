--require ("liquidEE")
pipecoverspictures = function()							--code for this function taken from __base__/prototypes/entitiy/demo-pipecovers (edited for use in this mod)
  return {
    north =
    {
      filename = "__base__/graphics/entity/pipe-covers/pipe-cover-north.png",
      priority = "extra-high",
      width = 44,
      height = 32
    },
    east =
    {
      filename = "__base__/graphics/entity/pipe-covers/pipe-cover-east.png",
      priority = "extra-high",
      width = 32,
      height = 32
    },
    south =
    {
      filename = "__base__/graphics/entity/pipe-covers/pipe-cover-south.png",
      priority = "extra-high",
      width = 46,
      height = 52
    },
    west =
    {
      filename = "__base__/graphics/entity/pipe-covers/pipe-cover-west.png",
      priority = "extra-high",
      width = 32,
      height = 32
    }
  }
end

function energycondenserpipes()				--code for this function taken from __base__/prototypes/entitiy/assemblerpipes (edited for use in this mod)
  return
  {
    north =
    {
      filename = "__base__/graphics/entity/assembling-machine-3/pipe-north.png",
      priority = "extra-high",
      width = 40,
      height = 45,
      shift = {0.03125, 0.3125}
    },
    east =
    {
      filename = "__base__/graphics/entity/assembling-machine-3/pipe-east.png",
      priority = "extra-high",
      width = 40,
      height = 45,
      shift = {-0.78125, 0.15625}
    },
    south =
    {
      filename = "__base__/graphics/entity/assembling-machine-3/pipe-south.png",
      priority = "extra-high",
      width = 40,
      height = 45,
      shift = {0.03125, -1.0625}
    },
    west =
    {
      filename = "__base__/graphics/entity/assembling-machine-3/pipe-west.png",
      priority = "extra-high",
      width = 40,
      height = 45,
      shift = {0.8125, 0}
    }
  }
end

data:extend(
{
{
    type = "assembling-machine",
    name = "energy-condenser-new",
    icon = "__Equivalent-Exchange__/graphics/icon_energy-condenser.png",
    flags = {"placeable-neutral","placeable-player", "player-creation"},
    minable = {hardness = 0.2, mining_time = 0.5, result = "energy-condenser-new"},
    max_health = 300,
    corpse = "small-remnants",
    dying_explosion = "medium-explosion",
    resistances =
    {
      {
        type = "fire",
        percent = 70
      },
	  {
		type = "explosion",
		percent = 80
	   },
    },
    open_sound = { filename = "__base__/sound/machine-open.ogg", volume = 0.85 },
    close_sound = { filename = "__base__/sound/machine-close.ogg", volume = 0.75 },
    vehicle_impact_sound =  { filename = "__base__/sound/car-metal-impact.ogg", volume = 0.65 },
    working_sound =
    {
      sound = {
        {
          filename = "__Equivalent-Exchange__/sounds/work-energy-condenser-1.ogg",
          volume = 0.8
        },
        {
          filename = "__Equivalent-Exchange__/sounds/work-energy-condenser-2.ogg",
          volume = 0.8
        },
      },
      idle_sound = { filename = "__Equivalent-Exchange__/sounds/idle-energy-condenser-1.ogg", volume = 0.5 },
    },
    collision_box = {{-0.35, -0.35}, {0.35, 0.35}},
    selection_box = {{-0.5, -0.5}, {0.5, 0.5}},
	animation =
    {
      north =
      {
        filename = "__Equivalent-Exchange__/graphics/icon_energy-condenser.png",
		width = 48,
		height = 34,
		shift = {0.2, 0},
        frame_count = 1,
      },
      east =
      {
        filename = "__Equivalent-Exchange__/graphics/icon_energy-condenser.png",
		width = 48,
		height = 34,
		shift = {0.2, 0},
        frame_count = 1,
      },
      south =
      {
        filename = "__Equivalent-Exchange__/graphics/icon_energy-condenser.png",
		width = 48,
		height = 34,
		shift = {0.2, 0},
        frame_count = 1,
      },
      west =
      {
        filename = "__Equivalent-Exchange__/graphics/icon_energy-condenser.png",
		width = 48,
		height = 34,
		shift = {0.2, 0},
        frame_count = 1,
      }
    },    
	crafting_categories = {"items-to-EMC", "EMC-to-items", "EE-normal", "EE-fluid"},
    crafting_speed = 2000.0,
    energy_source =
    {
      type = "electric",
      usage_priority = "secondary-input",
      emissions = 0 / 0
    },
    energy_usage = "50kW",
    ingredient_count = 6,
	fluid_boxes =
    {
      {
        production_type = "input",
        pipe_picture = energycondenserpipes(),
        pipe_covers = pipecoverspictures(),
        base_area = 10,
        base_level = -1,
        pipe_connections = {{ type="input", position = {0, -1} }}
      },
      {
        production_type = "output",
        pipe_picture = energycondenserpipes(),
        pipe_covers = pipecoverspictures(),
        base_area = 10,
        base_level = 1,
        pipe_connections = {{ type="output", position = {0, 1} }}
      },
      off_when_no_fluid_recipe = true
    },
},
  }
	)