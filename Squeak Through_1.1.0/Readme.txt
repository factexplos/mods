Squeak Through 1.1.0
====================

Version 1.1.0 was released April 10, 2016, was tested using Factorio v0.12.30. Initially conceived by Supercheese, the mod now primarily features code by Nommy, with contribution from Lupin.

This small mod reduces the collision boxes for many structures, allowing you to "squeak through" them while walking around your base.
Say goodbye to the frustration of having your path blocked by your steam engines or solar panels when walking around your base!

For a full list of structures altered, see the changelog.


See also the associated forum thread: http://www.factorioforums.com/forum/viewtopic.php?f=91&t=16476
