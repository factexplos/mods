require "config"

require "prototypes.items"
require "prototypes.fluids"
require "prototypes.recipes"

if Config.AllFluidBarrels then require "prototypes.barrels" end

require "prototypes.base-edits"
require "prototypes.technologies"
require "prototypes.groups"