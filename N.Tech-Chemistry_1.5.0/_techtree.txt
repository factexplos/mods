d-a-a		chemistry
d-a-b		advanced-chemistry

d-b-a		oxygen-production
d-b-b		nitrogen-processing
d-b-c		water-purification
d-b-d		electrolysis
d-b-e		carbon-processing

d-c-a		oil-processing
d-c-b		advanced-oil-processing
d-c-c		sulfur-processing
d-c-d		sulfur-oxide-processing
d-c-e		plastics

d-d			fluid-handling