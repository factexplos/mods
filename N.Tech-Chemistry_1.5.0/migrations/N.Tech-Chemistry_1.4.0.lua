for _,force in pairs(game.forces) do
	force.reset_technologies()
	force.reset_recipes()
	
	if Config.Sulfur_Processing_Chain and force.technologies["sulfur-processing"].researched then
		force.recipes["crude-oil-desulphurization"].enabled = true
	end
	if Config.All_Fluid_Barrels and force.technologies["fluid-handling"].researched then
		force.recipes["fill-water-barrel"].enabled = true
		force.recipes["empty-water-barrel"].enabled = true
		force.recipes["fill-light-oil-barrel"].enabled = true
		force.recipes["empty-light-oil-barrel"].enabled = true
		force.recipes["fill-heavy-oil-barrel"].enabled = true
		force.recipes["empty-heavy-oil-barrel"].enabled = true
		force.recipes["fill-petroleum-gas-barrel"].enabled = true
		force.recipes["empty-petroleum-gas-barrel"].enabled = true
		force.recipes["fill-lubricant-barrel"].enabled = true
		force.recipes["empty-lubricant-barrel"].enabled = true
		force.recipes["fill-sulfuric-acid-barrel"].enabled = true
		force.recipes["empty-sulfuric-acid-barrel"].enabled = true
		force.recipes["fill-sulfur-trioxide-barrel"].enabled = true
		force.recipes["empty-sulfur-trioxide-barrel"].enabled = true
		force.recipes["fill-disulfuric-acid-barrel"].enabled = true
		force.recipes["empty-disulfuric-acid-barrel"].enabled = true
	end
end