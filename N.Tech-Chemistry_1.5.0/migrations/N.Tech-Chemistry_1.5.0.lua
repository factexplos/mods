for _,force in pairs(game.forces) do
	force.reset_technologies()
	force.reset_recipes()
	
	if force.technologies["chemistry"].researched then
		force.recipes["carbon-dioxide"].enabled = true
	end
end