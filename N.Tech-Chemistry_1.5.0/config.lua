Config = Config or {}
local c = Config

c.AllFluidBarrels = true			-- adds barrels for all fluids (not gases!)
c.SulfurProcessingChain = true		-- adds a sulfur processing chain
c.NewFlameThrowerAmmoRecipe = true	-- changes the recipe of flame thrower ammo
c.Seawater = true					-- adds seawater which must be cleaned to use as normal water (except steam boilers)
c.NewWaterIcon = false				-- false; changes the icon of water to an icon that shows the H2O molecule
c.NewRocketFuelRecipe = true		-- adds new chemical fluids to improve the rocket fuel recipe