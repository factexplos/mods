fluid = function(name, subgroup, order)
	data.raw.fluid[name].subgroup = subgroup
	data.raw.fluid[name].order = order
end
item = function(name, subgroup, order)
	data.raw.item[name].subgroup = subgroup
	data.raw.item[name].order = order
end
recipe = function(name, subgroup, order)
	data.raw.recipe[name].subgroup = subgroup
	data.raw.recipe[name].order = order
end

data:extend({
  {
	type = "item-group",
	name = "chemistry",
	icon = "__N.Tech-Chemistry__/graphics/icons/chemistry-64.png",
	inventory_order = "d",
	order = "d"
  },
  {
	type = "item-subgroup",
	name = "chemistry-materials",
	group = "chemistry",
	order = "a"
  },
  {
	type = "item-subgroup",
	name = "oil-processing",
	group = "chemistry",
	order = "b"
  },
  {
	type = "item-subgroup",
	name = "gas-processing",
	group = "chemistry",
	order = "d"
  },
  {
	type = "item-subgroup",
	name = "organics",
	group = "chemistry",
	order = "e"
  }
})
if Config.SulfurProcessingChain then
	data:extend({
	  {
		type = "item-subgroup",
		name = "sulfur-processing",
		group = "chemistry",
		order = "c"
	  }
	})
	recipe("sulfuric-acid", "sulfur-processing", "c[sulfuric-acid]")
end

--[[fluid("water", "fluid", "a[raw-fluid]-a")
fluid("crude-oil", "fluid", "a[raw-fluid]-b")
fluid("heavy-oil", "fluid", "a[raw-fluid]-c")
fluid("light-oil", "fluid", "a[raw-fluid]-d")
fluid("petroleum-gas", "fluid", "a[raw-fluid]-e")
fluid("lubricant", "fluid", "a[raw-fluid]-f")
fluid("sulfuric-acid", "fluid", "a[raw-fluid]-o")

if data.raw.fluid["lava"] then fluid("lava", "fluid", "a[raw-fluid]-g") end
if data.raw.fluid["molten-soldering-tin"] then fluid("molten-soldering-tin", "fluid", "a[raw-fluid]-h") end]]

recipe("basic-oil-processing", "oil-processing", "a-a")
recipe("advanced-oil-processing", "oil-processing", "a-b")

recipe("heavy-oil-cracking", "oil-processing", "b-a")
recipe("light-oil-cracking", "oil-processing", "b-b")

recipe("solid-fuel-from-heavy-oil", "oil-processing", "c-a")
recipe("solid-fuel-from-light-oil", "oil-processing", "c-b")
recipe("solid-fuel-from-petroleum-gas", "oil-processing", "c-c")