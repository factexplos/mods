data:extend({
  {
    type = "item",
    name = "carbon",
    icon = "__N.Tech-Chemistry__/graphics/icons/carbon.png",
    flags = {"goes-to-main-inventory"},
    subgroup = "chemistry-materials",
    order = "a[carbon]",
    stack_size = 50
  }
})