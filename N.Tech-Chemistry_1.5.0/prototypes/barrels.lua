local fluids = {"water", "heavy-oil", "light-oil", "petroleum-gas", "lubricant", "sulfuric-acid"}
if Config.SulfurProcessingChain then
	table.insert(fluids, "sulfur-trioxide")
	table.insert(fluids, "disulfuric-acid")
end

local chars = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"}

for i,fluid in ipairs(fluids) do
	local item = fluid .. "-barrel"
	local fill = "fill-" .. fluid .. "-barrel"
	local empty = "empty-" .. fluid .. "-barrel"
	data:extend({
	  {
		type = "item",
		name = item,
		icon = "__N.Tech-Chemistry__/graphics/icons/barrel/" .. item .. ".png",
		flags = {"goes-to-main-inventory"},
		subgroup = "barrel",
		order = chars[i + 2] .. "[" .. item .. "]-a",
		stack_size = 10
	  },
	  {
		type = "recipe",
		name = fill,
		icon = "__N.Tech-Chemistry__/graphics/icons/barrel/" .. fill .. ".png",
		category = "crafting-with-fluid",
		energy_required = 1,
		subgroup = "barrel",
		--order = chars[(i * 2) + 2],
		order = chars[i + 2] .. "[" .. item .. "]-b",
		enabled = false,
		ingredients =
		{
		  {type="fluid", name=fluid, amount=25},
		  {type="item", name="empty-barrel", amount=1},
		},
		results=
		{
		  {type="item", name=item, amount=1}
		}
	  },
	  {
		type = "recipe",
		name = empty,
		icon = "__N.Tech-Chemistry__/graphics/icons/barrel/" .. empty .. ".png",
		category = "crafting-with-fluid",
		energy_required = 1,
		subgroup = "barrel",
		--order = chars[(i * 2) + 3],
		order = chars[i + 2] .. "[" .. item .. "]-c",
		enabled = false,
		ingredients =
		{
		  {type="item", name=item, amount=1}
		},
		results=
		{
		  {type="fluid", name=fluid, amount=25},
		  {type="item", name="empty-barrel", amount=1}
		}
	  }
	})
	table.insert(data.raw.technology["fluid-handling"].effects, {
		type = "unlock-recipe",
		recipe = fill
	})
	table.insert(data.raw.technology["fluid-handling"].effects, {
		type = "unlock-recipe",
		recipe = empty
	})
end