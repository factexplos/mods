-- crude oil values
data.raw.fluid["crude-oil"].max_temperature = 90
data.raw.fluid["crude-oil"].heat_capacity = "400J"
data.raw.fluid["crude-oil"].pressure_to_speed_ratio = 0.25

-- new barrel icon
data.raw.item["crude-oil-barrel"].icon = "__N.Tech-Chemistry__/graphics/icons/barrel/crude-oil-barrel.png"

-- new water icon
if Config.NewWaterIcon then
	data.raw.fluid["seawater"].icon = "__base__/graphics/icons/fluid/water.png"
	data.raw.fluid["seawater"].base_color = {r=0, g=0.34, b=0.6}
	data.raw.fluid["water"].icon = "__N.Tech-Chemistry__/graphics/icons/water.png"
end

-- oil processing recipe changes
data.raw.recipe["advanced-oil-processing"].ingredients[1].name = "hydrogen"
data.raw.recipe["advanced-oil-processing"].ingredients[1].amount = 50
data.raw.recipe["heavy-oil-cracking"].ingredients[1] = {type="fluid", name="hydrogen", amount=30}
data.raw.recipe["light-oil-cracking"].ingredients[1] = {type="fluid", name="hydrogen", amount=30}
table.insert(data.raw.recipe["advanced-oil-processing"].results, {type="item", name="sulfur", amount=1})

-- order fixes
data.raw.technology["oil-processing"].order = "d-c-a"
data.raw.technology["advanced-oil-processing"].order = "d-c-b"
data.raw.technology["sulfur-processing"].order = "d-c-c"
data.raw.technology["plastics"].order = "d-c-e"
data.raw.technology["fluid-handling"].order = "d-d"

-- flame thrower ammo recipe changes
if Config.NewFlameThrowerAmmoRecipe then
	data.raw.recipe["flame-thrower-ammo"].ingredients = {
		{"iron-plate", 5},
		{"plastic-bar", 5},
		{type="fluid", name="heavy-oil", amount=2.5},
		{type="fluid", name="nitrogen", amount=20}
	}
end