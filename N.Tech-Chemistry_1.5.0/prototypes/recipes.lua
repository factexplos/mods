-- carbon
-- carbon dioxide
-- ammonia
-- air cracking
-- water electrolysis
-- methanol
-- methylamine
-- dimethylamine
-- methylhydrazine
-- dimethylhydrazine
-- rocket-fuel-from-methylhydrazine
-- rocket-fuel-from-dimethylhydrazine
-- crude oil desulphurization
-- sulfur-dioxide
-- sulfur-trioxide
-- disulfuric-acid
-- sulfuric-acid-from-disulfuric-acid
-- water purification
data:extend({
  {
	type = "recipe",
	name = "carbon",
	energy_required = 0.8,
	category = "chemistry",
	enabled = false,
	ingredients = {{"coal", 3}},
	results = {{"carbon", 2}}
  },
  {
	type = "recipe",
	name = "carbon-dioxide",
	icon = "__N.Tech-Chemistry__/graphics/icons/carbon-dioxide.png",
	energy_required = 2,
	enabled = false,
	subgroup = "organics",
	order = "a[carbon-dioxide]",
	category = "chemistry",
	ingredients = {
	  {"carbon", 1},
	  {type="fluid", name="oxygen", amount=10}
	},
	results = {
	  {type="fluid", name="carbon-dioxide", amount=10}
	}
  },
  {
	type = "recipe",
	name = "ammonia",
	icon = "__N.Tech-Chemistry__/graphics/icons/ammonia.png",
	energy_required = 1,
	category = "chemistry",
	subgroup = "gas-processing",
	order = "b[gas-production]-b[ammonia]",
	enabled = false,
	ingredients =
	{
      {type="fluid", name="nitrogen", amount=10},
	  {type="fluid", name="hydrogen", amount=30}
	},
	results=
	{
	  {type="fluid", name="ammonia", amount=20}
	}
  },
  {
	type = "recipe",
	name = "air-cracking",
	icon = "__N.Tech-Chemistry__/graphics/icons/air-cracking.png",
	energy_required = 3,
	enabled = "false",
	subgroup = "gas-processing",
	order = "b[gas-production]-a[air-cracking]",
	category = "chemistry",
	ingredients = {},
	results = {
	  {type="fluid", name="oxygen", amount=0.75},
	  {type="fluid", name="nitrogen", amount=1.25}
	}
  },
  {
	type = "recipe",
	name = "water-electrolysis",
	icon = "__N.Tech-Chemistry__/graphics/icons/water-electrolysis.png",
	subgroup = "gas-processing",
	order = "a[water-processing]-b[water-electrolysis]",
	enabled = false,
	category = "chemistry",
	ingredients = {
	  {type="fluid", name="water", amount=2}
	},
	results = {
	  {type="fluid", name="hydrogen", amount=20},
	  {type="fluid", name="oxygen", amount=10}
	}
  }
})
if Config.NewRocketFuelRecipe then
	data:extend({
	  {
		type = "recipe",
		name = "methanol",
		icon = "__N.Tech-Chemistry__/graphics/icons/methanol.png",
		energy_required = 1,
		category = "chemistry",
		subgroup = "organics",
		order = "c[methanol]",
		enabled = false,
		ingredients =
		{
		  {type="fluid", name="carbon-dioxide", amount=5},
		  {type="fluid", name="hydrogen", amount=5}
		},
		results=
		{
		  {type="fluid", name="methanol", amount=20}
		}
	  },
	  {
		type = "recipe",
		name = "methylamine",
		icon = "__N.Tech-Chemistry__/graphics/icons/methylamine.png",
		energy_required = 1,
		category = "chemistry",
		subgroup = "organics",
		order = "d[methylamine]",
		enabled = false,
		ingredients =
		{
		  {type="fluid", name="methanol", amount=1},
		  {type="fluid", name="ammonia", amount=10},
		},
		results=
		{
		  {type="fluid", name="methylamine", amount=10},
		  {type="fluid", name="water", amount=1}
		}
	  },
	  {
		type = "recipe",
		name = "dimethylamine",
		icon = "__N.Tech-Chemistry__/graphics/icons/dimethylamine.png",
		energy_required = 1,
		category = "chemistry",
		subgroup = "organics",
		order = "e[dimethylamine]",
		enabled = false,
		ingredients =
		{
		  {type="fluid", name="methylamine", amount=10},
		  {type="fluid", name="methanol", amount=1},
		},
		results=
		{
		  {type="fluid", name="dimethylamine", amount=10},
		  {type="fluid", name="water", amount=1}
		}
	  },
	  {
		type = "recipe",
		name = "methylhydrazine", -- Katalysator
		icon = "__N.Tech-Chemistry__/graphics/icons/methylhydrazine.png",
		energy_required = 1,
		category = "chemistry",
		subgroup = "organics",
		order = "f[methylhydrazine]",
		enabled = false,
		ingredients =
		{
		  {type="fluid", name="methylamine", amount=10},
		  {type="fluid", name="ammonia", amount=10},
		},
		results=
		{
		  {type="fluid", name="methylhydrazine", amount=1},
		  {type="fluid", name="hydrogen", amount=10}
		}
	  },
	  {
		type = "recipe",
		name = "dimethylhydrazine", -- Katalysator
		icon = "__N.Tech-Chemistry__/graphics/icons/dimethylhydrazine.png",
		energy_required = 1,
		category = "chemistry",
		subgroup = "organics",
		order = "g[dimethylhydrazine]",
		enabled = false,
		ingredients =
		{
		  {type="fluid", name="dimethylamine", amount=10},
		  {type="fluid", name="ammonia", amount=10},
		},
		results=
		{
		  {type="fluid", name="dimethylhydrazine", amount=1},
		  {type="fluid", name="hydrogen", amount=10}
		}
	  }
	})
	data.raw.recipe["rocket-fuel"].category = "chemistry"
	data.raw.recipe["rocket-fuel"].ingredients = {
		{type="fluid", name="methylhydrazine", amount=5},
		{type="fluid", name="dimethylhydrazine", amount=5}
	}
end
if Config.SulfurProcessingChain then
	data:extend({
	  {
		type = "recipe",
		name = "crude-oil-desulphurization",
		category = "chemistry",
		energy_required = 3,
		subgroup = "oil-processing",
		order = "b-c",
		enabled = false,
		icon = "__N.Tech-Chemistry__/graphics/icons/crude-oil-desulphurization.png",
		ingredients =
		{
		  {type="fluid", name="crude-oil", amount=20}
		},
		results=
		{
		  {type="fluid", name="crude-oil", amount=19},
		  {type="item", name="sulfur", amount=1}
		}
	  },
	  {
		type = "recipe",
		name = "sulfur-dioxide", -- Chemischer Ofen (Schwefel muss verbrannt werden)
		category = "chemistry",
		energy_required = 1,
		enabled = false,
		subgroup = "sulfur-processing",
		order = "a[sulfur-dioxide]",
		ingredients =
		{
		  {type="fluid", name="oxygen", amount=10},
		  {type="item", name="sulfur", amount=1},
		},
		results=
		{
		  {type="fluid", name="sulfur-dioxide", amount=10}
		}
	  },
	  {
		type = "recipe",
		name = "sulfur-trioxide", -- Katalysator (sonst zu langsam)
		category = "chemistry",
		energy_required = 2,
		enabled = false,
		subgroup = "sulfur-processing",
		order = "b[sulfur-trioxide]",
		ingredients =
		{
		  {type="fluid", name="sulfur-dioxide", amount=10},
		  {type="fluid", name="oxygen", amount=10}
		},
		results=
		{
		  {type="fluid", name="sulfur-trioxide", amount=10}
		}
	  },
	  {
		type = "recipe",
		name = "disulfuric-acid",
		category = "chemistry",
		energy_required = 1,
		enabled = false,
		subgroup = "sulfur-processing",
		order = "e[disulfuric-acid]",
		ingredients =
		{
		  {type="fluid", name="sulfur-trioxide", amount=1},
		  {type="fluid", name="sulfuric-acid", amount=1},
		},
		results=
		{
		  {type="fluid", name="disulfuric-acid", amount=1}
		}
	  },
	  {
		type = "recipe",
		name = "sulfuric-acid-from-disulfuric-acid",
		icon = "__N.Tech-Chemistry__/graphics/icons/sulfuric-acid-from-disulfuric-acid-old.png",
		category = "chemistry",
		energy_required = 2,
		enabled = false,
		subgroup = "sulfur-processing",
		order = "d[sulfuric-acid-from-disulfuric-acid]",
		ingredients =
		{
		  {type="fluid", name="disulfuric-acid", amount=10},
		  {type="fluid", name="water", amount=10},
		},
		results=
		{
		  {type="fluid", name="sulfuric-acid", amount=20}
		}
	  }
	})
	data.raw.recipe["sulfuric-acid"].energy_required = 10
	data.raw.recipe["sulfuric-acid"].ingredients = {
		{type="fluid", name="sulfur-trioxide", amount=10},
		{type="fluid", name="water", amount=10},
	}
	data.raw.recipe["sulfuric-acid"].results = {
		{type="fluid", name="sulfuric-acid", amount=20}
	}
end
if Config.Seawater then
	data:extend({
	  {
		type = "recipe",
		name = "water-purification",
		icon = "__N.Tech-Chemistry__/graphics/icons/water-purification.png",
		energy_required = 1,
		enabled = false,
		subgroup = "gas-processing",
		order = "a[water-processing]-a[water-purification]",
		category = "chemistry",
		ingredients = {
		  {type="fluid", name="seawater", amount=100}
		},
		results = {
		  {type="fluid", name="water", amount=100},
		  {type="fluid", name="carbon-dioxide", amount=100}
		}
	  }
	})
end
if data.raw["assembling-machine"]["electrolycer"] then
	data.raw.recipe["water-electrolysis"].category = "electrolycer"
end
