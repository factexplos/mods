-- chemistry
-- advanced chemistry
-- advanced chemistry 2
-- oxygen production
-- nitrogen processing
-- carbon processing
-- electrolysis
-- sulfur oxide processing
-- water purification
data:extend({
  {
	type = "technology",
	name = "chemistry",
	icon = "__N.Tech-Chemistry__/graphics/icons/chemistry.png",
	icon_size = 128,
	prerequisites = {"oil-processing"},
	effects = {
	  {
		type = "unlock-recipe",
		recipe = "carbon-dioxide"
	  }
	},
	unit =
	{
	  count = 40,
	  ingredients =
	  {
		{"science-pack-1", 1},
		{"science-pack-2", 1}
	  },
	  time = 15
	},
	order = "d-a-a"
  },
  {
	type = "technology",
	name = "advanced-chemistry",
	icon = "__base__/graphics/technology/advanced-chemistry.png",
	prerequisites = {"chemistry", "battery"},
	effects = {
	  {
		type = "unlock-recipe",
		recipe = "science-pack-3"
	  }
	},
	unit =
	{
	  count = 75,
	  ingredients =
	  {	
		{"science-pack-1", 1},
		{"science-pack-2", 1}
	  },
	  time = 30
	},
	order = "d-a-b",
	upgrade = "true"
  },
  {
	type = "technology",
	name = "advanced-chemistry-2",
	icon = "__base__/graphics/technology/advanced-chemistry.png",
	prerequisites = {"advanced-chemistry", "nitrogen-processing"},
	effects = {},
	unit =
	{
	  count = 250,
	  ingredients =
	  {	
		{"science-pack-1", 1},
		{"science-pack-2", 1},
		{"science-pack-3", 1}
	  },
	  time = 60
	},
	order = "d-a-b",
	upgrade = "true"
  },
  
  {
	type = "technology",
	name = "oxygen-production",
	icon = "__N.Tech-Chemistry__/graphics/icons/oxygen-production.png",
	icon_size = 128,
	effects = {
	  {
		type = "unlock-recipe",
		recipe = "air-cracking"
	  }
	},
	prerequisites = {"chemistry"},
	unit = {
      count = 25,
      ingredients = {
        {"science-pack-1", 1},
        {"science-pack-2", 1}
      },
      time = 30
    },
    order = "d-b-a"
  },
  {
	type = "technology",
	name = "nitrogen-processing",
	icon = "__N.Tech-Chemistry__/graphics/icons/nitrogen-processing.png",
	icon_size = 128,
	effects = {
	  {
		type = "unlock-recipe",
		recipe = "ammonia"
	  }
	},
	prerequisites = {"chemistry"},
	unit = {
      count = 50,
      ingredients = {
        {"science-pack-1", 1},
        {"science-pack-2", 1},
        {"science-pack-3", 1}
      },
      time = 30
    },
    order = "d-b-b"
  },
  {
	type = "technology",
	name = "carbon-processing",
	icon = "__N.Tech-Chemistry__/graphics/icons/carbon-tech.png",
	icon_size = 128,
	effects = {
	  {
		type = "unlock-recipe",
		recipe = "carbon"
	  }
	},
	prerequisites = {"advanced-material-processing"},
	unit = {
      count = 100,
      ingredients = {
        {"science-pack-1", 1},
        {"science-pack-2", 1}
      },
      time = 30
    },
    order = "d-b-e"
  },
  
  {
	type = "technology",
	name = "electrolysis",
	icon = "__N.Tech-Chemistry__/graphics/icons/water-electrolysis.png",
	effects = {
	  {
		type = "unlock-recipe",
		recipe = "water-electrolysis"
	  }
	},
	prerequisites = {"oxygen-production"},
	unit = {
      count = 20,
      ingredients = {
        {"science-pack-1", 1},
        {"science-pack-2", 1}
      },
      time = 20
    },
    order = "d-b-d"
  }
})
if Config.SulfurProcessingChain then
	data:extend({
	  {
		type = "technology",
		name = "sulfur-oxide-processing",
		icon = "__N.Tech-Chemistry__/graphics/icons/sulfur-oxide-processing.png",
		icon_size = 128,
		effects = {
		  {
			type = "unlock-recipe",
			recipe = "sulfur-dioxide"
		  },
		  {
			type = "unlock-recipe",
			recipe = "sulfur-trioxide"
		  },
		  {
			type = "unlock-recipe",
			recipe = "sulfuric-acid"
		  },
		  {
			type = "unlock-recipe",
			recipe = "disulfuric-acid"
		  },
		  {
			type = "unlock-recipe",
			recipe = "sulfuric-acid-from-disulfuric-acid"
		  }
		},
		prerequisites = {"sulfur-processing", "oxygen-production"},
		unit = {
		  count = 50,
		  ingredients = {
			{"science-pack-1", 1},
			{"science-pack-2", 1},
		  },
		  time = 30
		},
		order = "d-c-d"
	  }
	})
	table.remove(data.raw.technology["sulfur-processing"].effects, 2)
	table.remove(data.raw.technology["sulfur-processing"].effects, 1)
	table.insert(data.raw.technology["sulfur-processing"].effects, {type="unlock-recipe", recipe="crude-oil-desulphurization"})
end
if Config.Seawater then
	data:extend({
	  {
		type = "technology",
		name = "water-purification",
		icon = "__N.Tech-Chemistry__/graphics/icons/water-purification-tech.png",
		icon_size = 128,
		effects = {
		  {
			type = "unlock-recipe",
			recipe = "water-purification"
		  }
		},
		prerequisites = {"chemistry"},
		unit = {
		  count = 60,
		  ingredients = {
			{"science-pack-1", 1},
			{"science-pack-2", 1}
		  },
		  time = 45
		},
		order = "d-b-c"
	  }
	})
	table.insert(data.raw.technology["electrolysis"].prerequisites, "water-purification")
end

if Config.NewRocketFuelRecipe then
	table.insert(data.raw.technology["advanced-chemistry"].effects, {type="unlock-recipe", recipe="methanol"})
	table.insert(data.raw.technology["advanced-chemistry-2"].effects, {type="unlock-recipe", recipe="methylamine"})
	table.insert(data.raw.technology["advanced-chemistry-2"].effects, {type="unlock-recipe", recipe="dimethylamine"})
	table.insert(data.raw.technology["advanced-chemistry-2"].effects, {type="unlock-recipe", recipe="methylhydrazine"})
	table.insert(data.raw.technology["advanced-chemistry-2"].effects, {type="unlock-recipe", recipe="dimethylhydrazine"})
end

if #data.raw.technology["advanced-chemistry-2"].effects == 0 then
	data.raw.technology["advanced-chemistry-2"] = nil
end

table.insert(data.raw.technology["sulfur-processing"].prerequisites, "chemistry")
table.insert(data.raw.technology["explosive-rocketry"].prerequisites, "chemistry")
table.insert(data.raw.technology["alien-technology"].prerequisites, "advanced-chemistry")
table.insert(data.raw.technology["advanced-oil-processing"].prerequisites, "chemistry")
table.insert(data.raw.technology["plastics"].prerequisites, "chemistry")
table.remove(data.raw.technology["advanced-oil-processing"].effects, 3)