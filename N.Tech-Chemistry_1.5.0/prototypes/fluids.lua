-- hydrogen
-- nitrogen
-- oxygen
-- carbon dioxide
-- ammonia
-- methanol
-- methylamine
-- dimethylamine
-- methylhydrazine
-- dimethylhydrazine
-- sulfur-dioxide
-- sulfur-trioxide
-- disulfuric-acid
-- seawater
data:extend({
  {
	type = "fluid",
	name = "hydrogen",
	default_temperature = 25,
	heat_capacity = "0KJ",
	base_color = {r=0.4, g=0.4, b=0.4},
	flow_color = {r=0.5, g=0.5, b=0.5},
	max_temperature = 200,
	icon = "__N.Tech-Chemistry__/graphics/icons/hydrogen.png",
	pressure_to_speed_ratio = 0.4,
    flow_to_energy_ratio = 0.59,
	subgroup = "fluid",
    order = "a[raw-fluid]-j[hydrogen]"
  },
  {
	type = "fluid",
	name = "nitrogen",
	default_temperature = 25,
	heat_capacity = "0KJ",
	base_color = {r=0.4, g=0.4, b=0.4},
	flow_color = {r=0.5, g=0.5, b=0.5},
	max_temperature = 200,
	icon = "__N.Tech-Chemistry__/graphics/icons/nitrogen.png",
	pressure_to_speed_ratio = 0.4,
    flow_to_energy_ratio = 0.59,
	subgroup = "fluid",
    order = "a[raw-fluid]-k[nitrogen]"
  },
  {
	type = "fluid",
	name = "oxygen",
	default_temperature = 25,
	heat_capacity = "0KJ",
	base_color = {r=0.4, g=0.4, b=0.4},
	flow_color = {r=0.5, g=0.5, b=0.5},
	max_temperature = 200,
	icon = "__N.Tech-Chemistry__/graphics/icons/oxygen.png",
	pressure_to_speed_ratio = 0.4,
    flow_to_energy_ratio = 0.59,
	subgroup = "fluid",
    order = "a[raw-fluid]-i[oxygen]"
  },
  {
	type = "fluid",
	name = "carbon-dioxide",
	default_temperature = 25,
	heat_capacity = "0KJ",
	base_color = {r=0.4, g=0.4, b=0.4},
	flow_color = {r=0.5, g=0.5, b=0.5},
	max_temperature = 200,
	icon = "__N.Tech-Chemistry__/graphics/icons/carbon-dioxide.png",
	pressure_to_speed_ratio = 0.4,
    flow_to_energy_ratio = 0.59,
	subgroup = "fluid",
    order = "a[raw-fluid]-t[carbon-dioxide]"
  },
  {
	type = "fluid",
	name = "ammonia",
	default_temperature = 25,
	heat_capacity = "0KJ",
	base_color = {r=0.4, g=0.4, b=0.4},
	flow_color = {r=0.5, g=0.5, b=0.5},
	max_temperature = 200,
	icon = "__N.Tech-Chemistry__/graphics/icons/ammonia.png",
	pressure_to_speed_ratio = 0.4,
    flow_to_energy_ratio = 0.59,
	subgroup = "fluid",
    order = "a[raw-fluid]-q[ammonia]"
  }
})
if Config.NewRocketFuelRecipe then
	data:extend({
	  {
		type = "fluid",
		name = "methanol",
		default_temperature = 25,
		heat_capacity = "0KJ",
		base_color = {r=0.4, g=0.4, b=0.4},
		flow_color = {r=0.5, g=0.5, b=0.5},
		max_temperature = 200,
		icon = "__N.Tech-Chemistry__/graphics/icons/methanol.png",
		pressure_to_speed_ratio = 0.4,
		flow_to_energy_ratio = 0.59,
		subgroup = "fluid",
		order = "a[raw-fluid]-r[methanol]"
	  },
	  {
		type = "fluid",
		name = "methylamine",
		default_temperature = 25,
		heat_capacity = "0KJ",
		base_color = {r=0.4, g=0.4, b=0.4},
		flow_color = {r=0.5, g=0.5, b=0.5},
		max_temperature = 200,
		icon = "__N.Tech-Chemistry__/graphics/icons/methylamine.png",
		pressure_to_speed_ratio = 0.4,
		flow_to_energy_ratio = 0.59,
		subgroup = "fluid",
		order = "a[raw-fluid]-s[methylamine]"
	  },
	  {
		type = "fluid",
		name = "dimethylamine",
		default_temperature = 25,
		heat_capacity = "0KJ",
		base_color = {r=0.4, g=0.4, b=0.4},
		flow_color = {r=0.5, g=0.5, b=0.5},
		max_temperature = 200,
		icon = "__N.Tech-Chemistry__/graphics/icons/dimethylamine.png",
		pressure_to_speed_ratio = 0.4,
		flow_to_energy_ratio = 0.59,
		subgroup = "fluid",
		order = "a[raw-fluid]-t[dimethylamine]"
	  },
	  {
		type = "fluid",
		name = "methylhydrazine",
		default_temperature = 25,
		heat_capacity = "0KJ",
		base_color = {r=0.4, g=0.4, b=0.4},
		flow_color = {r=0.5, g=0.5, b=0.5},
		max_temperature = 200,
		icon = "__N.Tech-Chemistry__/graphics/icons/methylhydrazine.png",
		pressure_to_speed_ratio = 0.4,
		flow_to_energy_ratio = 0.59,
		subgroup = "fluid",
		order = "a[raw-fluid]-u[methylhydrazine]"
	  },
	  {
		type = "fluid",
		name = "dimethylhydrazine",
		default_temperature = 25,
		heat_capacity = "0KJ",
		base_color = {r=0.4, g=0.4, b=0.4},
		flow_color = {r=0.5, g=0.5, b=0.5},
		max_temperature = 200,
		icon = "__N.Tech-Chemistry__/graphics/icons/dimethylhydrazine.png",
		pressure_to_speed_ratio = 0.4,
		flow_to_energy_ratio = 0.59,
		subgroup = "fluid",
		order = "a[raw-fluid]-v[dimethylhydrazine]"
	  }
	})
end
if Config.SulfurProcessingChain then
	data:extend({
	  {
		type = "fluid",
		name = "sulfur-dioxide",
		default_temperature = 25,
		heat_capacity = "0KJ",
		base_color = {r=0.7, g=0.7, b=0.7},
		flow_color = {r=0.5, g=0.5, b=0.5},
		max_temperature = 100,
		icon = "__N.Tech-Chemistry__/graphics/icons/sulfur-dioxide.png",
		pressure_to_speed_ratio = 0.4,
		flow_to_energy_ratio = 0.59,
		subgroup = "fluid",
		order = "a[raw-fluid]-m[sulfur-dioxide]"
	  },
	  {
		type = "fluid",
		name = "sulfur-trioxide",
		default_temperature = 25,
		heat_capacity = "0KJ",
		base_color = {r=0.7, g=0.7, b=0.7},
		flow_color = {r=0.5, g=0.5, b=0.5},
		max_temperature = 100,
		icon = "__N.Tech-Chemistry__/graphics/icons/sulfur-trioxide.png",
		pressure_to_speed_ratio = 0.4,
		flow_to_energy_ratio = 0.59,
		subgroup = "fluid",
		order = "a[raw-fluid]-n[sulfur-trioxide]"
	  },
	  {
		type = "fluid",
		name = "disulfuric-acid",
		default_temperature = 25,
		heat_capacity = "1KJ",
		base_color = {r=0.93, g=0.83, b=0.67},
		flow_color = {r=0.5, g=0.5, b=0.5},
		max_temperature = 100,
		icon = "__N.Tech-Chemistry__/graphics/icons/disulfuric-acid-old.png",
		pressure_to_speed_ratio = 0.4,
		flow_to_energy_ratio = 0.59,
		subgroup = "fluid",
		order = "a[raw-fluid]-p[disulfuric-acid]"
	  }
	})
end
if Config.Seawater then
	data:extend({
	  {
		type = "fluid",
		name = "seawater",
		default_temperature = 15,
		max_temperature = 200,
		heat_capacity = "2KJ",
		base_color = {r=0, g=0.41, b=0.33},
		flow_color = {r=0.7, g=0.7, b=0.7},
		icon = "__N.Tech-Chemistry__/graphics/icons/seawater.png",
		order = "a[fluid]-a[water]z",
		pressure_to_speed_ratio = 0.4,
		flow_to_energy_ratio = 0.59,
	  }
	})
	data.raw["offshore-pump"]["offshore-pump"].fluid = "seawater"
end
